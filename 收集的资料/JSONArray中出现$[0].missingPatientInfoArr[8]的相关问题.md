# JSONArray中出现**"$[0].missingPatientInfoArr[8]"**的相关问题

开发中经常使用JSONArray数组来处理数据，难免会遇到<font color='red'>Arr中的某个JSONObject与任意一个JSONObject重复，此时JSONArray会自动开启引用检测，将重复的元素变成 类似于 `"$[0].missingPatientInfoArr[8]"` 的字符串，导致程序混乱。</font>

如图：

![image-20240424171249247](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20240424171249247.png)

![image-20240424171259325](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20240424171259325.png)



解决：<font color='green'>可以关闭单个引用检测，对该JSONArray进行一次流化处理。</font>

代码示例：

```java
// 关闭单个引用检测 否则JSONArray重复的中JSONObject会被处理成"$[0].missingPatientInfoArr[8]"
JSONArray missingItems = ……
String listString  = JSON.toJSONString(missingItems, SerializerFeature.DisableCircularReferenceDetect);
missingItems = (JSONArray) JSON.parse(listString);
```

