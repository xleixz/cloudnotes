# OneNET传感器API调用说明

> **需要用到的数据：**
>
> - **OneNET API文档：**[https://admin.ln.cmcconenet.com/doc/book/application-develop/api/api-usage.html](https://admin.ln.cmcconenet.com/doc/book/application-develop/api/api-usage.html "OneNET API文档")
>
> - **获取数据的URL：**http://api.heclouds.com/devices/778234537/datapoints
>
>   <font color="red">devices 后面的 77823453 是设备id  </font>
>
> - **请求头api-key：** `7xUkdhEF3Qbi1DU7ileY=4orfso=`
>
>   <font color="red">api-key放在header里面</font>

### 1 调用/获取数据的方式

1. 调用第三方URL（OneNET）获取数据，以下是我的个人方式：（<font color="red">devices为了方便直观查看，临时写死，后期通过从数据库里索引</font>）

   ```java
   /**
        * 获取传感器数据
        * 若输入传感器名称，则迷糊查询，若没有输入传感器名称则返回全部
        * @return
        * @throws Exception
        */
       @PostMapping("/get")
       public AjaxResult getSensor(String sensorName) throws Exception {
   
   
           //获取当前登录用户的userid
           Long useridL = SecurityUtils.getUserId();
           int userid = useridL.intValue();
           System.out.println("==================>userid:" + userid);
   
   //        long devices = sweetProjectService.selectSensorCount(userid);
   
           long devices = 778234537;
   
           String apikey = sweetProjectService.selectSensorAppKey(sensorName, userid).getSensorKey();
           System.out.println("==================>apikey:" + apikey);
   
           String url = "http://api.heclouds.com/devices/" + devices + "/datapoints";
   
           HttpURLConnection httpURLConnection = null;
           InputStream in = null;
           BufferedReader br = null;
           String result = null;// 返回结果字符串
           try {
               // 创建远程url连接对象
               URL url1 = new URL(url);
               // 通过远程url连接对象打开一个连接，强转成httpURLConnection类
               httpURLConnection = (HttpURLConnection) url1.openConnection();
               // 设置连接方式：get
               httpURLConnection.setRequestMethod("GET");
               // 设置连接主机服务器的超时时间：15000毫秒
               httpURLConnection.setConnectTimeout(15000);
               // 设置读取远程返回的数据时间：60000毫秒
               httpURLConnection.setReadTimeout(60000);
               // 设置格式
               httpURLConnection.setRequestProperty("Content-type", "application/json");
               // 设置鉴权信息：Authorization: Bearer da3efcbf-0845-4fe3-8aba-ee040be542c0 OneNet平台使用的是Authorization+token
               // httpURLConnection.setRequestProperty("authorization", "Bearer eyJhbGciOiJIUzUxMiJ9.eyJsb2dpbl91c2VyX2tleSI6ImJhNTllMmI4LTA5YjQtNDNkNS05NjVkLTlmMTAyMDMzOGNmNSJ9.cQZP3N94ofFOw_TWC4I15a2V_hugZmm7Lns8ZdKwaxiDZzNLMLz3Hocfarqlom2saKUaL_Ydh82rwai-cdNzIA");
               httpURLConnection.setRequestProperty("api-key", apikey);
               // 发送请求
               httpURLConnection.connect();
               // 通过connection连接，获取输入流
               if (httpURLConnection.getResponseCode() == 200) {
                   in = httpURLConnection.getInputStream();
                   // 封装输入流is，并指定字符集
                   br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
                   // 存放数据
                   StringBuffer sbf = new StringBuffer();
                   String temp = null;
                   while ((temp = br.readLine()) != null) {
                       sbf.append(temp);
                       sbf.append("\r\n");
                   }
                   result = sbf.toString();
                   System.out.println("response=" + result);
                   //处理返回的数据
                   JSONObject jsonObject = JSON.parseObject(result);
                   System.out.println("========================> jsonobj:" + jsonObject);
                   JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                   System.out.println("========================> jsonobj1:" + jsonObject1);
                   String datastreams = jsonObject1.getString("datastreams");
                   System.out.println("========================> datastreams:" + datastreams);
   
                   //获取datastreams中的datapoints
                   JSONArray jsonArray = JSON.parseArray(datastreams);
                   System.out.println("========================> jsonarray:" + jsonArray);
                   //获取datapoints中的value
                   JSONObject value = jsonArray.getJSONObject(0);
                   System.out.println("========================> value:" + value);
   
   
                   JSONArray array = jsonObject1.getJSONArray("datastreams");
                   System.out.println("========================> array:" + array);
   
   
                   JSONObject jsonObject2 = array.getJSONObject(0);
                   System.out.println("datastreams内的=" + jsonObject2);
                   JSONArray array1 = jsonObject2.getJSONArray("datapoints");
                   for (int i = 0; i < array1.size(); i++) {
                       JSONObject jsonObject4 = array1.getJSONObject(i);
                       String time = jsonObject4.get("at").toString();
                       String values = jsonObject4.get("value").toString();
                       System.out.println("时间为 " + time.toString() + " and " + " 湿度为 " + values.toString());
                   }
   
                   AjaxResult ajax = AjaxResult.success();
                   ajax.put("data", array);
                   return ajax;
               }
           } catch (UnsupportedEncodingException unsupportedEncodingException) {
               unsupportedEncodingException.printStackTrace();
           } catch (ProtocolException protocolException) {
               protocolException.printStackTrace();
           } catch (MalformedURLException malformedURLException) {
               malformedURLException.printStackTrace();
           } catch (IOException ioException) {
               ioException.printStackTrace();
           } finally {
               // 关闭资源
               if (null != br) {
                   try {
                       br.close();
                   } catch (IOException e) {
                       e.printStackTrace();
                   }
               }
               if (null != in) {
                   try {
                       in.close();
                   } catch (IOException e) {
                       e.printStackTrace();
                   }
               }
               httpURLConnection.disconnect();// 关闭远程连接
           }
   
           return AjaxResult.success();
   
       }
   ```

### 2 返回结果

> `datapoints` ：数据点；
>
> `at`：时间；
>
> `value`：数据；
>
> `id`：周期名称，例如**twd**为土壤温度。

```json
{
    "msg": "操作成功",
    "code": 200,
    "data": [
        {
            "datapoints": [
                {
                    "at": "2021-09-14 14:59:47.453",
                    "value": {
                        "1": "26.5"
                    }
                }
            ],
            "id": "twd"
        },
        {
            "datapoints": [
                {
                    "at": "2022-07-22 22:54:29.593",
                    "value": {
                        "1": "135"
                    }
                }
            ],
            "id": "fx"
        },
        {
            "datapoints": [
                {
                    "at": "2022-07-22 22:54:16.087",
                    "value": {
                        "1": "14"
                    }
                }
            ],
            "id": "gz"
        },
        {
            "datapoints": [
                {
                    "at": "2022-07-22 22:54:30.353",
                    "value": {
                        "1": "1"
                    }
                }
            ],
            "id": "yl"
        },
        {
            "datapoints": [
                {
                    "at": "2021-09-14 14:59:48.213",
                    "value": {
                        "1": "0"
                    }
                }
            ],
            "id": "tsd"
        },
        {
            "datapoints": [
                {
                    "at": "2021-09-14 15:18:40.714",
                    "value": {
                        "1": "9.82"
                    }
                }
            ],
            "id": "ph"
        },
        {
            "datapoints": [
                {
                    "at": "2022-07-22 22:54:14.618",
                    "value": {
                        "1": "23.1"
                    }
                }
            ],
            "id": "kwd"
        },
        {
            "datapoints": [
                {
                    "at": "2022-07-22 22:54:15.343",
                    "value": {
                        "1": "98.3"
                    }
                }
            ],
            "id": "ksd"
        },
        {
            "datapoints": [
                {
                    "at": "2022-07-22 22:54:28.848",
                    "value": {
                        "1": "1.9"
                    }
                }
            ],
            "id": "fs"
        },
        {
            "datapoints": [
                {
                    "at": "2021-09-14 14:59:48.953",
                    "value": {
                        "1": "0"
                    }
                }
            ],
            "id": "ec"
        }
    ]
}
```



