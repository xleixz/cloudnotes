# MQTT协议

> **需要用到的数据：**
>
> - **EMQX API文档：**[https://www.emqx.io/docs/zh/v4/getting-started/getting-started.html](https://www.emqx.io/docs/zh/v4/getting-started/getting-started.html "EMQX API文档")
>
> - **引入依赖**
>
>   ```xml
>   <dependency>
>               <groupId>org.springframework.integration</groupId>
>               <artifactId>spring-integration-mqtt</artifactId>
>   </dependency>
>   ```

**获取方式**

1. 通过`URL`和`username` 、 `passwordChar` 密码验证获取订阅主题。

以下是我的请求方式：

***MqttGateway*** 发送消息接口

```java
package com.lns.shst.cofig.mqtt;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.mqtt.support.MqttHeaders;
import org.springframework.messaging.handler.annotation.Header;

@MessagingGateway(defaultRequestChannel = "mqttOutboundChannel")
public interface MqttGateway {

    // 定义重载方法，用于消息发送
    void sendToMqtt(String payload);
    // 指定topic进行消息发送
    void sendToMqtt(@Header(MqttHeaders.TOPIC) String topic, String payload);
    void sendToMqtt(@Header(MqttHeaders.TOPIC) String topic, @Header(MqttHeaders.QOS) int qos, String payload);
    void sendToMqtt(@Header(MqttHeaders.TOPIC) String topic, @Header(MqttHeaders.QOS) int qos, byte[] payload);
}
```



***MQTT配置文件***

```java
package com.lns.shst.cofig.mqtt;

import com.alibaba.fastjson2.JSONObject;
import com.lns.shst.pojo.mqtt.device.sensorData.MqttInfo;
import com.lns.shst.pojo.mqtt.device.sensorData.SetMqttData;
import com.lns.shst.service.impl.MqttDataServiceImpl;
import com.lns.shst.service.impl.MqttInfoServiceImpl;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.core.MessageProducer;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.core.MqttPahoClientFactory;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;
import org.springframework.integration.mqtt.outbound.MqttPahoMessageHandler;
import org.springframework.integration.mqtt.support.DefaultPahoMessageConverter;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

import java.text.SimpleDateFormat;
import java.util.List;


@Configuration
public class MqttConfigNet {

    @Autowired
    private MqttDataServiceImpl mqttDataService;

    @Autowired
    private MqttInfoServiceImpl mqttInfoService;

    // 消费消息

    
	/**
     * 创建MqttPahoClientFactory，设置MQTT Broker连接属性，如果使用SSL验证，也在这里设置。
     *
     * @return factory
     */

    @Bean
    public MqttPahoClientFactory mqttClientFactory() {
        DefaultMqttPahoClientFactory factory = new DefaultMqttPahoClientFactory();
        MqttConnectOptions options = new MqttConnectOptions();

//        String URL = "tcp://mqtt.linoro.net:1883";
//        String username = "lvsoftdevice";
//        char [] passwordChar = {'8', '4', '8', '6', '2', '0', '8', '0'};

        // 根据instructions查询客户端信息
        List<MqttInfo> administratorsList = mqttInfoService.selectAdministratorsByInstructions("haian001");
        System.out.println("\033[33m[MSG]客户端信息 --> " + administratorsList + "\033[0m");

        // 从administratorsList集合中获取代理端URL地址
        String URL = administratorsList.get(0).getURL();
        // 从administratorsList集合中获取代理端用户名
        String username = administratorsList.get(0).getUsername();
        // 从administratorsList集合中获取代理端密码, 并转换为char数组
        char[] passwordChar = administratorsList.get(0).getPassword().toCharArray();

        System.out.println("\033[33m[MSG]URL ---------------------------> " + URL +
                " username --> " + username +
                " passwordChar --> " + passwordChar + "\033[0m");

        // 设置代理端的URL地址，可以是多个
        options.setServerURIs(new String[]{URL});
        //设置代理端的账户名
        options.setUserName(username);
        //设置代理端的密码
        char[] password = passwordChar;
        options.setPassword(password);

        factory.setConnectionOptions(options);
        return factory;
    }

    
	/**
     * 入站通道
     */

    @Bean
    public MessageChannel mqttInputChannel() {
        return new DirectChannel();
    }

    
	/**
     * 入站
     */

    @Bean
    public MessageProducer inbound() {
        // Paho客户端消息驱动通道适配器，主要用来订阅主题
        MqttPahoMessageDrivenChannelAdapter adapter = new MqttPahoMessageDrivenChannelAdapter("consumerClient-paho",
                mqttClientFactory(), "boat", "collector", "battery", "/sensor/haian001");
        adapter.setCompletionTimeout(5000);

        // Paho消息转换器
        DefaultPahoMessageConverter defaultPahoMessageConverter = new DefaultPahoMessageConverter();
        // 按字节接收消息
//        defaultPahoMessageConverter.setPayloadAsBytes(true);
        adapter.setConverter(defaultPahoMessageConverter);
        adapter.setQos(1); // 设置QoS
        adapter.setOutputChannel(mqttInputChannel());
        return adapter;
    }

    @Bean
    // ServiceActivator注解表明：当前方法用于处理MQTT消息，inputChannel参数指定了用于消费消息的channel。
    @ServiceActivator(inputChannel = "mqttInputChannel")
    public MessageHandler handler() {
        return message -> {
            String payload = message.getPayload().toString();

            // byte[] bytes = (byte[]) message.getPayload(); // 收到的消息是字节格式
            String topic = message.getHeaders().get("mqtt_receivedTopic").toString();

            // 根据主题分别进行消息处理。
            if (topic.equals("test")) {
                System.out.println("\033[33m[MSG]主题 --> " + topic + " 负载 --> " + payload + "\033[0m");

            } else if (topic.equals("collector")) {
                System.out.println("\033[33m[MSG]采集器的消息 --> " + payload + "\033[0m");

            } else if (topic.equals("/sensor/haian001")) {
                System.out.println("\033[33m[MSG]主题 --> " + topic + " 负载 --> " + payload + "\033[0m");

                // 获取payload中{之后的内容
                String payload1 = payload.substring(payload.indexOf("{"));
                System.out.println("\033[33m[MSG]payload1 --> " + payload1 + "\033[0m");

                // 将payload转换为json格式,获取json中的数据
                JSONObject jsonObject = JSONObject.parseObject(payload1);
                
				/*
                 * @Author lei.hx
                 * @Description //TODO 处理传感器数据内容
                 */

                //获取payload中的kwd
                String kwd = jsonObject.getString("kwd");
                //获取payload中的ksd
                String ksd = jsonObject.getString("ksd");
                //获取payload中的pm
                String pm25 = jsonObject.getString("pm25");
                //获取payload中的yjhfw
                String yjhfw = jsonObject.getString("yjhfw");
                //获取payload中的jq
                String jq = jsonObject.getString("jq");
                //获取payload中的gz
                String gz = jsonObject.getString("gz");
                //获取payload中的aq
                String aq = jsonObject.getString("aq");
                //获取payload中的fs
                String fs = jsonObject.getString("fs");
                //获取payload中的fx
                String fx = jsonObject.getString("fx");
                //获取payload中的co2
                String co2 = jsonObject.getString("co2");
                //获取payload中的lhq
                String lhq = jsonObject.getString("lhq");
                //获取payload中的yl
                String yl = jsonObject.getString("yl");
                //获取payload中的UID
                String uid = jsonObject.getString("UID");

                
				/*
                 * @Author lei.hx
                 * @Description //TODO 处理deid, topic, timestamp, time_date
                 */

                SetMqttData setMqttData = new SetMqttData();
                // 获取当前系统时间戳 -- 毫秒级
                long timestamp = System.currentTimeMillis();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                String time_date = format.format(timestamp);
                setMqttData.setTime_date(time_date);

                setMqttData.setDeid("2BD2A207-3A08-4EB4-A4B6-DDF5AB3C10D2");// 临时写死
                setMqttData.setTopic(topic);
                setMqttData.setTimestamp(timestamp);

                setMqttData.setKwd(kwd);
                setMqttData.setKsd(ksd);
                setMqttData.setPm25(pm25);
                setMqttData.setYjhfw(yjhfw);
                setMqttData.setJq(jq);
                setMqttData.setGz(gz);
                setMqttData.setAq(aq);
                setMqttData.setFs(fs);
                setMqttData.setFx(fx);
                setMqttData.setCo2(co2);
                setMqttData.setLhq(lhq);
                setMqttData.setYl(yl);
                setMqttData.setUid(uid);

                System.out.println("\033[33m[MSG]mqttData --> " + setMqttData + "\033[0m");

                //将数据插入所有传感器数据的数据库
                mqttDataService.insertMqttDataAll(setMqttData);


            } else {
                System.out.println("\033[33m[MSG]主题 --> " + topic + " 负载 --> " + payload + "\033[0m");


            }

        };

    }

    // 发送消息

    
	/**
     * 出站通道
     */

    @Bean
    public MessageChannel mqttOutboundChannel() {
        return new DirectChannel();
    }

   
	/**
     * 出站
     */

    @Bean
    @ServiceActivator(inputChannel = "mqttOutboundChannel")
    public MessageHandler outbound() {

        // 发送消息和消费消息Channel可以使用相同MqttPahoClientFactory
        MqttPahoMessageHandler messageHandler = new MqttPahoMessageHandler("publishClient", mqttClientFactory());
        messageHandler.setAsync(true); // 如果设置成true，即异步，发送消息时将不会阻塞。
        messageHandler.setDefaultTopic("command");
        messageHandler.setDefaultQos(1); // 设置默认QoS

        // Paho消息转换器
        DefaultPahoMessageConverter defaultPahoMessageConverter = new DefaultPahoMessageConverter();

        // defaultPahoMessageConverter.setPayloadAsBytes(true); // 发送默认按字节类型发送消息
        messageHandler.setConverter(defaultPahoMessageConverter);
        return messageHandler;
    }


}
```

