# 萤石云平台

[TOC]

---

> **需要用到的数据：**
>
> - **萤石云 API文档：**[https://open.ys7.com/doc/zh/book/index/user.html](https://open.ys7.com/doc/zh/book/index/user.html "萤石云 API文档")
>
> - **获取视频监控的accessToken请求**https://open.ys7.com/api/lapp/token/get?appKey=3199877dceba401aa5fff380bb216274&appSecret=e1389510509b6284212eba7e66f8b934
>
>   <font color="red">appKey：监控设备的Key</font>
>
>   <font color="red">appSecret：app密钥</font>
>
> - **获取监控直播地址：**https://open.ys7.com/api/lapp/v2/live/address/get?accessToken=at.e0rm3vlrddpmr7h94981ijh4bgy2fv5v-5tq4dwkqao-0n31kd6-7h6upqhph&deviceSerial=G66146452&protocol=2&quality=1
>
>   <font color="red">accessToken：授权过访问令牌</font>
>
>   <font color="red">deviceSerial：直播源序列号</font>
>
>   <font color="red">protocol：流播放协议，1-ezopen、2-hls、3-rtmp、4-flv，默认为1</font>
>
>   <font color="red">quality：视频清晰度，1-高清（主码流）、2-流畅（子码流）</font>

### 1 获取方式

1. 通过`appKey`和`appSecret`请求**获取视频监控的accessToken请求**的URL，返回**访问令牌accessToken**；
2. 通过`accessToken`，`deviceSerial`，`protocol`，`quality`请求**获取监控直播地址**，返回**直播地址URL**。

以下是我的请求方式：

```java
/**
     * 获取视频监控的accessToken请求
     */
    @PostMapping("/getAccessToken")
    public AjaxResult getAccessToken(String monitorName) throws Exception {

        //获取当前登录用户的userid
        Long useridL = SecurityUtils.getUserId();
        int userid = useridL.intValue();
        System.out.println("==================>userid:" + userid);

        //String deviceSerial = "G66146452";
        String deviceSerial = sweetProjectService.selectMonitorAppKey(monitorName, userid).getDeviceSerial();

        String appKey = (sweetProjectService.selectMonitorAppKey(monitorName, userid)).getMonitorKey();
        System.out.println("=====================appKey=====================" + appKey);
        String appSecret = (sweetProjectService.selectMonitorAppKey(monitorName, userid)).getProjectKey();
        System.out.println("=====================appSecret=====================" + appSecret);

        String url = "https://open.ys7.com/api/lapp/token/get?appKey=" + appKey + "&appSecret=" + appSecret;

        // 创建 httpClient 对象
        HttpClient httpClient = HttpClients.createDefault();
        // 创建 url
        URIBuilder builder = new URIBuilder(url);
        builder.addParameter("username", "xxx");
        URI uri = builder.build();

        // 创建 httpPost 请求
        HttpPost httpPost = new HttpPost(uri);

        // 执行请求
        HttpResponse response = httpClient.execute(httpPost);

        System.out.println(response.getStatusLine().getStatusCode());
        // 处理返回数据

        if (response.getStatusLine().getStatusCode() == 200) {

            String result = EntityUtils.toString(response.getEntity());
            //获取result中的accessToken部分
            String accessToken = result.substring(result.indexOf("accessToken") + 14, result.indexOf("expireTime") - 3);
            System.out.println("=====================accessToken=====================" + accessToken);

            long protocol = 2;
            //质量 - 流畅 2
            long quality = 2;
            String url2 = "https://open.ys7.com/api/lapp/v2/live/address/get?accessToken=" + accessToken + "&deviceSerial=" + deviceSerial + "&protocol=" + protocol + "&quality=" + quality;

            // 创建 httpClient 对象
            httpClient = HttpClients.createDefault();
            // 创建 url
            builder = new URIBuilder(url2);
            builder.addParameter("username", "xxx");
            uri = builder.build();

            // 创建 httpPost 请求
            httpPost = new HttpPost(uri);
            // 执行请求
            response = httpClient.execute(httpPost);
            System.out.println(response.getStatusLine().getStatusCode());

            // 处理返回数据
            String result2 = EntityUtils.toString(response.getEntity());
            System.out.println("============result2===========>" + result2);

            JSONObject jsonObject = JSON.parseObject(result2);
            System.out.println("jsonobj =========>" + jsonObject);
            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
            System.out.println("jsonobj1 ========>" + jsonObject1);
            //获取jsonObject1中的url部分
            String url3 = jsonObject1.getString("url");
            System.out.println("=====================url3=====================" + url3);

            //将url3修改到cs_monitor表中的url字段中
            sweetProjectService.updateMonitorUrl(url3, monitorName);

            //如果jsonObject的code为20007，则说明设备不在线，否则为在线
            if (jsonObject.get("code").equals("20007")) {
                AjaxResult ajax = AjaxResult.error();
                ajax.put("message", "设备不在线");
                ajax.put("code", "20007");
                return ajax;
            } else {

                //将cs_monitor表中的url，设备名，userid返回给前端
                String MonitorUrl = sweetProjectService.selectMonitorAppKey(monitorName, userid).getUrl();
                String monitorName2 = sweetProjectService.selectMonitorAppKey(monitorName, userid).getMonitorName();
                //封装到List集合中
                List<SweetViewUrl> sweetViewUrls = new ArrayList<>();
                SweetViewUrl sweetViewUrl = new SweetViewUrl();
                sweetViewUrl.setMonitorName(monitorName2);
                sweetViewUrl.setUrl(MonitorUrl);
                sweetViewUrl.setUserid(userid);
                sweetViewUrls.add(sweetViewUrl);
                System.out.println("=====================sweetViewUrls=====================" + sweetViewUrls);

//               List<SweetMonitor> sweetMonitors = sweetProjectService.selectMonitorAppKeyList(userid);


                //返回直播地址id和url
                AjaxResult ajax = AjaxResult.success();
                //将List集合返回给前端
                ajax.put("data", sweetViewUrls);
                return ajax;
            }

        }
        // 关闭链接
        ((CloseableHttpClient) httpClient).close();

        return AjaxResult.error("获取直播地址失败");
    }
```

### 2 返回结果

> **url**：播放地址

```json
{
    "msg": "操作成功",
    "code": 200,
    "data": [
        {
            "userid": 1,
            "monitorName": "大门",
            "url": "https://open.ys7.com/v3/openlive/G66146452_1_2.m3u8?expire=1660182553&id=477413100249690112&t=ed2cb05a9c575e3d629d914ddd6b52f5d1c4db9efa28376a35ae8fcbd09c8aca&ev=100"
        }
    ]
}
```



