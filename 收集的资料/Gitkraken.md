# Gitkraken和谐

**破解之前先关闭 Gitkraken 软件**

解压破解工具，进入工具中的 GitCracken 目录，然后在此目录打开命令行，依次执行以下 3 条命令：

```shell
yarn install
yarn build
yarn gitcracken patcher     # Mac\Linux 用户可能需要root权限，需在前面加上 sudo
```

![image-20240111091817610](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20240111091817610.png)