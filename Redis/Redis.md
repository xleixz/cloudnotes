# Redis

![image-20221117150116246](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20221117150116246.png)

---

[TOC]



---

Redis是一个开源（BSD许可）的，内存中的数据结构存储系统，他可以用作<font color="red">数据库、缓存、消息中间件MQ</font>。它支持多种类型的数据结构，如字符串（strings）、散列（hashes）、列表（lists）、集合（sets）、有序集合（sorted sets）与范围查询。

## 1 Nosql

Nosql，非关系型的数据库。

### 1.1 SQL历史

> 1、单机MySQL年代

缺点：

1. 数据量如果太大，一个机器放不下；
2. 数据的索引（B+Tree），一个机器内存放不下；
3. 访问量过大，服务器承载不住。

> 2、Memcached（缓存）+ MySQL + 垂直拆分（读写分离）

80%的网站都是在读，每次读都要查询数据库会十分的缓慢，效率不高！可以通过缓存技术来缓解压力，提高系统的效率。<font color="red">缓存主要解决读的问题。</font>

**发展过程**：优化数据和索引 ----> 文件缓存（IO） ----> Memcached（当时最热门的技术）

> 3、分库分表 + 水平拆分 + MySQL集群

==本质：数据库（读，写）==

早些年MyISAM：表锁，十分影响效率！高并发下会出现严重的锁问题。

Innodb：行锁

> 4、现代

MySQL等关系型数据库不够用，数据量很大，变化很快！MySQL有的使用来存一些较大的文件、博客、图片，数据库很大，效率就会降低！

> NoSQL有什么优点？

用户的个人信息，社交网络，地理位置，用户自己产生的数据，用户日志等爆发式增长！

这些情况就需要使用NoSQL数据库来处理。

### 1.2 NoSQL概述

> 什么是关系型数据库？

关系型数据库：一个表格，由行和列记录。列负责类型，行负责记录。

> 什么是非关系型数据库？

NoSQL = `Not Only SQL`（不仅仅是SQL）

> 泛指非关系型数据库，随着web2.0互联网的诞生，传统的关系型数据库很难满足web2.0时代的需求，**尤其是超大规模的高并发社区。**Redis是当下发展最快的技术。

用户的个人信息，社交网络，地理位置，用户自己产生的数据，用户日志**这些数据存储不需要一个固定的格式。**

> NoSQL特点

1. 方便拓展（数据之间没有关系，解耦）

2. 大数据量高性能（Redis一秒可以写8万次，可以读取11万次，NoSQL的缓存记录级，是一种细粒度的缓存，性能会比较高）

3. 数据类型是多样型的，不需要事先设计数据库（随取随用，如果是数据库量非常大的表，是无法设计的）

4. 传统的RDBMS和NoSQL

   ```txt
   传统的RDBMS: 
    - 结构化组织
    - SQL
    - 数据和关系都存在单独的表中
    - 严格的一致性
    - 基础的事物
    ……
   ```

   ```txt
   NoSQL: 
    - 不仅仅是数据
    - 没有固定的查询语言
    - 键值对存储，列存储，文档存储，图形数据库（社交关系）
    - CAP定理和BASE理论（异地多活）
    - 高性能，高可用，高可扩
    ……
   ```

> 大数据时代的3V+3高

3V：主要是描述问题的

1. 海量Volume
2. 多样Variety
3. 实时Velocity

3高：主要是对程序的要求

1. 高并发
2. 高可扩
3. 高性能


​	

**真正的公司实践：NoSQL + RDBMS 一起使用才是最强的！**

---

​	

## 2 Redis入门

[Redis官网：https://redis.io/](https://redis.io/)

Linux版本可在官网下载，<font color = "red">**Windows版本在GitHub上下载（但是停更很久了）。**</font>

<font color = "red">**Redis推荐都是在Linux服务器上搭建的。**</font>

### 2.1 概述

> Redis是什么？

**Redis（Remote Dictionary Server），即远程字典服务。**是一个开源的使用ANSI ***C语言***编写、支持网络、可基于内存、可持久化的日志型、Key-Value数据库，并提供多种语言的API。Redis会周期性的把更新的数据写入磁盘或者把修改操作写入追加的记录文件，并且在此基础上实现了master-slave（主从）同步。

免费和开源！是当下最热门的NoSQL技术之一！**也被称之为结构化数据库！**

> Redis性能

读的速度是11万次/s，写的速度是8万1千次/s

> Redis作用

1. 内存存储，持久化**rdb、aof**（断电后数据仍然存在）
2. 效率高，可以用于高速缓存
3. 发布订阅系统，消息队列
4. 地图信息分析
5. 计数器（浏览量）、计时器
6. ……

> Redis特性

1. 多样的数据类型
2. 持久化
3. 集群
4. 事务
5. ……

### 2.2 Windows安装Redis

1. 下载安装包（推荐GitHub）

2. 解压到环境目录

3. 开启Redis，运行服务`redis-server.exe`即可！

4. 使用Redis客户端`redis-cli.exe`连接Redis

   ![image-20221115232127505](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20221115232127505.png)

<font color="red">**虽然Windows操作简单，但是官网推荐Redis在Linux中开发！**</font>

### 2.3 Linux安装Redis

[见Linux系统安装和卸载Redis](https://gitee.com/lei-hx/CloudNotes/blob/master/%E5%90%8E%E7%AB%AF%E5%BC%80%E5%8F%91/Linux%E6%9C%8D%E5%8A%A1%E5%99%A8%E7%9B%B8%E5%85%B3/Linux%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85%E5%92%8C%E5%8D%B8%E8%BD%BDRedis.md "点击查看Linux系统操作Redis")

### 2.4 性能测试

**redis-benchmark 是一个官网自带的性能、压力测试工具！**

redis 性能测试工具可选参数如下所示：

| 序号 |           选项            | 描述                                       | 默认值    |
| :--- | :-----------------------: | :----------------------------------------- | :-------- |
| 1    |          **-h**           | 指定服务器主机名                           | 127.0.0.1 |
| 2    |          **-p**           | 指定服务器端口                             | 6379      |
| 3    |          **-s**           | 指定服务器 socket                          |           |
| 4    |          **-c**           | 指定并发连接数                             | 50        |
| 5    |          **-n**           | 指定请求数                                 | 10000     |
| 6    |          **-d**           | 以字节的形式指定 SET/GET 值的数据大小      | 2         |
| 7    |          **-k**           | 1=keep alive 0=reconnect                   | 1         |
| 8    |          **-r**           | SET/GET/INCR 使用随机 key, SADD 使用随机值 |           |
| 9    |          **-P**           | 通过管道传输 <numreq> 请求                 | 1         |
| 10   |          **-q**           | 强制退出 redis。仅显示 query/sec 值        |           |
| 11   |         **--csv**         | 以 CSV 格式输出                            |           |
| 12   | ***-l\*（L 的小写字母）** | 生成循环，永久执行测试                     |           |
| 13   |          **-t**           | 仅运行以逗号分隔的测试命令列表。           |           |
| 14   | ***-I\*（i 的大写字母）** | Idle 模式。仅打开 N 个 idle 连接并等待。   |           |

**实例**

以下实例使用了多个参数来测试 redis 性能：

```bash
$ redis-benchmark -h 127.0.0.1 -p 6379 -t set,lpush -n 10000 -q

SET: 146198.83 requests per second
LPUSH: 145560.41 requests per second
```

以上实例中主机为 127.0.0.1，端口号为 6379，执行的命令为 set,lpush，请求数为 10000，通过 -q 参数让结果只显示每秒执行的请求数。

**分析性能测试结果**

![image-20221116230111514](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20221116230111514.png)

### 2.5 Redis基础知识

redis默认有**16**个数据库，**默认使用第0个数据库。**

![image-20221116232052356](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20221116232052356.png)

:arrow_right: 可以使用select进行切换数据库。

```bash
select 3
```

:arrow_right: 查看数据库大小

```bash
DBSIZE
```

:arrow_right: 查看当前数据库所有的Key

```bash
keys *
```

:arrow_right: 清空全部/清空当前库

```bash
# 清空全部数据库
flushall
# 清空当前库
flushdb
```

> **Redis是单线程的！**

Redis是很快的，官方表示，Redis是基于内存操作的，CPU不是Redis性能瓶颈，Redis的瓶颈是根据机器的内存和网络带宽，既然可以使用单线程来实现，所以就是用单线程了！

**为什么Redis单线程还这么快？**

了解：

1. 高性能服务器不一定是多线程的
2. 多线程（CPU上下文会切换）不一定比单线程效率高
3. CPU速度 > 内存速度 > 硬盘速度

<font color="green">**核心：Redis是将所有的数据全部放在内存中的，所以使用单线程去操作效率就是最高的，多线程CPU上下文会切换，较为耗时。对于内存系统来说，没有上下文切换，效率就是最高的。多次读写都是在一个CPU上，在内存情况下，这个就是最佳的方案。**</font>

---

​	

## 3 五大基本数据类型

### 3.1 Redis-Key

[Redis官网命令](https://redis.io/commands/)

:arrow_right: 查看所有key

```bash
keys *
```

:arrow_right: 设置key值

```bash
set name lei.hx
```

:arrow_right: 移动Key到对应数据库，后面的`1`指的是要移动到的的数据库

```bash
move name 1
```

:arrow_right: 判断当前的key是否存在，若返回1则存在，返回0则不存在

```bash
exists name
```

```bash
127.0.0.1:6379> exists name
(integer) 1
127.0.0.1:6379> exists age
(integer) 1
127.0.0.1:6379> exists ag1
(integer) 0
```

:arrow_right: 设置key的过期时间，单位是秒（s）

```bash
expire name 10
```

:arrow_right: 查看当前key的剩余时间

```bash
ttl name
```

:arrow_right: 查看当前key的类型

```bash
type name
```

```bash
127.0.0.1:6379> type name
string
```

### 3.2 String（字符串）

:arrow_right: 设置值

```bash
set key1 v1
```

:arrow_right: 获得值

```bash
get key1
```

:arrow_right: 获得所有key

```bash
keys *
```

:arrow_right: 判断某一个key是否再存

```bash
exists key1
```

:arrow_right: 追加字符串，若不存在，则相当于新set了一个key

```bash
append key1 "hello"
```

:arrow_right: 获取字符串长度

```bash
strlen key1
```

:arrow_right: 自增（应用：文章浏览量）

```bash
incr views
```

假设初始浏览量为1

```bash
127.0.0.1:6379> set views 1
OK
127.0.0.1:6379> get views
"1"
127.0.0.1:6379> incr views
(integer) 2
127.0.0.1:6379> get views
"2"
```

:arrow_right: 自减

```bash
decr views
```

:arrow_right: 设置步长自增

```bash
incrby views 10
```

:arrow_right: 设置步长自减

```bash
decrby views 15
```

浏览量按照指定步长自增、自减

```bash
127.0.0.1:6379> get views
"1"
127.0.0.1:6379> incrby views 10
(integer) 11
127.0.0.1:6379> get views
"11"
127.0.0.1:6379> decrby views 15
(integer) -4
```

:arrow_right: 字符串范围，`0` `3` 表示下标，`0` `-1`指获取所有字符串

```bash
getrange key1 0 3
```

```bash
getrange key1 0 -1
```

:arrow_right: 替换指定位置开始的字符串，`1`表示下标

```bash
setrange key1 1 xxx
```

例如：

```bash
127.0.0.1:6379> set key1 123456
OK
127.0.0.1:6379> setrange key1 1 xxx
(integer) 6
127.0.0.1:6379> get key1
"1xxx56"
```

:arrow_right: 设置值并设置过期时间 `setex = set with expire`。`30`指30秒过期，`test`指key3的值

```bash
setex key3 30 "test"
```

:arrow_right: 不存在时才会设置成功 `setnx = set if not exist`（在分布式锁中会经常使用）

```bash
setnx mykey "redis"
```

例如：

```bash
127.0.0.1:6379> setnx mykey "redis"    # 不存在mykey时，设置成功
(integer) 1
127.0.0.1:6379> keys *
1) "mykey"
127.0.0.1:6379> setnx mykey "f"        # 已存在mykey是，设置不成功，返回0
(integer) 0                            # 1为成功，0为失败
127.0.0.1:6379> get mykey
"redis"
```

:arrow_right: <font color="green">同时设置多个值</font>

```bash
mset k1 "v1" k2 "v2" k3 "v3"
```

:arrow_right: <font color="green">同时获取多个值</font>

```bash
mget k1 k2 k3
```

:arrow_right: <font color="green">同时设置多个值时判断是否存在，要么一起成功，要么一起失败</font>

```bash
msetnx k1 "v1" k4 "v4"
```

例如：

```bash
127.0.0.1:6379> mget k1 k2 k3
1) "v1"
2) "v2"
3) "v3"
127.0.0.1:6379> msetnx k1 "v1" k4 "v4"
(integer) 0
127.0.0.1:6379> get k4
(nil)
```

:arrow_right: 设置对象（两种方式）

```bash
# JSON字符串保存一个对象
set user:1 {name:lei.hx,age:3}

# 第二种设计，作为String字符串保存
mset user:2:name "lei.hx" user:2:age "23"
```

```bash
# JSON字符串保存一个对象
127.0.0.1:6379> get user:1
"{name:lei.hx,age:3}"

# 第二种设计，作为String字符串保存
127.0.0.1:6379> keys *
1) "user:2:name"
2) "user:2:age"
127.0.0.1:6379> mget user:2:name user:2:age
1) "lei.hx"
2) "23"
```

:arrow_right: getset组合命令，先get然后再set。如果不存在值，则返回nil；如果存在值，获取原来值，并设置新的值

```bash
getset db "redis"
```

```bash
127.0.0.1:6379> getset db "redis"
(nil)
127.0.0.1:6379> get db
"redis"
127.0.0.1:6379> getset db "mongdb"
"redis"
127.0.0.1:6379> get db
"mongdb"
```

### 3.3 List（列表）

基本的数据类型，列表，在redis中可以实现栈、队列、阻塞队列。Redis不区分大小写。

所有的list命令都是以 `l` 字母开头

:arrow_right: 将一个值或者多个值，插入到列表的头部（左边）

```bash
127.0.0.1:6379> lpush list one
(integer) 1
127.0.0.1:6379> lpush list two
(integer) 2
127.0.0.1:6379> lpush list three
(integer) 3
```

:arrow_right: 将一个值或者多个值，插入到列表的尾部（右边）

```bash
127.0.0.1:6379> rpush list four
(integer) 4
```

![image-20221117223210435](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20221117223210435.png)

:arrow_right: 获取区间范围内的list

```bash
127.0.0.1:6379> lrange list 0 -1
1) "three"
2) "two"
3) "one"
127.0.0.1:6379> lrange list 0 1
1) "three"
2) "two"
```

:arrow_right: 移除列表的第一个元素 `lpop list`

```bash
127.0.0.1:6379> lrange list 0 -1
1) "three"
2) "two"
3) "one"
4) "four"
127.0.0.1:6379> lpop list
"three"
127.0.0.1:6379> lrange list 0 -1
1) "two"
2) "one"
3) "four"
```

:arrow_right: 移除列表的最后一个元素 `rpop list`

```bash
127.0.0.1:6379> lrange list 0 -1
1) "two"
2) "one"
3) "four"
127.0.0.1:6379> rpop list
"four"
127.0.0.1:6379> lrange list 0 -1
1) "two"
2) "one"
```

:arrow_right: 通过下标获取list中的某一个值 `lindex list 0`

```bash
127.0.0.1:6379> lrange list 0 -1
1) "three"
2) "two"
3) "one"
4) "four"
127.0.0.1:6379> lindex list 0
"three"
127.0.0.1:6379> lindex list 1
"two"
```

:arrow_right: 获取列表的长度
```bash
127.0.0.1:6379> llen list
(integer) 4
```

:arrow_right: 移除指定的值，并设置移除数量（Redis中list列表可以存在重复key）

```bash
lrem list 1 one        # 1表示移除数量  one表示指定的key
```

:arrow_right: 通过截取指定长度，只剩下被截取的元素

```bash
ltrim mylist 1 2
```

例如：

```bash
127.0.0.1:6379> lpush mylist "hello"        # 设置一个key为mylist，值value为hello
(integer) 1
127.0.0.1:6379> lpush mylist "hello1"       # 设置一个key为mylist，值value为hello1
(integer) 2
127.0.0.1:6379> lpush mylist "hello2"       # 设置一个key为mylist，值value为hello2
(integer) 3 
127.0.0.1:6379> lpush mylist "hello3"       # 设置一个key为mylist，值value为hello3
(integer) 4
127.0.0.1:6379> ltrim mylist 1 2            # 截取下标为1~2的元素
OK
127.0.0.1:6379> lrange mylist 0 -1          # 获取所有元素查看是否只截取了1~2的元素
1) "hello2"
2) "hello1"
```

:arrow_right: 移除列表的最后一个元素，并移动到新的列表中

```bash
rpoplpush mylist myotherlist
```

例如：

```bash
127.0.0.1:6379> lpush mylist "hello"        # 设置一个key为mylist，值value为hello
(integer) 1
127.0.0.1:6379> lpush mylist "hello1"       # 设置一个key为mylist，值value为hello1
(integer) 2
127.0.0.1:6379> lpush mylist "hello2"       # 设置一个key为mylist，值value为hello2
(integer) 3 
127.0.0.1:6379> lpush mylist "hello3"       # 设置一个key为mylist，值value为hello3
(integer) 4
127.0.0.1:6379> rpoplpush mylist myotherlist # 移除列表的最后一个元素，并移动到新的列表myotherlist中
"hello"
127.0.0.1:6379> lrange mylist 0 -1
1) "hello3"
2) "hello2"
3) "hello1"
127.0.0.1:6379> lrange myotherlist 0 -1
1) "hello"
```

:arrow_right: 将指定列表中指定下标的值替换为另一个值，相当于更新操作

```bash
lset list 0 item
```

例如：

```:arrow_right:
127.0.0.1:6379> exists list              # 判断这个列表是否存在
(integer) 0
127.0.0.1:6379> lpush list "value1"      # 不存在会报错，在这里设置一个值
(integer) 1
127.0.0.1:6379> lrange list 0 -1
1) "value1"
127.0.0.1:6379> lset list 0 item         # 如果存在，更新指定下标的值；不存在会报错
OK
127.0.0.1:6379> lrange list 0 -1
1) "item"
```

:arrow_right: 将某个具体的value插入到指定元素的前面
```bash
linsert mylist before "world" "other"
```

:arrow_right: 将某个具体的value插入到指定元素的后面

```bash
linsert mylist after "hello" "test"
```

例如：

```bash
127.0.0.1:6379> rpush mylist "hello"
(integer) 1
127.0.0.1:6379> rpush mylist "world"
(integer) 2
127.0.0.1:6379> linsert mylist before "world" "other"
(integer) 3
127.0.0.1:6379> lrange mylist 0 -1
1) "hello"
2) "other"
3) "world"
127.0.0.1:6379> linsert mylist after "hello" "test"
(integer) 4
127.0.0.1:6379> lrange mylist 0 -1
1) "hello"
2) "test"
3) "other"
4) "world"
```

> 小结

- 它实际上是一个链表，before Node after，left，right都可以插入值
- 如果key不存在，需要创建链表
- 如果key存在，则可以新增内容
- 如果移除了所有的值，即为空链表，也代表不存在
- 在两边插入或者改动值效率最高

消息队列！（Lpush Rpop）

### 3.4  Set（集合）

**<font color ="red">set中的值是不能重复的！</font>**

:arrow_right: set集合中添加元素

```bash
sadd myset ""
```

:arrow_right: 查看指定set的所有值

```bash
smembers myset
```

:arrow_right: 判断某一个值是否在set集合中，是则返回1，不是则返回0

```bash
sismember myset hello
```

例如：

```bash
127.0.0.1:6379> sadd myset "hello"             # set集合中添加元素
(integer) 1
127.0.0.1:6379> sadd myset "lei.hx"   
(integer) 1
127.0.0.1:6379> sadd myset "test"
(integer) 1 
127.0.0.1:6379> smembers myset                 # 查看指定set的所有值
1) "hello"
2) "test"
3) "lei.hx"
127.0.0.1:6379> sismember myset hello            # 判断某一个值是否在set集合中，是则返回1，不是则返回0
(integer) 1
127.0.0.1:6379> sismember myset world
(integer) 0
```

:arrow_right: 获取set集合中内存元素的个数

```bash
scard myset
```

:arrow_right: 删除set中的指定元素

```bash
srem myset hello
```

:arrow_right: 随机删除set中的元素

```bash
spop myset
```

:arrow_right: 随机抽选出一个元素

```bash
srandmember myset
```

:arrow_right: 随机抽选出指定几个元素

```bash
srandmember myset 4
```

:arrow_right: 将一个指定的值移动到另一个set集合中。`myset`为指定元素所在集合，`myset2`为另一个指定集合

```bash
smove myset myset2 "hello"
```

:arrow_right: 并集，应用：微博、B站，共同关注

```bash
sunion key1 key2
```

:arrow_right: 差集，key1相对于key2的差别

```bash
sdiff key1 key2
```

:arrow_right: 交集，key1和key2相同部分

```bash
sinter key1 key2
```

### 3.5 Hash（哈希）

Map集合，`key-map`，这个值是一个map集合。本质和String类型没有太大区别，还是一个简单的 `key-value`。

:arrow_right: set一个具体的 `key-value`

```bash
hset myhash field1 lhx
```

:arrow_right: set多个的 `key-value`

```bash
hmset myhash field1 "hello" field2 "world"
```

:arrow_right: 获取多个 `key-value`

```bash
hget myhash field1
```

:arrow_right: 获取一个 `key-value`

```bash
hmget myhash field1 field2
```

:arrow_right: 获取所有 `key-value`

```bash
hgetall myhash
```

例如：

```bash
127.0.0.1:6379> hset myhash field1 lhx
(integer) 1
127.0.0.1:6379> hget myhash field1
"lhx"
127.0.0.1:6379> hmset myhash field1 "hello" field2 "world"
OK
127.0.0.1:6379> hget myhash field1
"hello"
127.0.0.1:6379> hget myhash field2
"world"
127.0.0.1:6379> hmget myhash field1 field2
1) "hello"
2) "world"
127.0.0.1:6379> hgetall myhash
1) "field1"
2) "hello"
3) "field2"
4) "world"
```

:arrow_right: 删除hash指定 `key` 字段，对应 `value`值也会消失

```bash
hdel myhash field1
```

:arrow_right: 获取所有的长度

```bash
hlen myhash
```

:arrow_right: 判断hash中的 `key`是否存在

```bash
hexists myhash field2
```

:arrow_right: 只获取所有的 `key `

```bash
hkeys myhash
```

:arrow_right: 只获取所有的`value `

```bash
hvals myhash
```

:arrow_right: 指定自增

```bash
hincrby myhash f3 1
```

:arrow_right: 如果存在则可以设置；如果不存在则不可以设置

```bash
hsetnx myhash f4 hello4
```

应用：hash变更数据更为方便，更适合方便存储对象。

### 3.6 Zset（有序集合）

在set的基础上增加了一个值作为排序，`set k1 v1` --> `zset k1 score1 v1`。

:arrow_right: 添加一个值

```absh
zadd myset 1 one
```

:arrow_right: 添加多个值

```bash
zadd myset 2 two 3 three
```

:arrow_right: 按照指定下标获取值

```bash
zrange myset 0 -1
```

:arrow_right: 按照`字段`从小到大`升序`排序 ，`-inf`负无穷，`+inf`正无穷，<font color="red">注意：最小值不能大于最大值。</font>也可以设置指定值排序。

```bash
zrangebyscore salary -inf +inf
```

:arrow_right: 按照`字段`从大到小`降序`排序 

```bash
zrevrange salary 0 -1
```

例如：

```bash
127.0.0.1:6379> zadd salary 2500 xiaohong        # 添加三个用户，薪水情况
(integer) 1
127.0.0.1:6379> zadd salary 5000 zhangsan
(integer) 1
127.0.0.1:6379> zadd salary 500  lhx
(integer) 1
127.0.0.1:6379> zrangebyscore salary -inf +inf    # 按照从小到大排序
1) "lhx"
2) "xiaohong"
3) "zhangsan"
127.0.0.1:6379> zrevrange salary 0 -1
1) "zhangsan"
2) "xiaohong"
3) "lhx"
```

:arrow_right: 移除元素

```bash
zrem salary xiaohong
```

:arrow_right: 获取有序集合中的个数

```bash
zcard salary
```

:arrow_right: 获取指定区间的成员数量

```bash
zcount myset 1 3
```

---

​	

## 4 三大特殊数据类型

### 4.1 geospatial （地理位置）

实现朋友的定位，附近的人，打车距离计算等功能。Redis的Geo功能在3.2版本就推出了！这个功能可以推算地址位置的信息，两地之间的距离，附近的人。

只有六个命令

> geoadd - 添加城市数据

:arrow_right: 添加城市数据，两级（地球南北极）无法添加，实践中可以通过Java程序直接一次性导入

参数：`key`  `value(经度、纬度、名称)`    <font color="red">注意：经纬度顺序不能颠倒。</font>

```bash
geoadd china:city 116.40 39.09 beijing
```

<font color="red">错误示范：</font>

```bash
127.0.0.1:6379> geoadd china:city 39.09 116.40 beijing
(error) ERR invalid longitude,latitude pair 39.090000,116.400000      # 经纬度错误
```

例如：

```bash
127.0.0.1:6379> geoadd china:city 116.40 39.09 beijing       # 添加城市数据
(integer) 1
127.0.0.1:6379> geoadd china:city 121.47 31.23 shanghai      # 添加城市数据
(integer) 1   
127.0.0.1:6379> geoadd china:city 160.50 29.53 chongqing     # 添加城市数据
(integer) 1
127.0.0.1:6379> geoadd china:city 114.05 22.52 shenzhen      # 添加城市数据
(integer) 1
127.0.0.1:6379> geoadd china:city 120.16 30.24 hangzhou      # 添加城市数据
(integer) 1
127.0.0.1:6379> geoadd china:city 108.96 34.26 xian          # 添加城市数据
(integer) 1
```

> geopos - 获取当前定位

:arrow_right: 获取当前定位，指定城市的经度、纬度

```bash
geopos china:city beijing
```

例如：

```bash
127.0.0.1:6379> geopos china:city beijing
1) 1) "116.39999896287918091"
   2) "39.08999952855024418"
127.0.0.1:6379> geopos china:city xian
1) 1) "108.96000176668167114"
   2) "34.25999964418929977"
127.0.0.1:6379> geopos china:city hangzhou
1) 1) "120.1600000262260437"
   2) "30.2400003229490224"
```

> geodist - 返回两个给定位置之间的距离

单位：（默认为**米**）

- **m** 表示单位为**米**
- **km**表示单位为**千米**
- **mi**表示单位为**英里**
- **ft**表示单位为**英尺**

:arrow_right: 返回两个给定位置之间的距离

```bash
geodist china:city beijing shanghai
```

例如：

```bash
127.0.0.1:6379> geodist china:city beijing shanghai      # 北京到上海距离查询，默认为米
"987909.3897"
127.0.0.1:6379> geodist china:city beijing shanghai km   # 北京到上海距离查询，单位为千米
"987.9094"
127.0.0.1:6379> geodist china:city beijing shanghai mi
"613.8600"
127.0.0.1:6379> geodist china:city beijing shanghai ft
"3241172.5383"
```

> georadius - 以给定的经纬度为中心，找出某一半径内的元素

应用：附近的人

:arrow_right: 以给定的经纬度为中心，找出某一半径内的元素。以`110 30`经纬度为中心，`1000 km`为半径，`withdist`为直线距离，`withcoord`为经纬度信息，`count n`查询n个，显示内容可以不加。

```bash
georadius china:city 110 30 1000 km withdist withcoord count 2
```

例如：

```bash
127.0.0.1:6379> georadius china:city 110 30 1000 km
1) "xian"
2) "shenzhen"
3) "hangzhou"
```

> georadiusbymember - 找出位于指定范围内的元素，中心点是由给定的位置元素决定

:arrow_right: 找出位于指定范围内的元素，中心点是由给定的位置元素决定。<font color="green">与georadius类似</font>

```bash
georadiusbymember china:city beijing 1000 km
```

> geohash - 返回一个或多个位置元素的Geohash表示

:arrow_right: 返回一个或多个位置元素的Geohash表示，将二维的经纬度转换为一维的字符串。（<font color="red">很少使用</font>）

```bash
geohash china:city beijing chongqing
```

例如：

```bash
127.0.0.1:6379> geohash china:city beijing chongqing    # 返回一个或多个位置元素的Geohash表示
1) "wwfy2spgu80"                                        # 将二维的经纬度转换为一维的字符串 
2) "xt4purb89n0"
```

<font color="#f33fff">**GEO 底层实现原理其实就是zset！可以使用zset命令进行操作geo**</font>

```bash
127.0.0.1:6379> zrange china:city 0 -1                  # 查看地图中的全部元素
1) "xian"
2) "shenzhen"
3) "hangzhou"
4) "shanghai"
5) "beijing"
6) "chongqing"
127.0.0.1:6379> zrem china:city beijing                  # 移除指定元素
(integer) 1
127.0.0.1:6379> zrange china:city 0 -1
1) "xian"
2) "shenzhen"
3) "hangzhou"
4) "shanghai"
5) "chongqing"
```

### 4.2 Hyperloglog （基数统计）

> 基数的概念

**基数**：就是不重复的元素。可以接受误差！

> 简介

Redis2.8.9版本更新了Hyperloglog数据结构。

**应用：网页的UV（一个人访问一个网站多次，但是还算作一次），**传统的方式使用set保存用户ID，统计set中元素数量作为标准（set元素不能重复）。

优点：占用内存较小。

> 运用

:arrow_right: 创建第一组元素 

```bash
pfadd mykey a b c d e f g h i j
```

:arrow_right: 统计基数数量

```bash
pfcount mykey
```

:arrow_right: 合并两组   `mykey3`合并后的新组

```bash
pfmerge mykey3 mykey mykey2
```

例如：

```bash
127.0.0.1:6379> pfadd mykey a b c d e f g h i j     # 创建第一组元素 
(integer) 1
127.0.0.1:6379> pfcount mykey                       # 统计基数数量
(integer) 10
127.0.0.1:6379> pfadd mykey2 i j z x c b v          # 创建第二组元素 
(integer) 1 
127.0.0.1:6379> pfcount mykey2                      # 统计基数数量
(integer) 7
127.0.0.1:6379> pfmerge mykey3 mykey mykey2
OK
127.0.0.1:6379> pfcount mykey3                      # 合并两组`mykey3`合并后的新组
(integer) 13
```

### 4.3 Bitmap（位存储）

> 位存储

应用：统计用户信息，活跃和不活跃，已登录和未登录的用户，已打卡和未打卡。*只要是两个状态的都可以使用Bitmaps*！

Bitmap 位图，数据结构，都是操作二进制位来进行记录，只有0和1两个状态！

> 运用

:arrow_right: 设置值

```bash
setbit sign 0 1
```

:arrow_right: 获取值

```bash
getbit sign 0
```

:arrow_right: 统计

```bash
bitcount sign
```

例如：

*使用Bitmap记录周一到周日的打卡记录：*

周一：1表示已打卡，周二：0表示未打卡

```bash
127.0.0.1:6379> setbit sign 0 1     # 设置值
(integer) 0
127.0.0.1:6379> setbit sign 1 0
(integer) 0
127.0.0.1:6379> setbit sign 2 1
(integer) 0
127.0.0.1:6379> setbit sign 3 1
(integer) 0
127.0.0.1:6379> setbit sign 4 1
(integer) 0
127.0.0.1:6379> setbit sign 5 1
(integer) 0
127.0.0.1:6379> setbit sign 6 0
(integer) 0
127.0.0.1:6379> getbit sign 3      # 获取值
(integer) 1
127.0.0.1:6379> getbit sign 6
(integer) 0
127.0.0.1:6379> bitcount sign      # 统计打卡记录
(integer) 5
```

---

​	

## 5 事务

> 事务的概念

**事务的本质：一组命令的集合！即一组命令全部执行，不允许被打断。事务中的所有命令都会被序列化，事务执行过程中，会按照顺序执行。Redis事务没有隔离级别的概念。所有命令在事务中，并没有直接被执行！只有发起执行命令时才会被执行！**

<font color="red">**Redis单条命令是保存原子性的，但是事务不保证原子性！**</font>

**Redis的事务：**

- 开启事务（`multi`）
- 命令入队（`......`）
- 执行事务（`exec`）

锁：Redis可以实现乐观锁。

> 正常执行事务

```bash
127.0.0.1:6379> multi         # 开启事务
OK
# 命令入队
127.0.0.1:6379(TX)> set k1 v1
QUEUED
127.0.0.1:6379(TX)> set k2 v2
QUEUED
127.0.0.1:6379(TX)> get k2
QUEUED
127.0.0.1:6379(TX)> set k3 v3
QUEUED
127.0.0.1:6379(TX)> exec    # 执行事务
1) OK
2) OK
3) "v2"
4) OK
```

> 放弃事务

```bash
127.0.0.1:6379> multi   # 开启事务
OK
# 命令入队
127.0.0.1:6379(TX)> set k1 v1
QUEUED
127.0.0.1:6379(TX)> set k2 v2
QUEUED
127.0.0.1:6379(TX)> set k4 v4
QUEUED
127.0.0.1:6379(TX)> discard    # 取消事务
OK
127.0.0.1:6379> get k4
(nil)
```

> 编译型异常（代码、命令有问题）

**事务中所有的命令都不会被执行！**

```bash
127.0.0.1:6379> multi
OK
127.0.0.1:6379(TX)> set k1 v1
QUEUED
127.0.0.1:6379(TX)> getset k3   # 错误的命令
(error) ERR wrong number of arguments for 'getset' command
127.0.0.1:6379(TX)> set k5 v5
QUEUED
127.0.0.1:6379(TX)> exec   # 执行事务报错，所有的命令都不会被执行
(error) EXECABORT Transaction discarded because of previous errors.
127.0.0.1:6379> get k5
(nil)
```

> 运行型异常

**事务队列中存在语法性错误，再执行命令时，其他命令是可以正常执行的，错误错误命令会抛出异常！**

```bash
127.0.0.1:6379> set k1 "v1"   # 
OK
127.0.0.1:6379> multi
OK
127.0.0.1:6379(TX)> incr k1    # 字符串无法自增，执行失败
QUEUED
127.0.0.1:6379(TX)> set k2 v2
QUEUED
127.0.0.1:6379(TX)> get k2
QUEUED
127.0.0.1:6379(TX)> exec   
1) (error) ERR value is not an integer or out of range    # 虽然第一条命令报错，但是依旧执行成功了
2) OK
3) "v2"
```

> 监控 Watch

**悲观锁：**

- 很悲观，什么时候都会出问题，无论做什么都会**加锁**！

**乐观锁：**

- 很乐观，认为什么时候都不会出问题，所以不会上锁！更新数据的时候去判断一下，在此期间是否有人修改过数据。
- 获取version
- 更新的时候比较version

<font color="green">**测试**</font>

**正常执行成功**

```bash
127.0.0.1:6379> set money 100
OK
127.0.0.1:6379> set out 0
OK
127.0.0.1:6379> watch money    # 监视 money 对象
OK
127.0.0.1:6379> multi   # 事务正常结束，数据期间没有发生变动，这个时候就正常执行成功！
OK
127.0.0.1:6379(TX)> decrby money 20
QUEUED
127.0.0.1:6379(TX)> incrby out 20
QUEUED
127.0.0.1:6379(TX)> exec
1) (integer) 80
2) (integer) 20
```

**测试多线程修改值，相当于使用watch可以当做Redis的乐观锁操作**

```bash
127.0.0.1:6379> watch money  # 监视 money 对象
OK
127.0.0.1:6379> multi
OK
127.0.0.1:6379(TX)> decrby money 10
QUEUED
127.0.0.1:6379(TX)> incrby out 10
QUEUED
127.0.0.1:6379(TX)> exec   # 执行之前，另外一个线程修改了值，导致事务执行失败
(nil)
```

```bash
# 另一个线程在上一个线程执行事务之前，修改值操作
127.0.0.1:6379> get money
"80"
127.0.0.1:6379> set money 1000
OK
127.0.0.1:6379> get money
"1000"
```

***解锁操作***

```bash
unwatch
```

![image-20221120155708514](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20221120155708514.png)

---

​	

## 6 Jedis

> Jedis概念

Jedis是Redis官方推荐的Java连接开发工具，使用Java操作Redis的一个中间件。

### 6.1 测试连接

1、导入对应的依赖

```xml
<dependencies>

        <!--jedis-->
        <dependency>
            <groupId>redis.clients</groupId>
            <artifactId>jedis</artifactId>
            <version>3.5.1</version>
        </dependency>

        <!--fastjson-->
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>fastjson</artifactId>
            <version>1.2.70</version>
        </dependency>
    </dependencies>
```

2、连接Redis

```java
package com.lei.hx;
import redis.clients.jedis.Jedis;
/**
 * @Author lei.hx
 * @Description //TODO
 * @Date 2022/11/20 - 16:09
 * @Version 1.8
 */
public class TestPing {

    public static void main(String[] args) {
        // new一个Jedis对象
        Jedis jedis = new Jedis("127.0.0.1", 6379);
        // Jedis 所有的命令就是Redis的所有指令
        System.out.println("\033[33m[MSG]" + jedis.ping() + "\033[0m");
    }
}
```

![image-20221120161541117](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20221120161541117.png)

<font color="red">**远程连接报错：**</font>

若是阿里云Esc服务器则需要设置如下操作：

1. 安全组要开启6379端口
2. 配置文件中 `daemonize` 设置为 `yes`，确保redis在后台运行中
3. 配置文件中 `build 127.0.0.1` 这行注释掉，这是对请求访问IP的限制
4. 配置文件中 `protected-mode` 设置为 `no` ，即可开启远程访问

进入配置文件：

```bash
sudo vim /home/lei.hx/environment/redis/bin/redis.conf
```

### 6.2 常用API

跟五大基本数据类型指令一样，[点击查看常用指令](# 3 五大基本数据类型)

例如：

```java
System.out.println("\033[33m[MSG]清空数据" + jedis.flushDB() + "\033[0m");
```

![image-20221120164148253](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20221120164148253.png)

### 6.3 Jedis 实现事务

**成功测试**

```java
package com.lei.hx;

import com.alibaba.fastjson.JSONObject;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

/**
 * @Author lei.hx
 * @Description //TODO
 * @Date 2022/11/20 - 16:50
 * @Version 1.8
 */
public class TestTx {

    public static void main(String[] args) {
        Jedis jedis = new Jedis("xleixz.cn", 6379);

        jedis.flushDB();

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("hello", "world");
        jsonObject.put("name", "lei.hx");

        // 开启事务
        Transaction multi = jedis.multi();
        try {
            multi.set("user1", jsonObject.toJSONString());
            multi.set("user2", jsonObject.toJSONString());
            // 执行事务
            multi.exec();
        } catch (Exception e) {
            // 放弃事务
            multi.discard();
            e.printStackTrace();
        } finally {
            System.out.println("\033[33m[MSG]user1:" + jedis.get("user1") + "\033[0m");
            System.out.println("\033[33m[MSG]user2:" + jedis.get("user2") + "\033[0m");
            // 关闭连接
            jedis.close();
        }
    }
}
```

![image-20221120171240160](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20221120171240160.png)

**失败测试**

```java
package com.lei.hx;

import com.alibaba.fastjson.JSONObject;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

/**
 * @Author lei.hx
 * @Description //TODO
 * @Date 2022/11/20 - 16:50
 * @Version 1.8
 */
public class TestTx {

    public static void main(String[] args) {
        Jedis jedis = new Jedis("xleixz.cn", 6379);

        jedis.flushDB();

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("hello", "world");
        jsonObject.put("name", "lei.hx");

        // 开启事务
        Transaction multi = jedis.multi();
        try {
            multi.set("user1", jsonObject.toJSONString());
            multi.set("user2", jsonObject.toJSONString());
            int i = 1 / 0;    // 设置失败
            // 执行事务
            multi.exec();
        } catch (Exception e) {
            // 放弃事务
            multi.discard();
            e.printStackTrace();
        } finally {
            System.out.println("\033[33m[MSG]user1:" + jedis.get("user1") + "\033[0m");
            System.out.println("\033[33m[MSG]user2:" + jedis.get("user2") + "\033[0m");
            // 关闭连接
            jedis.close();
        }
    }
}
```

![image-20221120171155353](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20221120171155353.png)

---

​	

## 7 SpringBoot 整合 Redis

所有的数据操作包括：jpa，jdbc，MongoDB，Redis都被**spring-data**包含。

### 7.1 源码分析

> 源码分析

<u>`RedisAutoConfiguration.class`</u>

```java
package org.springframework.boot.autoconfigure.data.redis;

import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

@AutoConfiguration
@ConditionalOnClass({RedisOperations.class})
@EnableConfigurationProperties({RedisProperties.class})
@Import({LettuceConnectionConfiguration.class, JedisConnectionConfiguration.class})
public class RedisAutoConfiguration {
    public RedisAutoConfiguration() {
    }

    @Bean
    // 当这个Bean不存在的时候，这个类可以替换该类
    // 可以自己定义一个redisTemplate
    @ConditionalOnMissingBean(
        name = {"redisTemplate"}
    )
    @ConditionalOnSingleCandidate(RedisConnectionFactory.class)
    public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        // 默认的RedisTemplate 没有过多的设置，redis对象都是需要序列化的
        // 两个泛型都是Object,Object类型，使用时需要强制转换 <String,Object>
        RedisTemplate<Object, Object> template = new RedisTemplate();
        template.setConnectionFactory(redisConnectionFactory);
        return template;
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnSingleCandidate(RedisConnectionFactory.class)
    // 由于String类型是Redis最常使用的类型，所以单独提出了一个bean
    public StringRedisTemplate stringRedisTemplate(RedisConnectionFactory redisConnectionFactory) {
        return new StringRedisTemplate(redisConnectionFactory);
    }
}
```

### 7.2 测试

<u>**1、导入操作Redis的依赖**</u>

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
```

在SpringBoot2.x以后，原来使用的 `jedis` 被替换为了 `lettuce`

**redis：**采用的直连，多个线程操作的话是不安全的，如果要避免该问题，就得使用`jedis pool`连接池，太麻烦！更像Bio模式。

**lettuce：**采用netty，实例可以在多个线程中共享，不存在线程不安全的情况，可以减少线程数量，不需要开启连接池了，更像Nio模式。

<u>**2、配置Redis**</u>

```yaml
spring:
  redis:
    host: 127.0.0.1
    port: 6379
```

**<u>3、测试</u>**

```java
package com.lei;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

@SpringBootTest
class RedisSpringBootApplicationTests {

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    void contextLoads() {

        // opsForValue 操作字符串
        redisTemplate.opsForValue().set("name", "lei");

        // opsForList 操作list
        redisTemplate.opsForList().leftPush("list", "a");

        // opsForSet 操作set
        redisTemplate.opsForSet().add("set", "a");

        // opsForHash 操作hash
        redisTemplate.opsForHash().put("hash", "name", "lei");

        // opsForZSet 操作zset
        redisTemplate.opsForZSet().add("zset", "a", 1);

        // 除了基本的操作，我们常用的方法都可以直接通过redisTemplate操作，比如事务和基本的CRUD
        // 获取redis的连接对象
        // RedisConnection connection = redisTemplate.getConnectionFactory().getConnection();
        // connection.flushDb();
        // connection.flushAll();

        redisTemplate.opsForValue().set("mykey", "lei");
        System.out.println("\033[33m[MGS]" + redisTemplate.opsForValue().get("mykey") + "\033[0m");

    }
}
```

![image-20221120180831039](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20221120180831039.png)

### 7.3 序列化的配置

> *默认的序列化方式是JDK序列化，SpringBoot自动帮我们在容器中生成了一个RedisTemplate和一个StringRedisTemplate。但是，这个RedisTemplate的泛型是<Object,Object>，写代码不方便，需要写好多类型转换的代码；我们需要一个泛型为<String,Object>形式的RedisTemplate。并且，这个RedisTemplate没有设置数据存在Redis时，key及value的序列化方式。*

在企业中，所有的pojo都会序列化！例如：

```java
@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {

    private String name;
    private int age;
}
```

但是这并不是最有效的方式。

<font color="red">**<u>还可以在设置对象时，进行序列化。</u>**</font>

```java
 @Test
    public void test() throws JsonProcessingException {
        // 真实的开发一般都使用json来传递对象
        User user = new User();
        user.setName("lei");
        user.setAge(23);
        // 将对象转换为json，序列化
        String jsonUser = new ObjectMapper().writeValueAsString(user);
        redisTemplate.opsForValue().set("user", jsonUser);
        System.out.println("\033[33m[MGS]" + redisTemplate.opsForValue().get("user") + "\033[0m");
    } 
```

<font color="red">**为了开发方便，可以直接注入StringRedisTemplate实现自动序列化，但是类型是String类型，不是JSON类型。**</font>

```java
@Autowired
    private StringRedisTemplate redisTemplate;

@Test
    public void test() throws JsonProcessingException {
        // 真实的开发一般都使用json来传递对象
        User user = new User();
        user.setName("lei");
        user.setAge(23);
    
        redisTemplate.opsForValue().set("user", user.toString());
        System.out.println("\033[33m[MGS]" + redisTemplate.opsForValue().get("user") + "\033[0m");
    }
```

> **<u>最佳方案：为了开发方便，编写一个属于自己的Redis配置模板 `RedisConfig`，同时搭配Utils工具类使用。</u>**

```java
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @Author lei.hx
 * @Description //Redis配置模板
 * @Date 2022/11/20 - 18:11
 * @Version 1.8
 */
@Configuration
public class RedisConfig {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        // 为了开发方便，一般直接使用<String, Object>
        RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
        template.setConnectionFactory(redisConnectionFactory);

        // Json序列化配置
         Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
         ObjectMapper om = new ObjectMapper();
         om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
         om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
         jackson2JsonRedisSerializer.setObjectMapper(om);

        // String的序列化
         StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();

        // key采用String的序列化方式
         template.setKeySerializer(stringRedisSerializer);
        // hash的key也采用String的序列化方式
         template.setHashKeySerializer(stringRedisSerializer);
        // value序列化方式采用jackson
         template.setValueSerializer(jackson2JsonRedisSerializer);
        // hash的value序列化方式采用jackson
         template.setHashValueSerializer(jackson2JsonRedisSerializer);

        template.afterPropertiesSet();

        return template;
        
    }

}
```

> **RedisUtil**

```java
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @Author lei.hx
 * @Description //Redis工具类
 * @Date 2022/11/21 - 0:27
 * @Version 1.8
 */

@Component
public class RedisUtil {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    // =============================common============================

    /**
     * 指定缓存失效时间
     *
     * @param key  键
     * @param time 时间(秒)
     * @return
     */
    public boolean expire(String key, long time) {
        try {
            if (time > 0) {
                redisTemplate.expire(key, time, TimeUnit.SECONDS);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 根据key 获取过期时间
     *
     * @param key 键 不能为null
     * @return 时间(秒) 返回0代表为永久有效
     */
    public long getExpire(String key) {
        return redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }

    /**
     * 判断key是否存在
     *
     * @param key 键
     * @return true 存在 false不存在
     */
    public boolean hasKey(String key) {
        try {
            return redisTemplate.hasKey(key);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 删除缓存
     *
     * @param key 可以传一个值 或多个
     */
    @SuppressWarnings("unchecked")
    public void del(String... key) {
        if (key != null && key.length > 0) {
            if (key.length == 1) {
                redisTemplate.delete(key[0]);
            } else {
                redisTemplate.delete(String.valueOf(CollectionUtils.arrayToList(key)));
            }
        }
    }

    // ============================String=============================

    /**
     * 普通缓存获取
     *
     * @param key 键
     * @return 值
     */
    public Object get(String key) {
        return key == null ? null : redisTemplate.opsForValue().get(key);
    }

    /**
     * 普通缓存放入
     *
     * @param key   键
     * @param value 值
     * @return true成功 false失败
     */
    public boolean set(String key, Object value) {
        try {
            redisTemplate.opsForValue().set(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    /**
     * 普通缓存放入并设置时间
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒) time要大于0 如果time小于等于0 将设置无限期
     * @return true成功 false 失败
     */
    public boolean set(String key, Object value, long time) {
        try {
            if (time > 0) {
                redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
            } else {
                set(key, value);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 递增
     *
     * @param key   键
     * @param delta 要增加几(大于0)
     * @return
     */
    public long incr(String key, long delta) {
        if (delta < 0) {
            throw new RuntimeException("递增因子必须大于0");
        }
        return redisTemplate.opsForValue().increment(key, delta);
    }

    /**
     * 递减
     *
     * @param key   键
     * @param delta 要减少几(小于0)
     * @return
     */
    public long decr(String key, long delta) {
        if (delta < 0) {
            throw new RuntimeException("递减因子必须大于0");
        }
        return redisTemplate.opsForValue().increment(key, -delta);
    }

    // ================================Map=================================

    /**
     * HashGet
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return 值
     */
    public Object hget(String key, String item) {
        return redisTemplate.opsForHash().get(key, item);
    }

    /**
     * 获取hashKey对应的所有键值
     *
     * @param key 键
     * @return 对应的多个键值
     */
    public Map<Object, Object> hmget(String key) {
        return redisTemplate.opsForHash().entries(key);
    }

    /**
     * HashSet
     *
     * @param key 键
     * @param map 对应多个键值
     * @return true 成功 false 失败
     */
    public boolean hmset(String key, Map<String, Object> map) {
        try {
            redisTemplate.opsForHash().putAll(key, map);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * HashSet 并设置时间
     *
     * @param key  键
     * @param map  对应多个键值
     * @param time 时间(秒)
     * @return true成功 false失败
     */
    public boolean hmset(String key, Map<String, Object> map, long time) {
        try {
            redisTemplate.opsForHash().putAll(key, map);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @return true 成功 false失败
     */
    public boolean hset(String key, String item, Object value) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @param time  时间(秒) 注意:如果已存在的hash表有时间,这里将会替换原有的时间
     * @return true 成功 false失败
     */
    public boolean hset(String key, String item, Object value, long time) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 删除hash表中的值
     *
     * @param key  键 不能为null
     * @param item 项 可以使多个 不能为null
     */
    public void hdel(String key, Object... item) {
        redisTemplate.opsForHash().delete(key, item);
    }

    /**
     * 判断hash表中是否有该项的值
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return true 存在 false不存在
     */
    public boolean hHasKey(String key, String item) {
        return redisTemplate.opsForHash().hasKey(key, item);
    }

    /**
     * hash递增 如果不存在,就会创建一个 并把新增后的值返回
     *
     * @param key  键
     * @param item 项
     * @param by   要增加几(大于0)
     * @return
     */
    public double hincr(String key, String item, double by) {
        return redisTemplate.opsForHash().increment(key, item, by);
    }

    /**
     * hash递减
     *
     * @param key  键
     * @param item 项
     * @param by   要减少记(小于0)
     * @return
     */
    public double hdecr(String key, String item, double by) {
        return redisTemplate.opsForHash().increment(key, item, -by);
    }

    // ============================set=============================

    /**
     * 根据key获取Set中的所有值
     *
     * @param key 键
     * @return
     */
    public Set<Object> sGet(String key) {
        try {
            return redisTemplate.opsForSet().members(key);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 根据value从一个set中查询,是否存在
     *
     * @param key   键
     * @param value 值
     * @return true 存在 false不存在
     */
    public boolean sHasKey(String key, Object value) {
        try {
            return redisTemplate.opsForSet().isMember(key, value);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 将数据放入set缓存
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 成功个数
     */
    public long sSet(String key, Object... values) {
        try {
            return redisTemplate.opsForSet().add(key, values);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 将set数据放入缓存
     *
     * @param key    键
     * @param time   时间(秒)
     * @param values 值 可以是多个
     * @return 成功个数
     */
    public long sSetAndTime(String key, long time, Object... values) {
        try {
            Long count = redisTemplate.opsForSet().add(key, values);
            if (time > 0) {
                expire(key, time);
            }
            return count;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 获取set缓存的长度
     *
     * @param key 键
     * @return
     */
    public long sGetSetSize(String key) {
        try {
            return redisTemplate.opsForSet().size(key);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 移除值为value的
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 移除的个数
     */
    public long setRemove(String key, Object... values) {
        try {
            Long count = redisTemplate.opsForSet().remove(key, values);
            return count;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
    // ===============================list=================================

    /**
     * 获取list缓存的内容
     *
     * @param key   键
     * @param start 开始
     * @param end   结束 0 到 -1代表所有值
     * @return
     */
    public List<Object> lGet(String key, long start, long end) {
        try {
            return redisTemplate.opsForList().range(key, start, end);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取list缓存的长度
     *
     * @param key 键
     * @return
     */
    public long lGetListSize(String key) {
        try {
            return redisTemplate.opsForList().size(key);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 通过索引 获取list中的值
     *
     * @param key   键
     * @param index 索引 index>=0时， 0 表头，1 第二个元素，依次类推；index<0时，-1，表尾，-2倒数第二个元素，依次类推
     * @return
     */
    public Object lGetIndex(String key, long index) {
        try {
            return redisTemplate.opsForList().index(key, index);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public boolean lSet(String key, Object value) {
        try {
            redisTemplate.opsForList().rightPush(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒)
     * @return
     */
    public boolean lSet(String key, Object value, long time) {
        try {
            redisTemplate.opsForList().rightPush(key, value);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public boolean lSet(String key, List<Object> value) {
        try {
            redisTemplate.opsForList().rightPushAll(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒)
     * @return
     */
    public boolean lSet(String key, List<Object> value, long time) {
        try {
            redisTemplate.opsForList().rightPushAll(key, value);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 根据索引修改list中的某条数据
     *
     * @param key   键
     * @param index 索引
     * @param value 值
     * @return
     */
    public boolean lUpdateIndex(String key, long index, Object value) {
        try {
            redisTemplate.opsForList().set(key, index, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 移除N个值为value
     *
     * @param key   键
     * @param count 移除多少个
     * @param value 值
     * @return 移除的个数
     */
    public long lRemove(String key, long count, Object value) {
        try {
            Long remove = redisTemplate.opsForList().remove(key, count, value);
            return remove;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

}
```

测试

```java
	@Autowired
    @Qualifier("redisTemplate")
    private RedisTemplate redisTemplate;

    @Autowired
    private RedisUtil redisUtil;

    @Test
    public void redisUtiltest() {
        User user = new User();
        user.setAge(23);
        user.setName("lei");
        redisUtil.set("info", user);
        System.out.println("\033[33m[MSG]" + redisUtil.get("info") + "\033[0m");
    }
```

![image-20221121003540831](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20221121003540831.png)

---

​	

## 8 Redis 配置文件

> 单位

` vim redis.conf`

:arrow_right: 配置文件对unit不区分大小写

![image-20230124171502636](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20230124171502636.png)

> 包含

:arrow_right: 包含指定文件夹

![image-20230124171806010](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20230124171806010.png)

> 网络

```bash
bind 127.0.0.1  # 绑定的ID
protected-mode no/yes  # 保护模式
port 6379 # 端口
```

![image-20230124171919443](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20230124171919443.png)

> 通用配置

```bash
daemonize yes  # 以守护进程方式运行，默认是no，使用前需要改为yes

pidfile /var/run/redis_6379.pid  # 如果以后台方式运行，需要指定一个pid

# 日志级别
# Specify the server verbosity level.
# This can be one of:
# debug (a lot of information, useful for development/testing)
# verbose (many rarely useful info, but not a mess like the debug level)
# notice (moderately verbose, what you want in production probably)默认生产环境
# warning (only very important / critical messages are logged)
loglevel notice

logfile ""  # 日志的文件位置名

databases 16  # 数据库的数量，默认为16个数据库

always-show-logo no  # 是否总是显示logo
```

> 快照

持久化，在规定的时间内，执行了多少次操作，则会持久化到文件（.rdb  .aof ）。redis是内存数据库，如果没有持久化，则数据断点则失。

![image-20230124172737224](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20230124172737224.png)

```bash
save 900 1   # 如果900秒内，至少有1个key进行了修改，就进行持久化操作
save 300 10   # 如果300秒内，至少有10个key进行了修改，就进行持久化操作
save 60 10000   # 如果60秒内，至少有10000个key进行了修改，就进行持久化操作

stop-writes-on-bgsave-error yes  # 持久化出错是否继续工作

rdbcompression yes # 是否压缩rdb文件，需要消耗一些CPU资源

rdbchecksum yes  # 保存rdb文件时，进行错误的检查

dir ./  # rdb文件保存的目录
```

> 主从复制

![image-20230124173438149](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20230124173438149.png)

> 安全

![image-20230124173558866](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20230124173558866.png)

```bash
requirepass 123456  # 设置密码为123456
```

或者通过命令设置密码

```bash
127.0.0.1:6379> config set requirepass "123456"   # 设置密码
OK
127.0.0.1:6379> auth 123456  # 登录验证密码
OK
127.0.0.1:6379> config get requirepass  # 获取密码
1) "requirepass"
2) "123456"
```

> 限制CLIENTS

![image-20230124174451226](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20230124174451226.png)

```bash
maxclients 10000  # 设置能连接上redis的最大客户端数量
maxmemory <bytes>  # redis配置最大的内存容量
maxmemory-policy noeviction  # 内存到达上限后的处理策略
							# 1.移除一些过期的key
							# 2.报错
							# 3.……
```

> APPEND ONLY MODE模式 aof配置

![image-20230124174851398](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20230124174851398.png)

```bash
appendonly no  # 默认不开启aof模式，默认使用rdb方式持久化，大部分情况下rdb已够用

appendfilename "appendonly.aof"  # 持久化文件的名字

# appendfsync always  # 每次修改都会sync(同步)，消耗性能
appendfsync everysec  # 每秒执行一次 sync（同步），可能会丢失这1秒的数据
# appendfsync no      # 不同步，这个时候操作系统自己同步数据，速度最快
```

具体配置如后文……

---

​	

## 9 Redis持久化

