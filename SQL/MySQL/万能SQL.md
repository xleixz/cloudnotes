# 万能SQL

[toc]

---

## 1 查询指定数据库，指定表中的字段名称、类型、注释

```mysql
SELECT
	TABLE_NAME AS '表名',
	COLUMN_NAME AS '字段名',
	COLUMN_TYPE AS '字段类型',
	COLUMN_COMMENT AS '注释' 
FROM
	information_schema.COLUMNS
WHERE
	TABLE_SCHEMA = '' 
	AND TABLE_NAME = '';
```

