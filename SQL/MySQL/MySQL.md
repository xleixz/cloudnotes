# MySQL

![Mysql_logo](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/Mysql_logo.png)

[TOC]

---

## 1 数据库介绍

### 1.1 DB

- 数据库（database）：存储数据的“仓库”。他保存了一系列有组织的数据。

### 1.2 DBMS

- 数据库管理系统（Database Management System）。数据库是通过DBMS创建和操作的容器。
- 常见的数据库管理系统：MySQL、Oracle（甲骨文）、DB2、SqlServer等。

### 1.3 SQL

- 结构化查询语言（Structure Query Language）：专门用来与数据库通信的语言。



## 2 MySQL常见命令与语法规范

### 2.1 MySQL常见命令

- 查看当前所有数据库

```mysql
show database;
```

- 打开指定额库

```mysql
use 库名
```

- 查看当前库的所有表

```mysql
show tables;
```

- 查看其他库的所有表

```mysql
show tables from 库名;
```

- 查看当前所在库

```mysql
select database();
```

- 创建表

```mysql
create table 表名(
  列名 列类型,
  列名 列类型,
  ...);
use mysql;
create table study01(
		id int,
		name varchar(20));
show tables from mysql;




	create table study02(
	id int);
	show tables from mysql;
```

- 查看表结构

```mysql
desc 表名;
```

![20220425115648](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425115648.png)

- 增加数据

```mysql
insert into 表名(属性) values('值');
insert into study01(id,name) values(1,'join');
```

![20220425115655](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425115655.png)

- 删除数据

```mysql
delete from study01 where id = 1;
```

![20220425115703](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425115703.png)

- 修改数据

```mysql
update study01 set name = 'lilei' where id = 1;
```

![20220425115716](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425115716.png)

- 查看表数据

```mysql
select * from study01;
```

![20220425115722](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425115722.png)



### 2.2 MySQL的语法规范

1. 不区分大小写，但是建议关键字大写，表名、列名小写。
2. 每条命令用分号结尾。
3. 每行命令根据需要可以缩进或换行。
4. 注释

5. 1. 单行注释：`#注释文字`
   2. 单行注释：`-- 注释文字`
   3. 多行注释：`/* 注释文字 */`



## 3 myemployess库的四张表介绍

### 3.1 四类SQL语言

（1）DQL语言

> DQL（Data Query Language）：数据查询语言  -->查

基础查询

条件查询

排序查询

常见函数

分组函数

分组查询

连接查询

子查询

分页查询

union联合查询

（2）DML语言

> DML：数据操作语言  -->增、删、改

插入语句

修改语句

删除语句

（4）DDL语言

> DDL：库和表的定义

库和表的管理

常见数据类型介绍

常见约束

（5）TCL语言

> TCL：事务控制语言

事务和事务处理

## 4 查询

### 4.1 基础查询

**语法与特点**

语法：

> `select 查询列表 from 表名;`

特点：

> 1. 查询列表可以是：表中的字段、常量、表达式、函数。
> 1. 查询的结果是一个虚拟的表格。

#### 4.1.1 查询

查询表中的字段：

> `select 字段 from 表名`

查询表中的多个字段：

> `select 字段，字段，字段 from 表名;`

查询表中的所有字段：

> `select 字段，字段，..... from 表名;`（可单击选择）

> 或    `select * from 表名;`（按顺序查询）

**常见细节**

做查询之前先启用指定的库：

> `USE 库名;`

着重号的使用：

> ![20220425131256](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425131256.png)

执行：

> 选中命令执行。

#### 4.1.2 查询常量值

查询常量值：

```mysql
select 100;
select 'join';
```

> ![20220425131425](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425131425.png)

#### 4.1.3 查询表达式

查询表达式：

```mysql
select 100*98;
```

> ![20220425131516](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425131516.png)

#### 4.1.4 查询函数

查询函数：

```mysql
select version();
```

> ![20220425131544](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425131544.png)

#### 4.1.5 起别名

起别名：(便于理解)（方式一：使用as）

```mysql
select 100*98 as 结果;
```

> ![20220425131611](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425131611.png)



```mysql
select last_name as 姓, first_name as 名 from 表名字;
```

![20220425131642](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425131642.png)



起别名：（方式二：空格）

```mysql
select last_name  姓, first_name  名 from 表名字;
```

> ![20220425131710](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425131710.png)



```mysql
select 100*98  结果;
```

![20220425131732](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425131732.png)

**案例**

查询salary ，显示结果为out put

```mysql
select salary as "out put" from 表名;
```

> 显示结果加上""，避免出现与关键词同名
>
> ![20220425131742](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425131742.png)

#### 4.1.6 去重  DISTINCT

去重关键字：`DISTINCT`

> 过滤重复的数据

![20220425131901](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425131901.png)

#### 4.1.7 +号的作用

**MySQL中的+号仅仅是运算符功能。**

- 如果其中一方为字符型，试图将字符型转化为数值型。
  - 如果转换成功，则继续做加法运算。
  - 如果转换失败，则将字符型数值转换为0。
- 如果有一方为null，则结果肯定为null。

![20220425131945](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425131945.png)

#### 4.1.8 concat函数

> 函数：concat 

用于实现连接

![20220425132042](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425132042.png)

#### 4.1.9 IFNULL函数

> 函数 ：IFNULL

防止出现null值导致查询的信息全为null

![20220425132134](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425132134.png)

### 4.2 排序查询

#### 4.2.1 条件查询语法结构

语法：

```mysql
select
 			查询列表
from 	
			表名
where
			筛选条件;
```

分类：

- 按条件表达式筛选[条件运算符的使用](https://www.yuque.com/xleixz/biancheng/cahpmv?view=doc_embed)
  - 条件运算符：`>` `<` `=`  `<>(不等于)`
- 按逻辑表达式筛选[逻辑运算符的使用](https://www.yuque.com/xleixz/biancheng/syy6yi?view=doc_embed)
  - 逻辑运算符：`&&` `||` `！` `and or not`
- 模糊查询[模糊查询](https://www.yuque.com/xleixz/biancheng/uwc4tr?view=doc_embed)
  - [like](https://www.yuque.com/xleixz/biancheng/uwc4tr#dns5r)
  - [between and](https://www.yuque.com/xleixz/biancheng/uwc4tr#tSvPD)
  - [in](https://www.yuque.com/xleixz/biancheng/uwc4tr#hiKFO)
  - [is null](https://www.yuque.com/xleixz/biancheng/uwc4tr#rPjdU)

#### 4.2.2 条件运算符的使用

> 1. 查询价格>20的书籍

```mysql
select * from tb_books where book_price>20;
```

![20220425132412](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425132412.png)

> 2. 查询书籍编号不等于3的书籍

```mysql
select * from tb_books where ID <>3;
```

![20220425132432](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425132432.png)

#### 4.2.3 逻辑运算符的使用

- 按条件表达式筛选
  - 简单的条件运算符：`>``<``!=``<>``>=``<=`
- 按逻辑表达式筛选
  - 逻辑运算符
    - 作用：用于连接条件表达式
      - `&&``||``!`
      - `and or not`
    - `&&` 和`and`：两个条件都为TRUE，结果为TRUE，反之为FALSE
    - `||`或`or`：只要有一个条件为TRUE，结果为TRUE，反之为FALSE
    - `!`或`not`：如果连接的条件本身为FALSE，结果为TRUE，反之为FALSE

案例1

> 查询图书表中书籍价格大于40小于60的书籍

![20220425132515](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425132515.png)



案例2

> 查询书籍ID不在3~10之间，或者书籍价格高于40的书籍信息

![20220425132525](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425132525.png)

#### 4.2.4 模糊查询



> like
> between and
> in 
> si null | is not null

##### （1）like

注意

- 字符串要用`""`
- 要使用通配符
  - 通配符
    - `%`任意多个字符，包含0个字符（%指在这个字符的前后还有其他字符）

    - `_`任意单个字符

**案例**

案例1

> 查询书籍中出版社包含字符“江苏”的书籍信息

![20220425133411](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425133411.png)



案例2

> 查询书籍中ISBN中第1个字符为9，第8个字符为3的书籍信息

![20220425133430](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425133430.png)



案例3

![20220425133436](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425133436.png)

**拓展**

转义字符不一定是`\`，当自定义转义字符时，需要`escape ""`进行转义

![20220425133446](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425133446.png)

##### （2）between and

**注意**

> 包含临界值
>
> 两个临界值不能调换顺序

![20220425133502](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425133502.png)

**案例**

案例1

> 查询书籍ID在3~10之间的书籍信息

![20220425133511](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425133511.png)

##### （3）in

**注意**

- `in`列表的值类型必须统一或兼容
- 不支持通配符的使用

![20220425133520](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425133520.png)

**案例**

案例1

> 查询书籍的书籍ISBN为978-7-302-23755-6和978-7-300-11134-6的书籍信息

![20220425133535](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425133535.png)

##### （4）is null

注意

- `=`或`<>`不能用于判断null值
- `is null`和`is not null`可以判断null值

**案例**

案例1

> 查询没有借书人姓名的书籍信息

![20220425133549](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425133549.png)



案例2

> 查询有借书人姓名的书籍信息

![20220425133556](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425133556.png)

**拓展：安全等于**

> `<=>`

**案例**

案例1

> 查询没有借书人姓名的书籍信息     <=>

![20220425133607](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425133607.png)



案例2

> 查询书籍价格为59元的书籍信息    <=>

![20220425133623](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425133623.png)

##### （5）is null和安全等于<=>的区别

`is null`仅仅可以判断Null值==（推荐使用）==

<=>既可以判断NUll值，又可以判断普通的数值，但可读性低



