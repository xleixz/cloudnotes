# MyBatisPlus

> MyBatisPlus节省了大量的工作时间，所有的CRUD代码都被自动化完成了。

## 一、特性

- 无侵入：在MyBatis的基础上，只做增强，不做改变。
- 损耗小：启动即会自动注入CURD，性能基本无损耗，直接面向对象操作，BaseMapper构造函数。
- 强大的CRUD操作：内置通用Mapper、通用Service，仅仅通过少量配置即可实现表单大部分CRUD操作，更有强大的条件构造器，满足各类实用需求。
- 支持Lambad形式调用：通过Lambda表达式，方便编写各类查询条件。
- 支持主键自动生成：支持多达4种主键策略，可自由配置。
- 支持ActiveRecord模式：支持ActiveRecord形式调用，实体类只需继承Model类即可进行强大的CRUD操作。
- 支持自定义全局通用操作：支持全局通用方法注入。
- 内置代码生成器：采用代码或者Maven插件可快速生成Mapper、Model、Service、Controller层代码，支持模版引擎。
- 内置分业插件：基于MyBatis物理分页，无需具体操作，配置插件即可完成分页。
- 内置插件支持多种数据库：支持MySQL、Oracle、SQLService等多种数据库。
- 内置性能分析插件：可输出sql语句以及其执行时间。
- 内置全局拦截插件：提供全表delete、update操作执行分析阻断，也可以自定义拦截规则。



## 二、快速使用

注入MyBatisPlus依赖，注意：MyBatis和MyBatisPlus依赖最好不要同时注入

1、准备一个实体类DTO、DO、VO

2、准备一个mapper接口继承BaseMapper，并添加@Mapper注解，以便于被Spring所绑定

3、需在在主启动类上添加@MapperScan("com.xxx.mapper")扫描mapper文件



## 三、配置日志

所有的sql是不可见的，所以需要配置日志便于查看过程。

可以使用原生的StdOutImpl，也可以使用Slf4或者Log4j

```yaml
#配置日志
mybatis-plus:
	configuration:
		log-imlp: 
```



## 四、CRUD拓展

### 4.1 主键生成策略

> 默认全局唯一ID：ID_WORKER

数据库插入的id的默认值为：全球唯一的id，他的生成方式是分布式系统中生成唯一id的雪花算法。

注解`@TableId(type = IdType.ID_WORKER)`

例如：实体类

```java
@TableId(type = IdType.ID_WORKER)
privete String uid;
```

> 主键自增

注解`@TableId(type = IdType.AUTO)`，且数据库字段一定是自增

> 未设置主键

注解`@TableId(type = IdType.NONE)`

> 手动输入

注解`@TableId(type = IdType.INPUT)`

> 全局唯一ID，UUID

注解`@TableId(type = IdType.UUID)`

> ID_WORKER 字符串表示法

注解`@TableId(type = IdType.ID_WORKER_STR)`

### 4.2 自动填充

注解`@TableField(fill = FieldFill.INSERT)` 表示增加时，自动新增

注解`@TableField(fill = FieldFill.INSERT_UPDATE)` 表示修改时，自动修改

然后自定义一个实现类，实现`MetaObjectHandler`类。

例如：

```java\
public class User {
    @TableField(fill = FieldFill.INSERT)
    private String createTime;

    @TableField(fill = FieldFill.UPDATE)
    private String updateTime;

    // 其他字段...
}
```



```java
@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("开始插入填充...");
        this.strictInsertFill(metaObject, "createTime", LocalDateTime.class, LocalDateTime.now());
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("开始更新填充...");
        this.strictUpdateFill(metaObject, "updateTime", LocalDateTime.class, LocalDateTime.now());
    }
}
```

> FieldFill 枚举

```
public enum FieldFill {

    DEFAULT,       // 默认不处理

    INSERT,        // 插入填充字段

    UPDATE,        // 更新填充字段

    INSERT_UPDATE  // 插入和更新填充字段

}
```

### 4.3 分页查询

1、在 Spring Boot 项目中，通过配置来添加分页插件：

```java
@Configuration
@MapperScan("scan.your.mapper.package")
public class MybatisPlusConfig {

    /**
     * 添加分页插件
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        // interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL)); // 如果配置多个插件, 切记分页最后添加
        // 如果有多数据源可以不配具体类型, 否则都建议配上具体的 DbType
        return interceptor;
    }
}
```

2、直接使用`Page`对象即可

```java
@Test
public void testPage() {
  Page<User> page = new Page<>(1, 5); // 当前页 页面大小
  userMapper.selectPage(page, null);
  
  page.getTotal(); // 总页数
  // ………
}
```

### 4.4 删除操作

userMapper.delete......

### 4.5 逻辑删除

例如：数据库对应的删除标识符为`isDel`，那么在实体类中可以通过注解`@TableLogic`来标识逻辑删除注解

```java
@TableLogic
private Integer isDel;
```

在 `application.yml` 中配置 MyBatis-Plus 的全局逻辑删除属性：

```yaml
mybatis-plus:
  global-config:
    db-config:
      logic-delete-field: deleted # 全局逻辑删除字段名
      logic-delete-value: 1 # 逻辑已删除值
      logic-not-delete-value: 0 # 逻辑未删除值
```

调用删除会执行逻辑删除（update）

```java
userMapper.deleteById(1);
```

同时查询时也会自动添加`isDel`的标识条件

### 4.6 性能分析插件

当遇到慢sql时，可以通过性能分析插件来处理，他的作用是分析每条sql执行的时间，如果超过设置的时间，就会停止运行。<font color="red">为了保证性能，只在测试环境和开发环境中开启！</font>

在MyBatisPlusConfig配置文件中，设置

```java
@Profile({"dev", "test"})
@Bean
public PerforManceInterceptor performanceInterceptor() {
  PerformanceInterceptor performanceInterceptor = new PerformanceInterceptor();
  performanceInterceptor.setMaxTime(1); // ms设置sql执行的最大时间，如果超过了则不执行
  performanceInterceptor.setFormat(true); // 是否开启格式化支持
}
```

### 4.7 条件构造器Wrapper

写一些复杂的sql时，可以使用Wrapper替代。

更多构造器查询官网：https://baomidou.com/guides/wrapper/

查询构造器

```java
QueryWrapper<User> wrapper = new QueryWrapper<>();
wrapper.isNotNull("name") // name不为空
  		 .isNotNull("email") // 邮箱email不为空
  		 .ge("age", 12) // >= 12
```

修改构造器

```java
UpdateWrapper<User> wrapper = new UpdateWrapper<>();
updateWrapper.set("name", "老李头");
```

子查询构造器

```java
QueryWrapper<User> wrapper = new QueryWrapper<>();
wrapper.isSql("name", "select name from user where name like '%李%'")
```

排序

```java
QueryWrapper<User> wrapper = new QueryWrapper<>();
wrapper.orderByDesc(); // 降序
wrapper.orderByASC(); // 升序
```

### 4.8 代码自动生成器

官网：https://baomidou.com/guides/new-code-generator/

依赖

```xml
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-generator</artifactId>
    <version>3.5.9</version>
</dependency>
```



完结end🎉
