# Linux查看nohup.out文件内容

1. 前台实时查看nohub.out文件内容：

```bash
tail -f nohup.out 
```

2. 根据关键字查看

```bash
tail -f nohup.out |grep "关键字"
```

3. 输出文件最后100行

```bash
tail -n 100 nohup.out 
```

4. 输出文件最后100行，含关键字

```bash
tail -n 100 nohup.out |grep "关键字"
```

