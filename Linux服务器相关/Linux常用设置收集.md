# Linux常用设置收集

[TOC]

---

## 1. 查看云盘的使用容量

```bash
df -h
```

![image-20230918100001315](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20230918100001315.png)

## 2. 登录linux系统设置默认目录

切换到root目录下，找到`.bashrc`文件

```shel
cd /root/
```

修改`.bashrc`文件，在文件的末尾添加，添加的`cd /test`就会登录后默认到`/test`目录下

```shell
cd /test
```

