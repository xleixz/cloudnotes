# Linux系统安装jdk

> 需要的工具：MobaXterm
>
> 官网下载地址：[https://mobaxterm.mobatek.net/download.html](https://mobaxterm.mobatek.net/download.html "点击去官网下载MobaXterm")
>
> 汉化版MobaXterm：[https://lhxi.lanzoul.com/b017y0n9a      密码:hdko](https://lhxi.lanzoul.com/b017y0n9a "点击下载 MobaXterm汉化版")

​	

1、到Oracle官网下载需要的jdk版本到本机，jdk下载地址：[Java Downloads | Oracle](https://www.oracle.com/java/technologies/downloads/#java8 "点击到Oracle下载jdk")；

![image-20220725153717009](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220725153717009.png)

2、用`rz`命令将jdk上传到服务器；

```java
[root@xl ~]# rz
-bash: rz: 未找到命令
```

<font color='red'>没有安装rz的就先执行 `yum install lrzsz`</font>

```java
[root@xl ~]# yum install -y lrzsz
```

等待安装完毕后，继续用`rz`命令将jdk上传到服务器；

3、上传完毕后，解压jdk

`sudo tar -vxf jdk-8u341-linux-x64.tar.gz -home/xl`

4、配置环境变量 `sudo vim /etc/profile`，添加如下信息到配置文件中，注意jdk版本号和路径

```java
export JAVA_HOME=/home/xl/jdk1.8.0_341

export JRE_HOME=${JAVA_HOME}/jre

exportCLASSPATH=.:${JAVA_HOME}/lib:${JRE_HOME}/lib

export PATH=${JAVA_HOME}/bin:$PATH
```

> 按"i"编辑，编辑完成后按"esc"，接着输入":wq"保存退出

5、让配置文件生效 `source /etc/profile`

6、测试是否成功 `java -version`

```java
[root@xl ~]# source /etc/profile
[root@xl ~]# java -version
java version "1.8.0_341"
Java(TM) SE Runtime Environment (build 1.8.0_341-b13)
Java HotSpot(TM) 64-Bit Server VM (build 25.341-b13, mixed mode)
```

​	

出现版本号即配置成功，jdk也就配置完成了。

