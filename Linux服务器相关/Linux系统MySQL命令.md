# Linux系统安装和卸载MySQL

> 需要的工具：MobaXterm
>
> 官网下载地址：[https://mobaxterm.mobatek.net/download.html](https://mobaxterm.mobatek.net/download.html "点击去官网下载MobaXterm")
>
> 汉化版MobaXterm：[https://lhxi.lanzoul.com/b017y0n9a      密码:hdko](https://lhxi.lanzoul.com/b017y0n9a "点击下载 MobaXterm汉化版")

​	

## 1 、安装 mysql 

### 1.1 下载MySQL安装包

1. 打开MySQL官网下载MySQL环境资源包 [https://www.mysql.com/downloads/](https://www.mysql.com/downloads/ "点击到mysql官网下载")，选择**MySQL Community (GPL) Downloads »**；

   ![image-20220901175505384](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220901175505384.png)

2. 选择**MySQL Yum Repository**；

   ![image-20220901175707966](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220901175707966.png)

3. 根据服务器的版本下载对应的版本，这里以我的Centos7 为例；

   ![image-20220901175819211](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220901175819211.png)

4. 跳转到登录页面，点击下方**No thanks, just start my download.**

   ![image-20220901175909371](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220901175909371.png)

### 1.2 安装 mysql

1. 将下载好的`mysql80-community-release-el7-7.noarch.rpm` rpm文件上传到服务器上；

2. 使用命令安装mysql ；

   ```bash
   yum localinstall mysql80-community-release-el7-7.noarch.rpm
   ```

3. 安装完毕后，继续安装mysql服务；

   ```bash
   yum install mysql-community-server
   ```

4. 安装完毕后，启动mysql服务；

   ```bash
   service mysqld start
   ```

5. 查看mysql服务是否启动成功；

   ```bash
   ps -ef|grep mysql
   ```

   ![image-20220901180736818](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220901180736818.png)

### 1.3 修改密码

1. 查询mysql的临时密码；

   ```bash
   grep 'temporary password' /var/log/mysqld.log
   ```

   ![image-20220901180839882](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220901180839882.png)

2. 复制临时密码，登录mysql；

   ```bash
   mysql -u root -p
   ```

3. 修改密码；

   ```bash
   ALTER USER 'root'@'localhost' IDENTIFIED BY '在这里输入你要设置的密码';
   ```

4. 刷新权限；

   ```bash
   flush privileges;
   ```

### 1.4 创建新用户

1. 登录mysql；

2. 创建新用户；

   ```bash
   create user '你的用户名'@'%' identified with mysql_native_password by '你的密码';
   ```

3. 刷新权限；

   ```bash
   grant all on *.* to '你的用户名'@'%';
   ```

<font color="green">安装完成！</font>

## 2、 卸载 mysql

1. 查看mysql安装的文件

   ```bash
   rpm -qa |grep -i mysql
   ```

   ![image-20220901181357276](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220901181357276.png)

2. 用以下命令卸载**所有的**mysql文件

   ```bash
   yum remove mysql-community-libs-8.0.30-1.el7.x86_64
   ```

3. 再次查看mysql安装的文件，确保卸载干净

4. 查找mysql相关目录

   ```bash
   find / -name mysql
   ```

   ![image-20220901181612640](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220901181612640.png)

5. 删除**所有**相关目录

   ```bash
   rm -rf /usr/lib64/mysql
   ```

6. 删除/etc/my.cnf

   ```bash
   rm -rf /etc/my.cnf
   ```

7. 删除/var/log/mysqld.log（如果不删除这个文件，会导致新安装的mysql无法生存新密码，导致无法登陆）

   ```bash
   rm -rf /var/log/mysqld.log
   ```

<font color="green">卸载完成！</font>

## 3、修改MySQL密码策略

查看密码策略

```bash
SHOW VARIABLES LIKE 'validate_password%';
```

设置密码策略

```bash
set global validate_password.policy=LOW;
```

<font color='red'>重启MySQL后会失效！</font>

## 4、一些报错问题

### 4.1 、#1227 – Access denied; you need (at least one of) the SYSTEM_USER privilege(s) for this ...

> 1227 - Access denied; you need (at least one of) the SYSTEM_USER privilege(s) for this operation

原因是由于root用户没有SYSTEM_USER权限，把权限加入后即可解决：

```mysql
grant system_user on *.* to 'root';
```

