# CentOS (Linux) 设置中文环境

[toc]

---

#### 1 首先查看当前字符集

```shell
locale
```

如果是 LC_CTYPE="en_US.UTF-8"

#### 2 看看有没有zh_CN.utf8

```shell
locale -a |grep CN
```

没有就安装中文配置

```shell
yum install -y langpacks-zh_CN
```

#### 3 安装后配置环境

打开配置文件

```shell
sudo vim /etc/locale.conf
```

更改语言配置

```shell
LANG="zh_CN.UTF-8"
```

#### 4 重启系统

```shell
reboot
```