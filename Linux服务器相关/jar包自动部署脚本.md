# jar包自动部署脚本

```bash
#!/bin/bash

APP_NAME=account_book
JAR_PATH="/home/project/account_book"
JAR_NAME="$APP_NAME"
ACTIVE=prod
JASYPT_ENCRYPTOR_PASSWORD=EbfYkitulv73I2p0mXI50JMXoaxZTKJ0

PID="$JAR_PATH/$APP_NAME.pid"
LOG_DIR="$JAR_PATH/out"

usage() {
  echo "Usage: $0 [start|stop|restart|status]"
  exit 1
}

is_exist() {
  # 精确匹配进程（避免误杀其他进程）
  pid=$(ps -ef | grep "[j]ava.*$JAR_NAME.jar" | awk '{print $2}')
  [ -z "$pid" ] && return 1 || return 0
}

start() {
  cd "$JAR_PATH" || { echo "Error: 无法进入目录 $JAR_PATH"; exit 1; }
  is_exist
  if [ $? -eq 0 ]; then
    echo ">>> $APP_NAME 正在运行 (PID=$pid) <<<"
    return 0
  fi

  TIME_NOW=$(date +%Y%m%d%H%M%S)
  mkdir -p "$LOG_DIR" || { echo "Error: 无法创建日志目录 $LOG_DIR"; exit 1; }
  LOG_FILE="$LOG_DIR/${JAR_NAME}_${TIME_NOW}.out"

  echo ">>> 启动 $APP_NAME ... <<<"
  nohup java -jar "$JAR_NAME.jar" \
    --spring.profiles.active="$ACTIVE" \
    --jasypt.encryptor.password="$JASYPT_ENCRYPTOR_PASSWORD" \
    > "$LOG_FILE" 2>&1 &

  local pid=$!
  echo ">>> 启动中，新进程 PID=$pid <<<"
  
  # 等待并确认进程启动成功
  sleep 2
  if ps -p "$pid" >/dev/null 2>&1; then
    echo "$pid" > "$PID"
    echo ">>> 启动成功 PID=$pid <<<"
  else
    echo ">>> 启动失败，请检查日志 $LOG_FILE <<<"
    exit 1
  fi
}

stop() {
  local pid
  if [ -f "$PID" ]; then
    pid=$(cat "$PID")
    if ps -p "$pid" >/dev/null 2>&1; then
      echo ">>> 停止 $APP_NAME (PID=$pid) ... <<<"
      kill "$pid"
      
      # 等待进程退出，最多5秒
      local count=0
      while [ $count -lt 5 ] && ps -p "$pid" >/dev/null; do
        sleep 1
        ((count++))
      done

      if ps -p "$pid" >/dev/null; then
        echo ">>> 强制停止 PID=$pid ... <<<"
        kill -9 "$pid"
        sleep 2
        ps -p "$pid" >/dev/null && \
          { echo ">>> 错误: 无法停止进程 $pid <<<"; return 1; }
      fi
      echo ">>> $APP_NAME 已停止 <<<"
      rm -f "$PID"
    else
      echo ">>> 进程 $pid 不存在，清理PID文件 <<<"
      rm -f "$PID"
    fi
  else
    is_exist
    if [ $? -eq 0 ]; then
      echo ">>> 发现未管理的 $APP_NAME 进程 (PID=$pid)，尝试停止... <<<"
      kill "$pid"
      sleep 2
      is_exist && { kill -9 "$pid"; sleep 2; }
      is_exist && { echo ">>> 错误: 无法停止进程 $pid <<<"; return 1; }
      echo ">>> $APP_NAME 已停止 <<<"
    else
      echo ">>> $APP_NAME 未运行 <<<"
    fi
  fi
}

status() {
  is_exist
  if [ $? -eq 0 ]; then
    echo ">>> $APP_NAME 正在运行 (PID=$pid) <<<"
  else
    echo ">>> $APP_NAME 未运行 <<<"
  fi
}

restart() {
  stop
  start
}

case "$1" in
  start)    start ;;
  stop)     stop ;;
  status)   status ;;
  restart)  restart ;;
  *)        usage ;;
esac

exit 0

```

