# JavaWeb

[toc]

## Servlet

## 1 基本技术内容与分析

### 1.1 内容介绍

- JavaWeb（Servlet为核心）
- JSP
- Ajax（异步通信技术）
- IQuery（JavaScript库）
- MyBatis
- Spring
- SpringMVC
- SpringBoot
- SpringCLoud
- ……

### 1.2 需要准备的技术

- JavaSE（java语言的标准版，java提供的最基本的类库）
- MySQL（数据库）
   - 最基本的增删改查SQL语句
- JDBC（java语言连接数据库）
   - 这是一套java语言连接数据的接口
- Web前端语言
   - HTML（网页）
   - CSS（层叠样式表语言，修饰HTML）
   - JavaScript（一种脚本语言，运行在浏览器中，让浏览器中的元素可以增删改，让网页产生更强交互效果）
- Web后端
   - Servlet（Sever Applet：服务端的java小程序）
   - JSP
   - AJAX
   - JQuery
   - Spring
   - SpringMVC
   - SpringBoot
   - SpringCloud
   - ……

---

​	

## 2 关于系统架构

### 2.1 系统架构包括什么形式

- C/S架构
- B/S架构

### 2.2 C/S架构

- Client/Sever （客户端/服务器）
- 特点
  - 需要安装特定的软件
- 优点：
  - 速度快
  - 体验好
  - 界面炫酷
  - 安全
  - 服务器压力小
  - ……
- 缺点：
  - 升级维护比较麻烦，每一个客户端软件都需要升级，有一些软件不宜安装

#### 2.2.1 B/S架构

- B/S（Browser/Sever，浏览器/服务器）
- http://www.baidu.com
- ……
- B/S结构的系统实际上还是一个C/S，只不过这个特殊的C/S比较特殊，这个Client是一个固定不变的浏览器软件
- 优点：
  - 升级维护方便，成本低，只需要升级服务器即可
- 缺点：
  - 体验差
  - 不安全
  - 速度慢
  - ……

#### 2.2.2 C/S和B/S结构的系统哪个好？

1. 娱乐软件建议使用C/S
1. 公司内部使用一些业务软件建议使用B/S，方便维护

#### 2.2.3 B/S系统的本质

- 开发B/S系统其实就是开发一个Web

#### 2.2.4 JavaSE，JavaEE，JavaME

- Java包括三大块：
  - JavaSE
    - java标准版（一套类库，被人写好的类库）
  - JavaEE
    - java企业版（也是一套类库，也是别人写好的类库，专门为企业内部提供解决方案的一套类库，可以开发企业级项目）
    - 比较火爆
    - 13种规范，其中Servlet就是JavaEE规范之一，学Servlet还是学Java语言
  - JavaME
    - java微型版（也是一套类库，只不过这套类库帮助我们进行电子微型设备内的程序的开发）
    - 微波炉，电冰箱

---

​	

## 3 关于Web服务器的软件

### 3.1 Web服务器软件有哪些？

- Tomcat（Apache）
- jetty（Web服务器）
- JBoss（应用服务器）

> 应用服务器和Web服务器的关系

- 应用服务器实现了javaEE的所有规范
- Web服务器只实现了javaEE中的Servlet＋JSP两个核心的规范

### 3.2 Tomcat

#### 3.2.1 下载

- [Apache官网](https://www.apache.org/)
- [Tomcat官网](https://tomcat.apache.org/)
- Tomcat开源免费的轻量级Web服务器
- Tomcat是java写的
- Tomcat服务器想要运行，必须先有jre（java运行时的环境）

#### 3.2.2 Tomcat安装与配置

1. 解压安装
2. 启动Tomcat
   1. bin目录下有一个文件：startup.bat文件，通过他可以启动Tomcat服务器
   1. ...bat文件是Windows操作系统专用的，bat文件是批处理文件，这种文件可以编写大量的Windows的dos命令，然后执行bat文件就相当于在执行dos命令
   1. start.sh 这个文件在Windows中无法执行，他是在Linux中执行
3. 关于Tomcat服务器
   1. bii：Tomcat服务器的命令文件存放的目录，比如启动Tomcat
   1. conf：Tomcat服务器的配置文件存放目录（Servlet.xml文件可以配置端口号，默认Tomcat端口号是8080）
   1. lib：Tomcat服务器的核心程序目录，因为Tomcat服务器是java语言编写的，这里的jar包都是class文件
   1. logs：Tomcat服务器的日志目录，Tomcat服务器启动等信息都会在该目录下生成日志文件
   1. temp：Tomcat服务器的临时目录，存放临时文件
   1. webapps：这个目录是用来存放大量的webapp应用
   1. work：用来存放JSP文件翻译之后的class文件

---

​	

## 4 实现一个Webapp

实现一个基本的Web应用

> 步骤

1. 找到CATALINA_HOME\webapps目录。因为所有的APP要放到该目录下，若不放，则找不到
2. 在CATALINA_HOME\webapps下新建一个子目录oa
   1. 这个oa就是这个webapp的名字
3. 在oa目录下新建资源。例如：index.html
   1. 编写html内容
4. 启动Tomcat服务器
5. 打开浏览器输入url
   1. http://127.0.0.1:8080/oa/index.html

> 思考

在浏览器上直接输入url动作和使用超链接是一样的

```html
<a href="http://127.0.0.1:8080/oa/login.html">user login</a>

<a href="/oa/login.html">user login</a>
```

> 静态资源变动态资源

- 连接数据库需要JDBC程序，也就是需要编写java程序连接数据库，网页内容随着数据库内的数据改变而改变
- 对于一个动态的Web来说，一个请求和响应的过程有多少个角色参与，角色与角色之间有多少个协议

---

​	

## 5 开发一个带有Servlet的webapp（重点）

### 5.1 开发步骤

1. 在webapp目录下新建一个目录，起名crm（这个crm就是webapp的名字）。当然也可以是其他项目，比如银行系统可以创建一个目录bank，办公系统可以创建一个oa
   - 注意：
     - crm就是这个webapp的根
2. 在webapp根目录（crm）下，创建一个目录：WEB-INF
   - 注意：
     - 这个目录的名字是Servlet规范中规定的，必须全部大写，必须一模一样
3. 在WEB-INF目录下新建一个目录：classes
   - 注意：
     - 这个目录的名字必须是全部小写的classes。这也是Servlet规范中规定的，另外这个目录下一定存放的是java程序编译之后的class文件（这里存放的是字节码文件）
4. 在WEB-INF目录下新建一个目录：lib
   - 注意：
     - 这个目录不是必须的，但如果一个webapp需要第三方的jar包的话，这个jar包要放到这个lib下，这个目录的名字也不能随意编写，必须全部小写的lib。例如java语言连接数据库需要数据库的驱动jar包，那么这个jar包一定要放在lib目录下，这个也是Servlet规范中规定的
5. 在WEB-INF目录下新建一个_文件_：web.xml
   - 注意：
     - 这个文件是必须的，这个文件名字必须叫web.xml。这个文件必须放在这里，一个合法的webapp，web.xml是必须的，这个web.xml文件就是一个配置文件，在这个配置文件中描述了请求路径和Servlet类之间的对照关系
     - 这个文件最好从其他的webapp中拷贝，最好别手写，复制粘贴
6. 编写一个Java程序，这个小Java必须实现Servlet接口。
   - 思考？
     - 这个Servlet接口不在jdk中（因为jServlet不是JavaSE了，Servlet属于JavaEE）
     - Servlet接口（Servlet.class文件）是Oracle提供的
     - Servlet接口是JavaEE的规范中的一员
     - Tomcat服务器实现了Servlet规范，所以Tomcat服务器也需要使用Servlet接口，Tomcat服务器中应该有这个接口，Tomcat服务器的CATALINA_HOME\lib目录下有一个servlet-api.jar，解压后会得到一个Servlet.class文件
     - 从JakartaEE10开始之后，Servlet接口的全名变了：jakarta.servlrt.Servlet
   - 注意：
     - 编写这个Java程序的时候，java源码在哪就在哪，位置无所谓，只需要将Java源代码编译之后的class文件放到classes目录下即可
7. 编译编写的HelloServlet
   - 重点：
     - 配置环境变量CLASSPATH=.;路径下的lib\servlet-api.jar
     - 配置环境变量CALSSPATH与没有任何关系，只是为了让java文件成功编译
8. 将以上编译之后的HelloServlet.clss文件拷贝到WEB-INF\classes目录下
9. 在web.xml文件中编写配置信息，让“请求路径”和“Servlet类名”关联在一起
   - 这一步用专业属于描述：在web.xml文件中注册Servlet类

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="https://jakarta.ee/xml/ns/jakartaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="https://jakarta.ee/xml/ns/jakartaee 
                             https://jakarta.ee/xml/ns/jakartaee/web-app_5_0.xsd"
         version="5.0"  
         metadata-complete="true">
  
         <!--servlet描述信息-->
         <!--任何一个Servlet都对应一个servlet-mapping-->      
         <servlet>
            <servlet-name>asdfghjk</servlet-name>
            <servlet-class>Servlet.HelloServlet</servlet-class>
         </servlet>
  
          <!--servlet映射信息-->
         <servlet-mapping>
             <!--这个也是随便的，不过这里写的内容要和上面的一样-->
            <servlet-name>asdfghjk</servlet-name>
           <!---这里需要一个路径-->
            <!---这个路径唯一的要求是必须以 / 开始-->
            <!--当前这个路径可以随便写-->
            <url-pattern>/as/fe/g/h/gh/h</url-pattern>
         </servlet-mapping>
         </web-app>

```

10. 启动Tomcat服务器
10. 打开浏览器，在浏览器地址栏输入一个url，这个URL必须是web.xml文件中的url-pattern一致

   - 浏览器上编写的路径太复杂，可以使用超链接
   - **非常重要：**
     - html页面只能放到WEB-INF目录之外
     - 以后不需要编写main方法了，Tomcat服务器负责调用main方法，Tomcat服务器启动时候，执行的就是main方法，JavaWeb程序员只需要编写Servlet接口实现类，然后将其注册到web.html文件中即可

### 5.2 关于JavaEE的版本

- 目前最高版本是JavaEE8
- JavaEE被Oracle捐献给了Apache
- Apache把JavaEE改成了JakartaEE
- 以后没有JavaEE了，以后都叫Jakarta EE
- JavaEE8版本升级以后的“JavaEE9”不再是这个名字了，叫做JakartaEE9
- JavaEE8的时候对应的Servlet类名是：javax.servlet.Servlet
- JavaEE9的时候对应的Servlet类名是：jakarta.servlet.Servlet

### 5.3 向浏览器响应一段HTML代码

```java
package Servlet;

import jakarta.servlet.Servlet;
import jakarta.servlet.ServletException;
import jakarta.servlet.*;

import java.io.IOException;


import java.io.IOException;
import java.io.PrintWriter;


public class HelloServlet implements Servlet {


    public void init(ServletConfig config) throws ServletException {

    }

    public void service(ServletRequest request, ServletResponse response)
            throws ServletException, IOException {

        //这里往控制台输出
        System.out.println("Hello word!");

        //设置响应的内容类型是普通文本或html代码
        //注意:这是响应的内容类型时，不要在获取流之后设置
        //需要在设置流对象之前使用，有效
        response.setContentType("text/html");
        //若出现乱码:加上charset=utf-8
        //response.setContentType("text/html;charset=utf-8");

        //怎么将一个信息直接输入到浏览器上？？？
        //需要使用ServletResponse接口:response
        //response表示响应向浏览器发送数据叫做响应
        PrintWriter out = response.getWriter();

        //向浏览器上打印
        out.print("Hello Servlet,You are first servlet!");

        //在浏览器上输出一串代码
        out.print("<h1>hello servlet</h1>");

        //这是一个输出流，负责输出数据或字符串到浏览器
        //这个输出流不需要我们刷新，不需要我们关闭，这些都由Tomcat完成
        /*
        out.flush();
        out.close();
         */
    }

    public void destroy() {

    }

    public String getServletInfo() {
        return "";
    }

    public ServletConfig getServletConfig() {
        return null;
    }


}

```

```html
<!doctype html>
<html>
<head>
	<title>index page</title>
</head>
<body>
  <!--<a href="请求路径">
请求路径要带项目名-->
<a href="http://127.0.0.1:8080/crm/as/fe/g/h/gh/h">hello</a>
</body>
</html>
```

### 5.4 请求路径

- [x] 请求路径要带上项目名
  - IDEA中

<img src="https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424151013.png" alt="20220424151013" style="zoom: 33%;" />

### 5.5 总结

> 一个合法的webapp目录结是什么样的？

```xml
webapproot
	|…………WEB-INF
		|…………classes（存放字节码）
		|…………lib（第三方jar包）
		|…………web.xml（注册Servlet）
	|…………html
	|…………css
	|…………JavaScript
	|…………image
……
```

---

​	

## 6 Servlet中编写JDBC程序连接数据库

> 怎么做？

- 在Servlet中直接编写Java代码即可（JDBC）
- 需要将驱动jar包拷贝到webapps\crm\WEB-INF\lib目录下

> 实现连接数据库

```java
package Servlet;

/**
 * @Author: 小雷学长
 * @Date: 2022/3/16 - 10:06
 * @Version: 1.8
 */

import jakarta.servlet.Servlet;
import jakarta.servlet.ServletException;
import jakarta.servlet.*;

import java.io.IOException;


import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;


public class StudentServlet implements Servlet {


    public void init(ServletConfig config)
            throws ServletException {

    }

    public void service(ServletRequest request, ServletResponse response)
            throws ServletException, IOException {

        /*
        打印到浏览器上
         */
        response.setContentType("text/html");
        //返回out
        PrintWriter out = response.getWriter();

        /*
        编写JDBC代码，连接数据，查询所有信息
         */
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            //注册驱动
            Class.forName("com.mysql.cj.jdbc.Driver");

            //获取连接
            String url = "jdbc:mysql://localhost:3306/practice";
            String user = "root";
            String password = "123456";
            conn = DriverManager.getConnection(url, user, password);

            //获取预编译的数据库操作对象
            String sql = "select id,informant_name from t_student";
            ps = conn.prepareStatement(sql);

            //执行SQL
            rs = ps.executeQuery();

            //处理查询结果集
            while (rs.next()) {
                String id = rs.getString("id");
                String informant_name = rs.getString("informant_name");

                //打印out
                out.print(id + "," + informant_name + "<br>");

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //释放资源
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void destroy() {

    }

    public String getServletInfo() {
        return "";
    }

    public ServletConfig getServletConfig() {
        return null;
    }


}

```

---

​	

## 7 使用IDEA工具开发Servlet

> 使用IDEA开发Servlet关键步骤

> 关键步骤

1. 让module变成JavaEE的模块
   1. 在Module点击右键：Add Framework Suppoet……（添加框架支持）
   1. 在弹出的窗口中选择Web Application（选择的是webapp的支持）
   1. 选择了这个webapp的支持之后，IDEA会自动生成一个符合Servlet规范的webapp目录结构
   1. 注意：
      - 在IDEA工具中根据Web Application模板生成的目录中有一个web目录，这个web目录就代表webapp的根
2. 若发现Servlet.class文件没有，则将CATALINA_HOME/lib/servlet-api.jar和jsp-api.jar添加到classpath中（这里的classpath是IDEA的classpath）
   1. IDEA中File---->Project Structure……---->modules---->Dependencies---->增加一个Jars---->添加servlet-api.jar和jsp-api.jar
   1. 实现jakarta.servlet.Serlvet接口的5个方法
3. 在WEB-INF目录下新建一个子目录：lib
   1. 将数据库驱动jar包拷贝到lib目录下
4. 在web.xml文件中完成StudentServlet类的注册（请求路径和Servlet之间对应起来）

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
    
    <servlet>
        <servlet-name>studentSerlvet</servlet-name>
        <servlet-class>Servlet.StudentServlet</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>studentSerlvet</servlet-name>
        <url-pattern>/serfdsfdfsdf</url-pattern>
    </servlet-mapping>
</web-app>
```

5. 给一个html页面，在HTML页面中编写一个超链接，用户点击这个超链接，发送请求，Tomcat执行后台的StudentServlet
   - student.html
     - 这个文件不能放到WEB-INF目录里面，只能放到WEB-INF目录外面
     - student.html的内容

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>student page</title>
</head>
<body>
<!--这里的项目名是 /xmm ，无法动态获取，先写死-->
<a href = "/xmm/servlet/student">点击这里</a>

</body>
</html>
```

6. 让IEDA工具去关联Tomcat服务器。关联过程中将webapp部署到Tomcat服务器中
   1. IDEA工具右上角Add COnfiguration
   1. 左上角加号，点击Tomcat Servlet---->local
   1. 在弹出的界面中设置服务器sever参数（基本不用动）
   1. 在当前窗口中有一个Deployment（点击这个部署webapp），继续点击加号---->Artificial……，部署即可
   1. 修改Application context（应用的根）为：空
7. 启动Tomcat服务器
   1. 在右上角有一个绿色的小虫子（Debug），点击可以采用Debug模式启动Tomcat服务器
   1. 在开发模式中建议使用Debug模式启动Tomcat服务器
8. 打开浏览器，在浏览器地址栏输入：http://localhost:8080/student.html
9. html页面只能放到WEB-INF目录之外

---

​	

## 8 （不理解时看）Servlet步骤简洁概括

> 步骤一

![20220424155339](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424155339.png)

​	

> 步骤二

![20220424155356](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424155356.png)

​	

> 步骤三

![20220424155404](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424155404.png)

---

​	

## 9 Servlet对象的生命周期

> 什么是Servlet对象生命周期？

- Servlet对象是什么时候被创建？
- Servlet对象是什么时候被销毁？
- Servlet对象创建了几个？
- Servlet对象的生命周期表示：一个Servlet对象从出生到最后的死亡，整个过程是怎样的？
- Servlet对象是由谁来维护的？

> Servlet对象和WEB容器

- Servlet对象的创建，对象上方法的调用，对象最终的销毁，程序员是无权干预的
- Servlet对象的生命周期是由Tomcat服务器（WEB Sever）全权负责的
- Tomcat服务器通常我们又称为：WEB容器
- 我们自己new的Servlet对象是不受WEB容器管理的
- WEB容器创建的Servlet对象，这些是Servlet对象都会被放到一个集合当中（HashMap），只有放到这个HashMap集合中的Servlet才能够被WEB容器管理，自己new的Servlet的对象不会被WEB容器管理，（自己new的Servlet独享不在容器当中）
- WEB容器底层应该都有一个HashMap这样的集合，在这个集合当中存储了Servlet对象和请求路径之间的关系

> Servlet对象生命周期

```java
package servlet;

import jakarta.servlet.*;

import java.io.IOException;

/**
 * @Author: 小雷学长
 * @Date: 2022/3/17 - 10:37
 * @Version: 1.8
 */
public class AServlet implements Servlet {

    //无参数构造方法
    public AServlet() {
        System.out.println("A无参数构造方法启动了");
    }

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        System.out.println("A service启动了");
    }

    @Override
    public void destroy() {
        System.out.println("A destory启动了");

    }

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        System.out.println("A inti启动了");
    }

    @Override
    public String getServletInfo() {
        return null;
    }

    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

}

```

- 默认情况下服务器启动时 A Servlet对象并没有被实例化
- 用户发送第一次请求的时候，控制台输出了以下内容：

```
A无参数构造方法启动了
A inti启动了
A service启动了
```

- 根据以上输出的内容得出结论：
  1. 用户在发送第一次请求的时候Servlet对象被实例化（AServlet构造的方法被执行了。并且执行的是无参数构造方法）
  1. A Servlet对象被创建出来之后，Tomcat服务器马上调用了AServlet对象的init方法（init方法在执行的时候，Servlet对象已经存在了，已经被创建出来了）
  1. 无参数构造方法、init方法只在第一次用户发送请求的时候执行，也就是说无参数构造方法只执行一次，init方法只被Tomcat服务器调用一次
  1. 只要用户发送一次请求，service方法必然只会被Tomcat定位器调用一次，发送100次请求，service方法会被调用100次
  1. 关闭服务器的时候，控制台输出了以下内容

```
A destory启动了
```

   - 通过以上输出内容，可以得出一下结论：
     - Servlet的destroy方法只被Tomcat服务器用一次
     - destroy方法在服务器关闭的时候被调用，因为服务器关闭的时候要销毁AServlet对象的内存
     - 服务器在销毁A Servlet对象内存之前，Tomcat服务器会自动调用AServlet对象的destroy方法
     - destroy方法执行的时候A Servlet对象还在，没有被销毁，destroy方法执行结束之后，AServlet对象的内存才会被释放

> Servlet生命周期总结

（1） 各方法的含义

- 无参数构造方法执行：标志着出生
- init方法的执行：标志着正在接收教育
- service方法的执行：标志着开始工作，并且一直服务
- destroy方法的执行：标志着临终

（2）各方法被调用的次数

- 构造方法只执行一次
- init方法只执行一次
- service方法：用户发送一次请求一次，发送N次请求N次
- destroy方法只执行一次

（3）常见错误

- 当Servlet类中编写一个有参数构造方法，如果没有手动编写无参数构造方法会报错，`500错误---->500错误是一个HTTP协议的错误状态码`，一般情况下是因为服务器端的Java程序出现了异常（服务器内部错误都是500错误）

```java
/*  //无参数构造方法
    public AServlet() {
        System.out.println("A无参数构造方法启动了");
    }*/

    //程序员手动提供有参数的构造方法
    public AServlet(String s){

    }
```

![20220424155715](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424155715.png)

- 如果没有无参数的构造方法会导致500错误
- 一般不建议程序员来定义构造方法

> 研究

```java
package servlet;

import jakarta.servlet.*;

import java.io.IOException;

/**
 * @Author: 小雷学长
 * @Date: 2022/3/17 - 10:37
 * @Version: 1.8
 */
public class AServlet implements Servlet {

    //无参数构造方法
    public AServlet(){
        System.out.println("无参数构造方法");

    }
    @Override
    public void init(ServletConfig servletConfig) throws ServletException {

    }

    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {

    }

    @Override
    public String getServletInfo() {
        return null;
    }

    @Override
    public void destroy() {

    }
}

```

```java
package servlet;

import jakarta.servlet.*;

import java.io.IOException;

/**
 * @Author: 小雷学长
 * @Date: 2022/3/17 - 11:03
 * @Version: 1.8
 */
public class BServlet implements Servlet {

    //无参数构造方法
    public BServlet() {
        System.out.println("这是B无参数构造");
    }

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {

    }

    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {

    }

    @Override
    public String getServletInfo() {
        return null;
    }

    @Override
    public void destroy() {

    }
}

```

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">

    <servlet>
        <servlet-name>aservlet</servlet-name>
        <servlet-class>servlet.AServlet</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>aservlet</servlet-name>
        <url-pattern>/a</url-pattern>
    </servlet-mapping>

    <servlet>
        <servlet-name>bservlet</servlet-name>
        <servlet-class>servlet.BServlet</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>bservlet</servlet-name>
        <url-pattern>/b</url-pattern>
    </servlet-mapping>
</web-app>
```

1. 测试发现，服务器在启动时Servlet对象不会被实例化
1. 这个设计是合理的，用户没有发送请求之前，如果提前创建出来所有的Servlet对象，必然是消耗内存的，并且一直没有用用户访问，显然这个Servlet对象是一个废物，没必要先创建

> 如何让服务器启动的时候创建Servlet对象

1. 在Servlet标签中添加`_<_load-on-startup_>_0_</_load-on-startup_>_`子标签，在该子标签中，越小的整数优先级越高

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">

    <servlet>
        <servlet-name>aservlet</servlet-name>
        <servlet-class>servlet.AServlet</servlet-class>
        <load-on-startup>0</load-on-startup>
    </servlet>
    <servlet-mapping>
        <servlet-name>aservlet</servlet-name>
        <url-pattern>/a</url-pattern>
    </servlet-mapping>

    <servlet>
        <servlet-name>bservlet</servlet-name>
        <servlet-class>servlet.BServlet</servlet-class>
        <load-on-startup>1</load-on-startup>
    </servlet>
    <servlet-mapping>
        <servlet-name>bservlet</servlet-name>
        <url-pattern>/b</url-pattern>
    </servlet-mapping>
</web-app>
```

![20220424155753](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424155753.png)

2. 一般不配，特殊情况下才配

> 各方法的使用

**service方法**

- [x] service方法是使用最多的方法，service方法一定要实现的，因为service方法是处理用户请求的核心方法

**init方法**

- [x] init方法很少用，通常在init方法中当初始化操作，并且这个初始化操作只需要执行一次，例如：数据库连接池，初始化线程池……

**destroy方法**

- [x] destroy方法也很少用，通常在destroy方法中，进行资源的关闭，马上对象要被销毁时，抓紧时间关闭资源，还有什么资源没保存的，抓紧时间保存一下

**无参数构造方法能不能代替init方法？**

- [x] 不能。
- [x] Servlet规范中要求，作为Javaweb程序员，编写Servlet类的时候，不建议手动编写构造方法，很容易让无参数构造方法消失，这个操作可能会导致Servlet对象无法实例化。
- [x] 所以init方法的存在是必要的

---

​	

## 10 适配器模式改造Servlet

### 10.1 引入适配器

- [x] 我们编写service方法，其他大部分情况下是不需要使用的，代码很丑陋

### 10.2 适配器设计模式Adapter

> 实例

- [x] 手机连直接插到220V的电压下，手机就报废了，找一个充电器，这个充电器就是一个适配器。手机连接适配

  器，适配器连接220V的电压，这样问题就解决了

> 方法

- [x] 编写一个GenericServlet类，这个类是一个抽象类，其中有一个抽象方法service
- [x] GenericServlet实现Servlet接口
- [x] GenericServlet是一个是适配器
- [x] 以后编写所有的Servlet类继承GenericServlet，重写service方法即可

```java
package servlet;

import jakarta.servlet.*;

import java.io.IOException;

/**
 * @Author: 小雷学长
 * @Date: 2022/3/17 - 19:47
 * @Version: 1.8
 */

/**
 * 编写一个标准通用的Servlet，起名：GenericServlet
 * 以后所有的Servlet类都不要直接实现Servlet接口了
 * 以后所有的Servlet类都要继承GenericServlet类
 * GenericServlet 就是一个适配器
 */
public abstract class GenericServlet implements Servlet {
    @Override
    public void init(ServletConfig servletConfig) throws ServletException {

    }

    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

    /**
     * 抽象方法，这个最常用,所以要求子类必须实现service方法
     * @param servletRequest
     * @param servletResponse
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public abstract void service(ServletRequest servletRequest, ServletResponse servletResponse)
            throws ServletException, IOException;


    @Override
    public String getServletInfo() {
        return null;
    }

    @Override
    public void destroy() {

    }
}

```

```java
package servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;

import java.io.IOException;

/**
 * @Author: 小雷学长
 * @Date: 2022/3/17 - 21:03
 * @Version: 1.8
 */
public class VipServlet extends GenericServlet {
    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse)
            throws ServletException, IOException {
        System.out.println("VIP");
    }
}
```

```java
package servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;

import java.io.IOException;

public class LoginServlet extends GenericServlet{

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        System.out.println("Login");
    }
}
```

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
    
    <servlet>
        <servlet-name>loginServlet</servlet-name>
        <servlet-class>servlet.LoginServlet</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>loginServlet</servlet-name>
        <url-pattern>/login</url-pattern>
    </servlet-mapping>

    <servlet>
        <servlet-name>VIPServlet</servlet-name>
        <servlet-class>servlet.VipServlet</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>VIPServlet</servlet-name>
        <url-pattern>/VIP</url-pattern>
    </servlet-mapping>

    <servlet>
        <servlet-name>aservlet</servlet-name>
        <servlet-class>servlet.AServlet</servlet-class>
        <!--<load-on-startup>0</load-on-startup>-->
    </servlet>
    <servlet-mapping>
        <servlet-name>aservlet</servlet-name>
        <url-pattern>/a</url-pattern>
    </servlet-mapping>

    <servlet>
        <servlet-name>bservlet</servlet-name>
        <servlet-class>servlet.BServlet</servlet-class>
        <!--<load-on-startup>1</load-on-startup>-->
    </servlet>
    <servlet-mapping>
        <servlet-name>bservlet</servlet-name>
        <url-pattern>/b</url-pattern>
    </servlet-mapping>
</web-app>
```

### 10.3 改造GenericServlet

> 关于init方法

- [x] 在提供了GenericServlet之后，init方法仍然会被调用
- [x] init方法是Tomcat服务器调用的
- [x] Tomcat服务器先创建了ServletConfig对象，如何调用init方法，将ServletConfig对象传给了init方法

---

​	

## 11 ServletConfig接口

### 11.1 ServletConfig介绍

- [x] 一个Servlet对象中有一个ServletConfig对象，一对一。100个Servlet对象有100个ServletConfig对象
- [x] Tomcat服务器创建了ServletConfig对象
- [x] 在创建Servlet对象的时候，同时创建了ServletConfig对象

### 11.2 ServletConfig讲解

- [x] ServletConfig翻译过来为`Servlet对象的配置信息对象`
  - 一个ServletConfig对象就有一个配置信息对象
  - 100个ServletConfig对象就有100个配置信息对象
- [x] ServletConfig对象到底包装了什么信息？

```xml
<servlet>
        <servlet-name>configtest</servlet-name>
<servlet-class>servlet.ConfigTestServlet</servlet-class>
 </servlet>
```

- [x] 获取<servlet-name>

```java
String servletName = config.getServletName()
```

- [x] 配置Servlet对象初始化信息
  - Servlet标签中的`<init-param>`是初始化参数，这个初始化参数会自动被小猫咪封装到ServletConfig对象中

```xml
<init-param>
  <param-name>driver</param-name>
  <param-value>com.mysql.cj.jdbc.Driver</param-value>
</init-param>

<init-param>
  <param-name>user</param-name>
  <param-value>root</param-value>
</init-param>
```

   - 通过ServletConfig对象的两个方法，可以获取到web.xml文件中的初始化参数配置信息

```java
//获取所有的初始化参数的name
java.util.EnumerationMjava.lang.String>getInitParameterNames()  
    

java.lang.String.getInitParameter(java.lang.String.name)
```

### 11.3 ServletConfig作用

```java
@Override
public void init(ServletConfig config) throws ServletException {
	//获得servlet名称
	String servletName = config.getServletName();
	System.out.println(servletName);
	//获得servelt初始化参数
	String initParameter = config.getInitParameter("url");
	System.out.println(initParameter);
	//获得ServletContext
	ServletContext servletContext = config.getServletContext();
		
		
	}
```

---

​	

## 12 ServletContext接口

### 12.1 ServletContext的获取

- [x] 通过ServletConfig获取ServletContext接口

```java
//第一种方式
ServletContext application = config.getServletContext();

//第二种方式:通过this也可以获得ServletContext对象
ServletContext application2 = this.getServletContext();
```

### 12.2 ServletContext的作用

- [x] 获取 web.xml 中配置的上下文参数 `context-param–context.getInitParameter()`
- [x] 获取当前的工程路径`context.getContextPath()`
- [x] 获取工程部署后在服务器硬盘上的绝对路径`context.getContextPath()`
- [x] 像 Map 一样存取数据`setAttribute()、getAttribute()、removeAttribute()`

### 12.3 重点

- [x] 以后在编写Servlet类的时候，实际上不是会去直接继承ServletContext类的，直接继承的是HttpServlet。（因为HttpServlet是HTTP协议专用的
- [x] 继承结构

```java
jakarta.servlet.Servlet（接口）【爷爷】
jakarta.servlet.GenericServlet implements Servlet（抽象类）【儿子】
jakarta.servlet.http.HttpServlet extends GenericServlet（抽象类）【孙子】
```

---

​	

## 13 Http协议

### 13.1 Http 和Https 理解

> Http

- [x] HTTP（超文本协议）：是一个简单的请求-响应协议，它通常运行在TCP之上
  - 文本：html、字符串……
  - 超文本：图片、音乐、视频、定位、地图……
  - 80

> Https理解

- [x] https：安全的
  - 443

### 13.2 两个时代

> http1.0

- [x] HTTP/1.0
  - 客户端可以与web服务器连接后，只能获得一个web资源，断开连接

> http1.1

- [x] HTTP/1.1
  - 客户端可以与web服务器连接后，可以获得多个web资源

### 13.3 HTTP请求

- [x] 客户端……发送请求（Request）……服务器
- [x] 以百度为例

```
Request URL:https://www.baidu.com/   请求地址
Request Method:GET     get方法/post方法
Status Code:200 OK     状态码：200
Remote(远程)  Address:14.215.177.39:443   地址+端口
Referrer Policy:no-referrer-when-downgrade
```

> 请求行

- [x] 请求行中的请求方式：`GET`
- [x] 请求方式：`Get`、`Post`

（1）Get请求方法

- [x] get：请求能够携带的参数比较少，大小有限制，会在浏览器的URL地址栏显示数据内容，不安全，但高效

[3.2 Get和Post方法](https://www.yuque.com/xleixz/aygur6/ssup8o?view=doc_embed)

（2）Post请求方法

- [x] post：请求能够携带的参数没有限制，大小没有限制，不会在浏览器的URL地址栏显示数据内容，安全，但不高效

[3.2 Get和Post方法](https://www.yuque.com/xleixz/aygur6/ssup8o?view=doc_embed)

> 请求头

```
Accept:告诉浏览器，它所支持的数据类型
Accept-Encoding：支持哪种格式  GBK  UTF-8  GB2312 ISO8859-1
Accept-Language：告诉浏览器，它的语言环境
Cache-Control：缓存控制
Connection:告诉浏览器。请求完成是断开还是保持连接
HOST：主机
…………
```

### 13.4 HTTP响应

```
Cache-Control:private    缓存控制
Connection:Keep-Alive    连接
Content-Encoding:gizp    编码
Content-Type:text/html   类型
```

> 响应体

```
Accept:告诉浏览器，它所支持的数据类型
Accept-Encoding：支持哪种格式  GBK  UTF-8  GB2312 ISO8859-1
Accept-Language：告诉浏览器，它的语言环境
Cache-Control：缓存控制
Connection:告诉浏览器。请求完成是断开还是保持连接
HOST：主机
…………
```

> 响应状态码

- [x] 200：请求成功
- [x] 3xx：请求重定向
  - 重定向：你重新到我给你的新位置去
- [x] 4xx：资源不存在
  - 404：找不到资源
- [x] 5xx：服务器代码错误（Java程序有问题）
  - 502：网关错误

---

​	

## 14 Get和Post方法

### 14.1 Get请求和Post请求的区别

> Get方法

- [x] get请求发送数据的时候，数据会挂在URI后面，并且在URI后面添加了一个`?`，`?`后面是数据。这样会导致发送的数据回显在浏览器的地址栏上，get在请求行中发送
- [x] get请求只能发送普通的字符串，并且发送的字符串长度有限，不同的浏览器，限制不同。这个没有明确的规范
- [x] get请求无法发送大数据量
- [x] get请求在W3C中是这样说的：get请求比较适合从服务器端获取数据（适合获取数据）
- [x] get请求是安全的，是绝对安全的（因为get请求只是为了从服务器获取数据）
- [x] get请求支持缓存
  - 一个get请求路径，对应一个资源
  - 一个get请求路径，对应一个资源
  - 一个get请求路径，对应一个资源
  - 一个get请求路径，对应一个资源
  - ………………

> Post方法

- [x] post请求发送数据时，在请求体当中发送，不会回显到浏览器的地址栏上，post在请求体中发送
- [x] post请求可以发送任何类型的数据，包括字符串，流媒体等信息：视频、声音、图片
- [x] post请求可以发送大数据量，理论上没有长度限制
- [x] post请求在W3C中是这样说的：post请求适合向服务器端传送数据（适合传送数据）
- [x] post请求是危险的，因为post请求时向服务器提交数据，另外post为了提交数据，所以一般情况下拦截请求的时候，大部分会选择拦截（监听）post请求
- [x] post请求不支持缓存
  - post请求之后，定位器响应的结果不会被浏览器换存起来，因为这个缓存没有意义
- [x] form表单提交，都是post方式，因为要填写大量的数据，这些数据都是收集用户的信息，一般都是传给服务器
- [x] 有敏感信息，建议使用post方式，因为get会回显敏感信息到地址栏
- [x] 做文件上传，一定是post请求

---

- [x] 不管是get请求还是post请求，发送请求的格式是完全相同的，只不过是位置不同，格式都是统一的

---

​	

## 15 模板方法设计模式

### 15.1 什么是设计模式？

- [x] 某个问题的固定解决方案（可以重复使用）

### 15.2 设计模式

- [x] GoF设计模式
  - 通常说的23中设计模式
  - 单例模式
  - 工厂模式
  - 代理模式
  - 门面模式
  - ……
- [x] JavaEE设计模式
  - DAO
  - DTO
  - VO
  - ……

### 15.3 什么是模板方法设计模式

```java
package template01;

/**
 * @Author: 小雷学长
 * @Date: 2022/3/19 - 23:16
 * @Version: 1.8
 */
public abstract class Person {
    
    
    //final使得代码得以保护
    public final void day() {
        xishu();
        chizaocan();
        doSome();
        shujiao();
        qichaugn();
    }

    public void xishu() {

    }

    public void chizaocan() {

    }

    public abstract void doSome();

    public void qichaugn() {

    }

    public void shujiao() {

    }
}

```

```java
package template01;

/**
 * @Author: 小雷学长
 * @Date: 2022/3/19 - 23:07
 * @Version: 1.8
 */
public class Student extends Person{

    @Override
    public void doSome() {

    }
}

```

```java
package template01;

/**
 * @Author: 小雷学长
 * @Date: 2022/3/19 - 23:13
 * @Version: 1.8
 */
public class Teacher extends Person {

    @Override
    public void doSome() {

    }
}

```

- [x] 如上，Person类就是一个模板类
- [x] 通常模板类是一个抽象类

---

​	

## 16 HttpServlet源码分析

### 16.1 HttpServlet类

- [x] HttpServlet类是专门为HTTP协议准备的，比GenericServlet更适合HTTP协议的开发
- [x] HttpServlet在哪个包下？
  - jakarta.serlvet.http.HttpServlet

### 16.2 Servlet规范中的接口

- [x] jakarta.servlet.Servlet              核心接口（接口）
- [x] jakarta.serlvet.SerlvetContext  Servlet上下文接口（接口）
- [x] jakarta.servlet.ServletConfig    Servlet配置信息接口（接口）
- [x] jakarta.servlet.ServletRequest  Servlet请求接口（接口）
- [x] jakarta.servlet.ServletResponse   Servlet响应接口（接口）
- [x] jakarta.servlet.ServletException   Servlet异常（类）
- [x] jakarta.servlet.GenericServlet      标准通用的Servlet类（抽象类）

### 16.3 http包下都有哪些类和接口 

>jakarta.servlet.http.*;

- [x] jakarta.servlet.http.HttpServlet（Http协议专用的Servlet类，抽象类）
- [x] jakarta.servlet.http.HttpServletRequest（Http协议专用的请求对象）
- [x] jakarta.servlet.http.HttpServletResponse（Http协议专用的响应对象）

### 16.4 HttpServletRequest对象封装了什么信息？

- [x] HttpServletRequest，简称request对象
- [x] HttpServletRequest中封装了请求协议的全部内容

### 16.5 HttpServlet源码分析

- [x] 编写的HelloServlet直接继承HttpServlet，直接重写HttpServlet类中的Service()方法行吗？
  - 可以，只不过是享受不到405错误，享受不到HTTP协议专属的东西

---

​	

## 17 Servlet类开发步骤

- [x] 编写一个Servlet类，直接继承HttpServlet
- [x] 重写doGet方法或者重写doPost方法，到底写谁？Javaweb程序员说了算

```java
package servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

/**
* @Author: 小雷学长
* @Date: 2022/3/20 - 15:24
* @Version: 1.8
*/
public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {
        
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.print("<h1>登录成功……</h1>");
    }
}

```

- [x] 将Servlet类配置到web.xml文件当中

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">

    <servlet>
        <servlet-name>login</servlet-name>
        <servlet-class>servlet.LoginServlet</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>login</servlet-name>
        <url-pattern>/login</url-pattern>
    </servlet-mapping>
</web-app>
```

- [x] 准备前端的页面（form表单），form表单中指定请求路径路径即可，

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Login page</title>
  </head>
  <body>
    <form action="/Servlet00/login" method="post">
      <input type="submit" value="login">
    </form>
    
  </body>
</html>
```

![image-20220523000515128](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220523000515128.png)

![20220424161324](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424161324.png)

---

​	

## 18 web站点的欢迎页面

### 18.1 什么是一个欢迎页面？

- [x] 对于一个webapp来说，我们是可以设置他的欢迎页面的
- [x] 设置了欢迎页面之后，当你访问这个webapp的时候，或者访问这个web站点的时候，没有指定任何资源路径，这时候会默认访问你的欢迎页面
- [x] 默认的是`http://localhost:8080/Servlet00/login.html`
- [x] 如果我们访问的方式是[http://localhost:8080/Servlet00](http://localhost:8080/Servlet00)，没有具体的资源路径，它默认会访问设置的欢迎页面

### 18.2 设置一个欢迎页面

> 设置一个欢迎页面方法

- [x] 在IDEA工具的web目录下新建一个文件login.html
- [x] 在web.xml文件中进行以下的配置

```xml
<welcome-file-list>
        <welcome-file>login.html</welcome-file>
    </welcome-file-list>
```

   - 注意
     - 设置欢迎页面的时候，这个路径不需要以`/`开始，并且这个路径默认是从webapp的根下开始查找
- [x] 启动浏览器，浏览器地址栏输入地址`http://localhost:8080/Servlet05`
- [x] 一个webapp可以设置多个欢迎页

```xml
<welcome-file-list>
	<welcome-file>login.html</welcome-file>
	<welcome-file>page1/page2/page.html</welcome-file>
    </welcome-file-list>
```

   - 注意
     - 越靠上，优先级越高，上面找不到继续往下找

> 如果在webapp的根目录下还有其他目录

![20220424161529](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424161529.png)

（1）方法

- [x] 在webapp根下新建page1
- [x] 在page1下新建一个page2目录
- [x] 在page2目录下新建page.html页面
- [x] 在web.xml文件中应该这样配置

```xml
  <welcome-file-list>
        <welcome-file>page1/page2/page.html</welcome-file>
    </welcome-file-list>
```

   - 注意
     - 路径不需要以`/`开始，并且路径默认从webapp的根下开始找

### 18.3 特殊的index.html

- [x] 当文件名为index.html的时候，不需要在web.xml文件中配置欢迎页面
  - 原因
    - Tomcat服务器已经提前配好了
  - 实际上配置文件欢迎页面有两个地方可以配置
    - 一个是在webapp内部的web.xml文件中，这个地方配置的属于局部配置
    - 一个是在CATALINA_HOME/conf/web.xml文件中进行配置，这个地方属于全局配置

```xml
<welcome-file-list>
        <welcome-file>index.html</welcome-file>
        <welcome-file>index.htm</welcome-file>
        <welcome-file>index.jsp</welcome-file>
    </welcome-file-list>
```

   - 注意配置原则
     - 局部优先原则（就近原则）

<img src="https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424161541.png" alt="20220424161541" style="zoom:50%;" />

### 18.4 欢迎页可以是一个Servlet

- [x] 欢迎页就是一个资源，既然是一个资源，可以是静态，也可以是静态

> 方法

- [x] 创建一个Servlet类

```java
package servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

/**
* @Author: 小雷学长
* @Date: 2022/3/20 - 16:23
* @Version: 1.8
*/
public class WelcomeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.print("<h1>Welcome to Serclet</h1>");
    }
    
}

```

- [x] 在web.xml文件中配置Servlet和欢迎页

```java
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
    
   <!--配置欢迎页-->
    <welcome-file-list>
        <welcome-file>welcome</welcome-file>
    </welcome-file-list>
    
    <!--配置Servlet-->
    <servlet>
        <servlet-name>WelcomeServlet</servlet-name>
        <servlet-class>servlet.WelcomeServlet</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>WelcomeServlet</servlet-name>
        <url-pattern>/welcome</url-pattern>
    </servlet-mapping>

</web-app>
```

![20220424161558](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424161558.png)

---

​	

## 19 关于WEB-INF目录

WEB-INF目录下的资源是受保护的，在浏览器上不能够通过路径之间访问。所以像HTML、css、image等静态资源一定要放在WEB-INF目录之外

---

​	

## 20 HttpServletRequest接口

### 20.1 Request接口的详解

- [x] HttpServletRequest是一个接口，全限定名称：jakarta.servlet.http.HttpServletRequest
- [x] HttpServletRequest接口是Servlet规范中的一员
- [x] HttpServletRequest接口的父接口是：ServletRequest
  - `public interface HttpServletRequest extends ServletRequest{}`
- [x] HttpServletRequest接口的实现类是
- [x] Javaweb程序员面向HttpServletRequest接口编程，调用方法就可以获取到请求的信息了
- [x] 获取用户提交的数据的4个方法

```java
Map<String.String[]> getParameterMap() 这个是获取Map
Enumer ation<String> getParameterNames()  这个是获取Map集合中的多有key
String [] getParameter Values(java.lang.String name)  根据key获取Map集合的values
String getParameter(String name)  获取value这个一维数组当中的第一个元素，这个方法最常用
    //以上的4个方法和获取用户提交的数据有关系
```

- [x] `String getParameter(String name)`这个方法用的最多

### 20.2 request接口获取请求参数

- [x] 前端表单提交数据的时候，假设提交的是120这个数字，其实是以字符串"120"的方式提交到后端的，后端获取到的是字符串

> 测试request接口中的相关方法

```java
/**
* @description: 总结四个非常常用的方法
*
* Map<String,String[]> parameterMap = request.getParameterMap();
* Enumeration<String> names = request.getParameterNames();
* String [] values = request.getParameterValues("name");
* String value = request.getPatameter("name");
 */
```

> 各方法的详解

- [x] `Map<String,String[]> parameterMap = request.getParameterMap();`
- [x] 这个是获取Map

```java
/**
* @method Map<String,String[]> parameterMap = request.getParameterMap();
*/
//获取参数Map集合
Map<String, String[]> parameterMap = request.getParameterMap();

//遍历Map集合
//最基本的方式：获取Map集合中所有的Key，遍历
Set<String> keys = parameterMap.keySet();

//遍历Set集合
//迭代器
Iterator<String> it = keys.iterator();
//遍历
while (it.hasNext()) {
	String key = it.next();
	//System.out.println(key);
	
	//通过key获取value
	String[] values = parameterMap.get(key);
	//System.out.println(key + "=" + values);
	
	//遍历一维数组
	System.out.print(key + "=");
	
	for (String value : values) {
		System.out.print(value + ",");
	}
	//换行
	System.out.println();
        }
```

---

- [x] `Enumeration<String> names = request.getParameterNames();`
- [x] 这个是获取Map集合中的多有key

```java
/**
* @method Enumeration<String> names = request.getParameterNames();
* 直接通过getParameterNames()这个方法，
* 可以直接获取这个Map集合所有的key
*/
Enumeration<String> names = request.getHeaderNames();
while (names.hasMoreElements()) {
	String name = names.nextElement();
	//System.out.println("name");
	
        }
```

---

- [x] `String [] values = request.getParameterValues("name");`
- [x] 根据key获取Map集合的values

```java
/**
* @method String [] values = request.getParameterValues("name");
* 直接通过name获取values这个一位数组
*/
String[] usernames = request.getParameterValues("username");
String[] userpwds = request.getParameterValues("userpwd");
String[] interests = request.getParameterValues("interest");

//遍历一维数组
for (String username : usernames) {
	System.out.println(username);
}

for (String userpwd : userpwds) {
	System.out.println(userpwd);
}

for (String interest : interests) {
	System.out.println(interest);
        }
```

---

- [x] `String value = request.getPatameter("name");`
- [x] 获取value这个卫衣数组当中的第一个元素，这个方法最常用

```java
/**
* @method String value = request.getPatameter("name");
* 通过name获取value这个一维数组的第一个元素
* 这个方法使用的最多，因为这个一维数组一般只有一个元素
*/
String username = request.getParameter("username");
String userpwd = request.getParameter("userpwd");
//注意：这个方法只获取一维数组的第一个元素
//String interest = request.getParameter("interest");

//既然是CheckBox，调用方法：request.getParameterValues(:interest");
String[] interests02 = request.getParameterValues("interest");

System.out.println(username);
System.out.println(userpwd);
//System.out.println(interest);

for (String interest02 : interests02) {
	System.out.println(interest02);
}
    }
```

---

```java
package request;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
* @Author: 小雷学长
* @Date: 2022/3/20 - 23:52
* @Version: 1.8
*/

/*
usename=zhangsan&userpwd=123&interest=s&interest=d
Map<String,String[]>
key            value
---------------------------
"username"      {"zhagnsan"}
"userpwd"       {"123"}
"interest"      {"s","d"}
*/

/**
* @description: 总结四个非常常用的方法
*
* Map<String,String[]> parameterMap = request.getParameterMap();
* Enumeration<String> names = request.getParameterNames();
* String [] values = request.getParameterValues("name");
* String value = request.getPatameter("name");
*/
public class requestTest extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		
		
		//面向接口编程：HttpServletRequest接口
		//获取前端提交的数据
		//前端会提交
		// usename=zhangsan&userpwd=123&interest=s&interest=d
		//以上的数据会被小猫咪封装到request对象中
		
		/**
		* @method Map<String,String[]> parameterMap = request.getParameterMap();
		*/
		//获取参数Map集合
		Map<String, String[]> parameterMap = request.getParameterMap();
		
		//遍历Map集合
		//最基本的方式：获取Map集合中所有的Key，遍历
		Set<String> keys = parameterMap.keySet();
		
		//遍历Set集合
		//迭代器
		Iterator<String> it = keys.iterator();
		//遍历
		while (it.hasNext()) {
			String key = it.next();
			//System.out.println(key);
			
			//通过key获取value
			String[] values = parameterMap.get(key);
			//System.out.println(key + "=" + values);
			
			//遍历一维数组
			System.out.print(key + "=");
			
			for (String value : values) {
				System.out.print(value + ",");
			}
			//换行
			System.out.println();
		}
		
		System.out.println("----------------------");
		
		/**
		* @method Enumeration<String> names = request.getParameterNames();
		* 直接通过getParameterNames()这个方法，
		* 可以直接获取这个Map集合所有的key
		*/
		Enumeration<String> names = request.getHeaderNames();
		while (names.hasMoreElements()) {
			String name = names.nextElement();
			//System.out.println("name");
			
		}
		
		System.out.println("----------------------");
		
		/**
		* @method String [] values = request.getParameterValues("name");
		* 直接通过name获取values这个一位数组
		*/
		String[] usernames = request.getParameterValues("username");
		String[] userpwds = request.getParameterValues("userpwd");
		String[] interests = request.getParameterValues("interest");
		
		//遍历一维数组
		for (String username : usernames) {
			System.out.println(username);
		}
		
		for (String userpwd : userpwds) {
			System.out.println(userpwd);
		}
		
		for (String interest : interests) {
			System.out.println(interest);
		}
		
		System.out.println("----------------------");
		
		/**
		* @method String value = request.getPatameter("name");
		* 通过name获取value这个一维数组的第一个元素
		* 这个方法使用的最多，因为这个一维数组一般只有一个元素
		*/
		String username = request.getParameter("username");
		String userpwd = request.getParameter("userpwd");
		//注意：这个方法只获取一维数组的第一个元素
		//String interest = request.getParameter("interest");
		
		//既然是CheckBox，调用方法：request.getParameterValues(:interest");
		String[] interests02 = request.getParameterValues("interest");
		
		System.out.println(username);
		System.out.println(userpwd);
		//System.out.println(interest);
		
		for (String interest02 : interests02) {
			System.out.println(interest02);
		}
	}
}

```

### 20.3 请求域对象

#### 20.3.1 应用域对象

- [x] 应用域对象是什么？
  - ServletContext（Servlet上下文对象）
  - 什么时候会考虑向ServletCOntext这个应用域当中绑定数据？
    - 所有用户共享的数据
    - 这个共享数据量很小
    - 这个共享数据量很少的修改操作
    - 以上三个条件都满足的情况下，使用这个应用域对象，可以大大提高我们程序执行效率
    - 实际上向应用域中绑定数据，就相当于把数据放到了缓存中，然后用户访问的时候，直接从缓存中区域，减少IO的操作，大提高系统的性能，所以缓存技术系统是提高系统性能的重要手段
  - 缓存技术
    - 字符串常量池
    - 整数常量池[-128-127]
    - 数据库连接池（提前创建好N个连接对象，将连接对象放到集合中，使用连接对象的时候，直接从缓存中拿，省去了连接对象的创建过程。效率提升
    - 线程池（Tomcat服务器就是支持多线程的，所谓的线程池就是提前创建好N个线程对象，将线程对象存储到集合中，然后用户请求过来之后，直接从线程池中获取线程对象，直接拿来用，提升系统性能
    - …………
  - ServletContext中的三个操作域方法

```java
void setAttribute(String name,Object obj);//向域中绑定数据
Object getAttribute(String name);//从域中根据name获取数据
void removeAttribute(String name);//将域中绑定的数据移除

//以上操作类类似于Map方法
Map<String,Object>map;
map.out("name",obj);//向map集合中放key和Value
Object obj  = map.get("name");//通过Map集合的key删除key和value这个键值对
```

#### 20.3.2 请求域对象

- [x] request对象实际上又称为“请求域对象”
- [x] 请求域对象要比应用域对象范围小很多，生命周期短很多，请求域只在一次请求内有效
- [x] 一个请求对象request对应一个请求域对象。一次请求域结束以后，这个请求域就销毁了
- [x] 请求域对象也有跟应用域一样的三个方法

```java
void setAttribute(String name,Object obj);//向域中绑定数据
Object getAttribute(String name);//从域中根据name获取数据
void removeAttribute(String name);//将域中绑定的数据移除
```

#### 20.3.3 请求域和应用域的选用原则

- [x] 尽量使用小的域对象，因为小的域对象占用资源较少

#### 20.3.4 实现

> 实现请求

```java
package request;

/**
 * @Author: 小雷学长
 * @Date: 2022/3/21 - 11:40
 * @Version: 1.8
 */

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;


public class AServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //获取系统时间
        Date nowTime = new Date();

        //向request域中绑定数据
        request.setAttribute("nowTime", nowTime);

        //从request域中取出绑定的数据
        Object obj = request.getAttribute("nowTime");

        //输出到浏览器
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.print("request域当中获取的系统当前时间 = " + obj);
    }


}



```

![20220424162042](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424162042.png)

#### 20.3.5 出现问题

- [x] 若在AServlet绑定数据，用BServlet获取，是无法获取的，因为是两次请求了

```java
package request;

/**
 * @Author: 小雷学长
 * @Date: 2022/3/21 - 11:40
 * @Version: 1.8
 */

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;


public class AServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //获取系统时间
        Date nowTime = new Date();

        //向request域中绑定数据
        request.setAttribute("nowTime", nowTime);
    }
}
```

```java
package request;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * @Author: 小雷学长
 * @Date: 2022/3/21 - 11:40
 * @Version: 1.8
 */
public class BServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


        //从request域中取出绑定的数据
        Object obj = request.getAttribute("nowTime");

        //输出到浏览器
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.print("request域当中获取的系统当前时间 = " + obj);
    }
}

```

![20220424162100](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424162100.png)

> 解决 使用Servlet中请求转发机制

- [x] 问题：请求域只能请求一次，无法实现AServlet转跳到BServlet
- [x] 解决：让AServlet和BServlet放到一次请求中，使用Servlet当中的请求转发机制

**（1）跳转，转发机制**

- [x] 转发

1. 获取请求转发对象`RequestDispatcher dispatcher = request.getRequestDispatcher("/bservlet");`
1. 调用转发器的forward方法完成跳转/转发`dispatcher.forward(request,response);`

- [x] 第1步和第2步代码可以合二为一：`request.getRequestDispatcher("/bservlet".forward(request,response);`

**（2）两个Servlet共享数据**

- [x] 将数据放到ServletContext应用中，当然是可以的，但是应用域范围太大，占用资源太多，不建议使用
- [x] 可以将数据存放到request域当中，然后AServlet转发到BServlet，保证AServlet在同一次请求当中，这样就可以做到两个Servlet，或者多个Servlet共享一份数据
- [x] 转发的资源不一定是Servlet，只要是Tomcat服务器合法的资源都可以
- [x] 转发路径以`"/"`开始，不加项目名

**（3）request对象中两个容易混淆的方法**

```java
第一个方法：String value = request.getPatameter("name");
第二个方法：Object obj = request.getAttribute("name");
```

- [x] 以上两个方法的区别是
  - 第一个方法：获取的是用户在浏览器上提交的数据
  - 第二个方法：获取的是请求栏中绑定的数据

#### 20.3.6 实现一个HTML转发机制

```java
package request;

/**
* @Author: 小雷学长
* @Date: 2022/3/21 - 11:40
* @Version: 1.8
*/

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;


public class AServlet extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		
		//获取系统时间
		Date nowTime = new Date();
		
		//向request域中绑定数据
		request.setAttribute("nowTime", nowTime);
		
		//第一步：获取请求转发器对象
		RequestDispatcher dispatcher = request.getRequestDispatcher("/test.html");
		
		//第二步：调用请求转发器RequestDispatcher的forward方法进行转发
         dispatcher.forward(request,response);
```

```java
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<h1>Test HTML</h1>

</body>
</html>
```

![20220424162109](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424162109.png)

### 20.4 request接口实现IP地址

```java
package servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpCookie;

/**
 * @Author: 小雷学长
 * @Date: 2022/3/20 - 17:22
 * @Version: 1.8
 */
public class RequestTestServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.print("");

        //获取客户端的IP地址
        String remoteAddr = request.getRemoteAddr();
        System.out.println("客户端的IP地址:" + remoteAddr);

    }


}
```

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
  
  <servlet>
    <servlet-name>RequestTestServlet</servlet-name>
    <servlet-class>servlet.RequestTestServlet</servlet-class>
  </servlet>
  <servlet-mapping>
    <servlet-name>RequestTestServlet</servlet-name>
    <url-pattern>/testrequest</url-pattern>
  </servlet-mapping>
  
</web-app>
```

![image-20220523001745534](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220523001745534.png)
![20220424162159](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424162159.png)

### 20.5 获取用户的用户名

> 方法

- ✅`String username = request.getParameter("username");`

> 实现

```java
public class RequestTestServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //设置请求体的字符集
        //以免Tomcat9级以下版本出现乱码
        request.setCharacterEncoding("UTF-8");


        //这是Post请求
        String username = request.getParameter("username");
        //输出这个用户提交的用户名
        System.out.println(username);

    }

}
```

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
  
  <servlet>
    <servlet-name>RequestTestServlet</servlet-name>
    <servlet-class>servlet.RequestTestServlet</servlet-class>
  </servlet>
  <servlet-mapping>
    <servlet-name>RequestTestServlet</servlet-name>
    <url-pattern>/testrequest</url-pattern>
  </servlet-mapping>
  
</web-app>
```

​	

![20220424162231](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424162231.png)

> 解决请求体的乱码问题

- ✅Tomcat9版本以下，若用户输入中文表单，通过前端输入到后端的数据则会显示乱码

> 解决

```java
 //设置请求体的字符集
        //以免Tomcat9级以下版本出现乱码
        request.setCharacterEncoding("UTF-8");
```

- ✅Tomcat10版本及以后不需要考虑乱码问题题

### 20.6 获取用户的根路径

> 方法

- ✅`//这个方法用得比较多，动态获取应用的路径`

​        `String contextPath = request.getContextPath();`

​        `System.out.println("应用的根路径" + contextPath);`

> 实现

```java
public class RequestTestServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //这个方法用得比较多，动态获取应用的路径
        String contextPath = request.getContextPath();
        System.out.println("应用的根路径" + contextPath);

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.print(contextPath);

    }
}
```

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
  
  <servlet>
    <servlet-name>RequestTestServlet</servlet-name>
    <servlet-class>servlet.RequestTestServlet</servlet-class>
  </servlet>
  <servlet-mapping>
    <servlet-name>RequestTestServlet</servlet-name>
    <url-pattern>/testrequest</url-pattern>
  </servlet-mapping>
  
</web-app>
```

​	

![20220424162625](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424162625.png)

### 20.7 获取请求方式

> 方法

- ✅`//获取请求方式`

​        `String method = request.getMethod();`

​        `System.out.println(method);`

> 实现

```java
package servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpCookie;
import java.net.SocketTimeoutException;

/**
 * @Author: 小雷学长
 * @Date: 2022/3/20 - 17:22
 * @Version: 1.8
 */
public class RequestTestServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //获取请求路径
        String method = request.getMethod();
        System.out.println(method);

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.print(method);

    }
}
```

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
  
  <servlet>
    <servlet-name>RequestTestServlet</servlet-name>
    <servlet-class>servlet.RequestTestServlet</servlet-class>
  </servlet>
  <servlet-mapping>
    <servlet-name>RequestTestServlet</servlet-name>
    <url-pattern>/testrequest</url-pattern>
  </servlet-mapping>
  
</web-app>
```

​	

![20220424162747](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424162747.png)

### 20.8 获取请求URI

> 方法

- ✅`//获取请求的URI`

​        `String requestURI = request.getRequestURI();  `

​        `System.out.println(requestURI);`

> 实现

```java
public class RequestTestServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //获取请求的URI
        String requestURI = request.getRequestURI();
        System.out.println(requestURI);

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.print(requestURI);

    }
}

```

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
  
  <servlet>
    <servlet-name>RequestTestServlet</servlet-name>
    <servlet-class>servlet.RequestTestServlet</servlet-class>
  </servlet>
  <servlet-mapping>
    <servlet-name>RequestTestServlet</servlet-name>
    <url-pattern>/testrequest</url-pattern>
  </servlet-mapping>
  
</web-app>
```

​	

![20220424162852](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424162852.png)

### 20.9 获取Servlet路径

> 方法

- ✅`//获取Servlet路径`

​        `String servletpath = request.getServletPath();`

​        `System.out.println(servletpath);`

> 实现

```java
public class RequestTestServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //获取Servlet路径
        String servletpath = request.getServletPath();
        System.out.println("servletpath");

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.print(servletpath);

    }
}
```

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
  
  <servlet>
    <servlet-name>RequestTestServlet</servlet-name>
    <servlet-class>servlet.RequestTestServlet</servlet-class>
  </servlet>
  <servlet-mapping>
    <servlet-name>RequestTestServlet</servlet-name>
    <url-pattern>/testrequest</url-pattern>
  </servlet-mapping>
  
</web-app>
```

​	

![20220424162952](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424162952.png)

---

​	

## 21 实战

### 21.1 实现步骤

1.1 准备一张数据库表

```sql
create table dept(
								deptno int,
								dname  varchar(255),
								loc    varchar(255)
);
insert into dept(deptno,dname,loc)values(10,'销售部','北京'),
																				(20,'研发部','上海'),
																				(30,'技术部','广州'),
																				(40,'媒体部','深圳');
-- 提交
COMMIT;
select * from dept;
```

​	

1.2 准备一套HTML页面

   - 新增页面：add.html
   - 修改页面：edit.html
   - 详情页面：detail.html
   - 欢迎页面：index.html
   - 部门列表页面：list.html（以列表页面为核心，展开操作）

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>欢迎使用oa系统</title>
</head>
<body>
<a href="list.html">查看部门列表</a>

</body>
</html>
```

```html
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>部门列表</title>
</head>
<body>
<h1 align="center">部门列表</h1>
<hr>
<table border="1px" align="center" width="50%">
	<tr>
		<th>序号</th>
		<th>部门编号</th>
		<th>部门名称</th>
		<th>操作</th>
	</tr>
	<tr>
		<td>1</td>
		<td>10</td>
		<td>销售部</td>
		<td>
			<a href="javascript:void(0)" onclick="window.confirm('亲，确认删除该数据吗？')">删除</a>
			<a href="edit.html">修改</a>
			<a href="detail.html">详情</a>
		</td>
	</tr>
	<tr>
		<td>2</td>
		<td>20</td>
		<td>研发部</td>
		<td>
			<a href="javascript:void(0)" onclick="window.confirm('亲，确认删除该数据吗？')">删除</a>
			<a href="edit.html">修改</a>
			<a href="detail.html">详情</a>
		</td>
	</tr>
	<tr>
		<td>3</td>
		<td>30</td>
		<td>运用部</td>
		<td>
			<a href="javascript:void(0)" onclick="window.confirm('亲，确认删除该数据吗？')">删除</a>
			<a href="edit.html">修改</a>
			<a href="detail.html">详情</a>
		</td>
	</tr>
	<tr>
		<td>4</td>
		<td>40</td>
		<td>媒体部</td>
		<td>
			<a href="javascript:void(0)" onclick="window.confirm('亲，确认删除该数据吗？')">删除</a>
			<a href="edit.html">修改</a>
			<a href="detail.html">详情</a>
		</td>
	</tr>
</table>

<hr>
<a href="add.html">新增部门</a>

</body>
</html>
```

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>部门详情</title>
</head>
<body>
<h1>部门详情</h1>
<hr>
部门编号<br>
部门名称<br>
部门位置<br>

<form action="list.html">
    <input type="button" value="后退" onclick="window.history.back()">

</form>

</body>
</html>
```

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>修改部门</title>
</head>
<body>
<h1>修改部门</h1>
<hr>
<form action="list.html" method="post">
    部门编号<input type="text" name="deptno" value="20" readonly><br><!--readonly只读-->
    部门名称<input type="text" name="dname" value="销售部"><br>
    部门位置<input type="text" name="loc" value="北京"><br>
    <input type="submit" value="修改"><br>

</form>

</body>
</html>
```

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>新增部门</title>
</head>
<body>
<h1>新增部门</h1>
<hr>
<form action="list.html" method="post">
    部门编号<input type="text" name="deptno"><br>
    部门名称<input type="text" name="dname"><br>
    部门位置<input type="text" name="loc"><br>
    <input type="submit" value="保存"><br>

</form>

</body>
</html>
```

​	

1.3 分析这个系统包括哪些功能

- [x] 查看部门列表
- [x] 新增部门
- [x] 删除部门
- [x] 查看部门详情信息
- [x] 跳转到修改页面
- [x] 修改部门

​	

1.4 在IDEA中搭建开发环境

- [x] 创建一个webapp（给这个webapp添加servlet-api-jar和jsp-api-jar到classpath当中
- [x] 向webapp中添加连接数据库的jar包（MySQL驱动）
  - 必须在WEB-INF目录下创建lib，然后将mysql驱动jar包导入
- [x] JDBC的工具类

<img src="https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424172620.png" alt="20220424172620" style="zoom: 50%;" />

```java
package oa;

/**
* @Author: 小雷学长
* @Date: 2022/3/22 - 0:42
* @Version: 1.8
*/

import javax.xml.transform.Result;
import java.sql.*;
import java.util.ResourceBundle;

/**
* JDBC工具类
*/
public class DBUtil {
    
    //静态变量：也是在类加载时执行，并且是有自上而下的顺序
    //资源绑定器
    private static ResourceBundle bundle = ResourceBundle.getBundle("resources.jdbc");
    
    //根据属性配置文件的key获取value
    private static String driver = bundle.getString("driver");
    private static String url = bundle.getString("url");
    private static String user = bundle.getString("user");
    private static String password = bundle.getString("password");
    
    
    static {
        //注册驱动(注册驱动只需要注册一次，放在静态代码块中，DBUtil类加载的时候执行)
        try {
            /* com.mysql.jdbc.Driver是连接数据库的驱动，不能写死，因为以后可能还会写Oracle数据库
            * OCP开闭原则：对扩展开放，对修改关闭。（OCP：在进行拓展的时候不需要修改Java源码
            * 通过属性配置文件jdbc.properties来获取驱动
            */
            //Class.forName("com.mysql.jdbc.Driver");
            
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    
    
    /**
    * 获取数据库连接对象
    *
    * @return conn
    * @throws SQLException
    */
    public static Connection getConnection() throws SQLException {
        
        //获取连接
        Connection conn = DriverManager.getConnection(url, user, password);
        return conn;
    }
    
    /**
    * 释放资源
    * @param conn 连接对象
    * @param ps 数据库操作对象
    * @param rs 结果集对象
    */
    public static void close(Connection conn, Statement ps, ResultSet rs) {
        
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (ps != null) {
            try {
                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    
    
}

```

```properties
driver=com.mysql.jdbc.Driver
url=jdbc:mysql://localhost:3306/study
user=root
password=root
```

![20220424172924](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424172924.png)

- [x] 将所有HTML页面拷贝到web目录下

​	

1.5 实现连接

(1)注意

- [x] 如何实现？
  - 可以从后端一步一步往前端写，也可以从前端一步一步往后端写，不要想起来什么写什么。要根据程序执行的过程。程序到哪里，就写哪里，这样可以避免错误
- [x] 假设从前端开始，一定是从用户点击按钮开始

(2) 步骤

1. 先修改前端页面的超链接，因为用户先点击的就是超链接

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>欢迎使用oa系统</title>
  </head>
  <body>
    <a href="/oa/dept/list">查看部门列表</a>
    
  </body>
</html>
```

---

2. 编写web.xml文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
  
  <servlet>
    <servlet-name>list</servlet-name>
    <servlet-class>oa.DeptListServlet</servlet-class>
  </servlet>
  <servlet-mapping>
    <servlet-name>list</servlet-name>
    <url-pattern>/dept/list</url-pattern>
  </servlet-mapping>
  
</web-app>
```

---

3. 编写DeptListServlet类继承HttpServlet类，然后重写doGet方法

```java
package oa.action;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
* @Author: 小雷学长
* @Date: 2022/3/22 - 10:05
* @Version: 1.8
*/
public class DeptListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        
        
    }
}

```

---

4. 在DeptServlet类的doGet方法中连接数据库，查询所有的部门，动态的展示部门列表页面

- [x] 分析list.html页面中哪部分是固定不变的，哪部分是需要动态展示的
- [x] list.html页面中的内容所有的双引号，要变成单引号，因为print.out("")这里有一个双引号，容易冲突

```java
package oa.action;

import com.sun.corba.se.spi.orb.PropertyParser;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import oa.untils.DBUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @Author: 小雷学长
 * @Date: 2022/3/22 - 10:05
 * @Version: 1.8
 */
public class DeptListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //设置响应的内容类型
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        out.print("        <!DOCTYPE html>");
        out.print("<html lang='en'>");
        out.print("<head>");
        out.print("	<meta charset='UTF-8'>");
        out.print("	<title>部门列表</title>");
        out.print("</head>");
        out.print("<body>");
        out.print("<h1 align='center'>部门列表</h1>");
        out.print("<hr>");
        out.print("<table border='1px' align='center' width='50%'>");
        out.print("	<tr>");
        out.print("		<th>序号</th>");
        out.print("		<th>部门编号</th>");
        out.print("		<th>部门名称</th>");
        out.print("		<th>操作</th>");
        out.print("	</tr>");
        out.print("	<!--以上是固定的-->");
        /*
        上面一部分是死的
         */

        /*
         * 连接数据库
         */
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {

            //获取连接
            conn = DBUtil.getConnection();

            //获取预编译的数据库操作对象
            String sql = "select deptno,dname ,loc from dept";
            ps = conn.prepareStatement(sql);

            //执行SQL语句
            rs = ps.executeQuery();

            //处理结果集
            int i = 0;
            while (rs.next()) {
                String deptno = rs.getString("deptno");
                String dname = rs.getString("dname");
                String loc = rs.getString("loc");

                out.print("	<tr>");
                out.print("		<td>" + (++i) + "</td>");
                out.print("		<td>" + deptno + "</td>");
                out.print("		<td>" + dname + "</td>");
                out.print("		<td>");
                out.print("			<a href=''></a>");
                out.print("			<a href='edit.html'>修改</a>");
                out.print("			<a href='detail.html'>详情</a>");
                out.print("		</td>");
                out.print("	</tr>");

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {

            //释放资源
            DBUtil.close(conn, ps, rs);
        }

        /*
        下面一部分是死的
         */
        out.print("	<!--一下是固定的-->");
        out.print("</table>");
        out.print("<hr>");
        out.print("<a href=' add.html'>新增部门</a>");
        out.print("</body>");
        out.print("</html>");
    }
}

```

​	

1.6 查看部门详情

- [x] 从前端往后端一步一步实现，要考虑的是用户要点击的是什么，用户点击的在哪里

```java
 out.print("<a href='" + contextPath + "/dept/detail?deptno = " + deptno + "'>详情</a>");
```

- [x] 向服务器提交数据的格式：`url?name=value&name=value&name=value&name=value`
- [x] 这里的问号必须是英文的问号，不能写中文的问号，url不能带空格

---

1. 编写一个类：DeptDetailServelt.java

```java
package oa.action;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import oa.untils.DBUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @Author: 小雷学长
 * @Date: 2022/3/22 - 15:01
 * @Version: 1.8
 */
public class DeptDetailServelt extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();


        out.print("<!DOCTYPE html>");
        out.print("<html lang='en'>");
        out.print("<head>");
        out.print("	<meta charset='UTF-8'>");
        out.print("	<title>部门详情</title>");
        out.print("</head>");
        out.print("<body>");
        out.print("<h1>部门详情</h1>");
        out.print("<hr>");


        //获取部门编号
        //虽然提交的是30，但服务器获取的是30这个字符串
        String deptno = request.getParameter("deptno");


        /*
         * 连接数据库
         */
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {

            //获取连接
            conn = DBUtil.getConnection();

            //获取预编译的数据库操作对象
            String sql = "select deptno,dname,loc from dept where deptno=?";
            ps = conn.prepareStatement(sql);

            ps.setString(1, deptno);
            //这个结果集一定只有一个元素


            //执行SQL语句
            rs = ps.executeQuery();

            //处理结果集
            if (rs.next()) {
                String dname = rs.getString("dname");
                String loc = rs.getString("loc");

                out.print("部门编号" + deptno + "<br>");
                out.print("部门名称" + dname + "<br>");
                out.print("部门位置" + loc + "<br>");


            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {

            //释放资源
            DBUtil.close(conn, ps, rs);
        }

        out.print("");
        out.print("<form action='list.html'>");
        out.print("	<input type='button' value='后退' onclick='window.history.back()'>");
        out.print("");
        out.print("</form>");
        out.print("");
        out.print("</body>");
        out.print("</html>");
    }
}
```

​	

1.7 实现删除功能

（1）方法

- [x] `<!--href后面设置为javascript:void(0)表示：仍然保留住超链接的样子，只是点击超链接之后，只执行后面的js代码，不进行页面的跳转，不一定是0-->`
- [x] JavaScript确认删除提示的方法

```javascript
<script type="text/javascript">
  function del() {
  //弹出确认框，用户点击确定返回TRUE，点击取消返回FALSE
  var ok = window.confirm("亲，删了不可恢复哦！");
  if (ok){
    //发送请求，进行删除数据的操作
    //在js代码中如何发送请求给服务器？？？？？？？
    alert("正在删除数据，请稍后……")
    
    /*
    四种获取数据的方法
    */
    // document.location.href = "请求路径"
    //document.locaction = "请求路径"
    //window.location.href = "请求路径"
    //window.location = "请求路径"
    document.location.href = "项目名/请求路径";
  }
  

}
</script>


<!--调用del方法-->
<tr>
		<td>1</td>
		<td>10</td>
		<td>销售部</td>
		<td>
			<a href="javascript:void(0)" onclick="del()">删除</a>
			<a href='edit.html'>修改</a>
			<a href='detail.html'>详情</a>
		</td>
	</tr>
```

![20220424173218](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424173218.png)

​	

![20220424173226](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424173226.png)

​	

- [x] 四种获取请求的方法

```javascript
document.location.href = "请求路径"
document.locaction = "请求路径"
window.location.href = "请求路径"
window.location = "请求路径"
```

（2）实现删除

- [x] 前端程序要写到后端的java代码中，因为没有JSP的支持，只能在后端里写前端代码

```java
package oa.action;

import com.sun.corba.se.spi.orb.PropertyParser;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import oa.untils.DBUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
* @Author: 小雷学长
* @Date: 2022/3/22 - 10:05
* @Version: 1.8
*/
public class DeptListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        
        //获取应用的根路径
        String contextPath = request.getContextPath();
        
        //设置响应的内容类型
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        
        out.print("        <!DOCTYPE html>");
        out.print("<html lang='en'>");
        out.print("<head>");
        out.print("	<meta charset='UTF-8'>");
        out.print("	<title>部门列表</title>");
        out.print("</head>");
        
        out.print("<body>");
        out.print("<script type='text/javascript'>");
        out.print("        function del(dno) {");
        out.print("    if(window.confirm('亲，删了不可恢复哦！')){");
        out.print("        document.location.href = 'oa/dept/delete?deptno=' + dno;");
        out.print("    }");
        out.print("}");
        out.print("</script>");
        out.print("<h1 align='center'>部门列表</h1>");
        out.print("<hr>");
        out.print("<table border='1px' align='center' width='50%'>");
        out.print("	<tr>");
        out.print("		<th>序号</th>");
        out.print("		<th>部门编号</th>");
        out.print("		<th>部门名称</th>");
        out.print("		<th>操作</th>");
        out.print("	</tr>");
        out.print("	<!--以上是固定的-->");
        /*
        上面一部分是死的
        */
        
        /*
        * 连接数据库
        */
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            //获取连接
            conn = DBUtil.getConnection();
            
            //获取预编译的数据库操作对象
            String sql = "select deptno,dname ,loc from dept";
            ps = conn.prepareStatement(sql);
            
            //执行SQL语句
            rs = ps.executeQuery();
            
            //处理结果集
            int i = 0;
            while (rs.next()) {
                String deptno = rs.getString("deptno");
                String dname = rs.getString("dname");
                String loc = rs.getString("loc");
                
                out.print("	<tr>");
                out.print("		<td>" + (++i) + "</td>");
                out.print("		<td>" + deptno + "</td>");
                out.print("		<td>" + dname + "</td>");
                out.print("		<td>");
                out.print("<a href='javascript:void(0)' onclick='del(" + deptno + ")')'>删除</a>");
                out.print("			<a href='edit.html'>修改</a>");
                out.print("			<a href='" + contextPath + "/dept/detail?deptno=" + deptno + "'>详情</a>");
                out.print("		</td>");
                out.print("	</tr>");
                
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            
            //释放资源
            DBUtil.close(conn, ps, rs);
        }
        
        /*
        下面一部分是死的
        */
        out.print("	<!--一下是固定的-->");
        out.print("</table>");
        out.print("<hr>");
        out.print("<a href=' add.html'>新增部门</a>");
        out.print("</body>");
        out.print("</html>");
    }
}

```

- [x] 点击删除会出现404问题，解决
  - 配置web.xml

```java
 <!--删除-->
    <servlet>
        <servlet-name>delete</servlet-name>
        <servlet-class>oa.action.DeptDelServlet</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>delete</servlet-name>
        <url-pattern>/dept/delete</url-pattern>
    </servlet-mapping>
```

（3） 最终实现

<img src="https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424173259.png" alt="20220424173259" style="zoom:50%;" />

```java
package oa.action;

import com.sun.corba.se.spi.orb.PropertyParser;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import oa.untils.DBUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @Author: 小雷学长
 * @Date: 2022/3/22 - 10:05
 * @Version: 1.8
 */
public class DeptListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //获取应用的根路径
        String contextPath = request.getContextPath();

        //设置响应的内容类型
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        out.print("        <!DOCTYPE html>");
        out.print("<html lang='en'>");
        out.print("<head>");
        out.print("	<meta charset='UTF-8'>");
        out.print("	<title>部门列表</title>");
        out.print("</head>");

        out.print("<body>");
        out.print("<script type='text/javascript'>");
        out.print("        function del(dno) {");
        out.print("    if(window.confirm('亲，删了不可恢复哦！')){");
        out.print("        document.location.href = '"+contextPath+"/dept/delete?deptno=' + dno");
        out.print("    }");
        out.print("}");
        out.print("</script>");
        out.print("<h1 align='center'>部门列表</h1>");
        out.print("<hr>");
        out.print("<table border='1px' align='center' width='50%'>");
        out.print("	<tr>");
        out.print("		<th>序号</th>");
        out.print("		<th>部门编号</th>");
        out.print("		<th>部门名称</th>");
        out.print("		<th>操作</th>");
        out.print("	</tr>");
        out.print("	<!--以上是固定的-->");
        /*
        上面一部分是死的
         */

        /*
         * 连接数据库
         */
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {

            //获取连接
            conn = DBUtil.getConnection();

            //获取预编译的数据库操作对象
            String sql = "select deptno,dname ,loc from dept";
            ps = conn.prepareStatement(sql);

            //执行SQL语句
            rs = ps.executeQuery();

            //处理结果集
            int i = 0;
            while (rs.next()) {
                String deptno = rs.getString("deptno");
                String dname = rs.getString("dname");
                String loc = rs.getString("loc");

                out.print("	<tr>");
                out.print("		<td>" + (++i) + "</td>");
                out.print("		<td>" + deptno + "</td>");
                out.print("		<td>" + dname + "</td>");
                out.print("		<td>");
                out.print("<a href='javascript:void(0)' onclick='del("+ deptno +")'>删除</a>");
                out.print("			<a href='edit.html'>修改</a>");
                out.print("			<a href='" + contextPath + "/dept/detail?deptno=" + deptno + "'>详情</a>");
                out.print("		</td>");
                out.print("	</tr>");

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {

            //释放资源
            DBUtil.close(conn, ps, rs);
        }

        /*
        下面一部分是死的
         */
        out.print("	<!--一下是固定的-->");
        out.print("</table>");
        out.print("<hr>");
        out.print("<a href=' add.html'>新增部门</a>");
        out.print("</body>");
        out.print("</html>");
    }
}

```

```java
package oa.action;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import oa.untils.DBUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @Author: 小雷学长
 * @Date: 2022/3/22 - 15:01
 * @Version: 1.8
 */

/**
 * 实现查看部门详情
 */
public class DeptDetailServelt extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();


        out.print("<!DOCTYPE html>");
        out.print("<html lang='en'>");
        out.print("<head>");
        out.print("	<meta charset='UTF-8'>");
        out.print("	<title>部门详情</title>");
        out.print("</head>");
        out.print("<body>");
        out.print("<h1>部门详情</h1>");
        out.print("<hr>");


        //获取部门编号
        //虽然提交的是30，但服务器获取的是30这个字符串
        String deptno = request.getParameter("deptno");


        /*
         * 连接数据库
         */
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {

            //获取连接
            conn = DBUtil.getConnection();

            //获取预编译的数据库操作对象
            String sql = "select deptno,dname,loc from dept where deptno=?";
            ps = conn.prepareStatement(sql);

            ps.setString(1, deptno);
            //这个结果集一定只有一个元素


            //执行SQL语句
            rs = ps.executeQuery();

            //处理结果集
            if (rs.next()) {
                String dname = rs.getString("dname");
                String loc = rs.getString("loc");

                out.print("部门编号" + deptno + "<br>");
                out.print("部门名称" + dname + "<br>");
                out.print("部门位置" + loc + "<br>");


            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {

            //释放资源
            DBUtil.close(conn, ps, rs);
        }

        out.print("");
        out.print("<form action='list.html'>");
        out.print("	<input type='button' value='后退' onclick='window.history.back()'>");
        out.print("");
        out.print("</form>");
        out.print("");
        out.print("</body>");
        out.print("</html>");
    }
}
```

```java
package oa.action;

import com.sun.net.httpserver.HttpPrincipal;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jdk.internal.org.objectweb.asm.TypeReference;
import oa.untils.DBUtil;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @Author: 小雷学长
 * @Date: 2022/3/22 - 18:08
 * @Version: 1.8
 */
public class DeptDelServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //根据部门编号，删除部门
        //获取部门编号
        String deptno = request.getParameter("deptno");

        //连接数据库删除数据
        Connection conn = null;
        PreparedStatement ps = null;

        int count = 0;

        try {
            conn = DBUtil.getConnection();

            //开启事务（自动提交机制关闭）（非必要）
//            conn.setAutoCommit(false);

            String sql = "delete from dept where deptno=?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, deptno);

            //返回值是：影响了数据库表当中多少条记录
            count = ps.executeUpdate();

            //事务提交（非必要）
//            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            DBUtil.close(conn, ps, null);
        }

        //判断删除成功还是失败
        if (count == 1) {
            //删除成功
            //仍然跳回到部门列表页面
            //部门列表页面需要执行另一个Servlet，利用转发机制
            request.getRequestDispatcher("/dept/list").forward(request, response);

        } else {
            //删除失败
            request.getRequestDispatcher("/error.html").forward(request, response);
        }

    }
}

```

```java
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>error</title>
</head>
<body>
<h1>操作失败<a href="javascript:void(0)" onclick="window.history.back()">返回</a></h1>

</body>
</html>
```

```java
driver=com.mysql.jdbc.Driver
url=jdbc:mysql://localhost:3306/study
user=root
password=123456
```

```java
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">

    <!--查看部门列表-->
    <servlet>
        <servlet-name>list</servlet-name>
        <servlet-class>oa.action.DeptListServlet</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>list</servlet-name>
        <url-pattern>/dept/list</url-pattern>
    </servlet-mapping>

    <!--查看详情-->
    <servlet>
        <servlet-name>detail</servlet-name>
        <servlet-class>oa.action.DeptDetailServelt</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>detail</servlet-name>
        <url-pattern>/dept/detail</url-pattern>
    </servlet-mapping>

    <!--删除-->
    <servlet>
        <servlet-name>delete</servlet-name>
        <servlet-class>oa.action.DeptDelServlet</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>delete</servlet-name>
        <url-pattern>/dept/delete</url-pattern>
    </servlet-mapping>

</web-app>
```

[OA.7z](https://www.yuque.com/attachments/yuque/0/2022/7z/2560440/1647951299442-2d1baf30-7c84-4a86-b40d-2b2ff146b90a.7z?_lake_card=%7B%22src%22%3A%22https%3A%2F%2Fwww.yuque.com%2Fattachments%2Fyuque%2F0%2F2022%2F7z%2F2560440%2F1647951299442-2d1baf30-7c84-4a86-b40d-2b2ff146b90a.7z%22%2C%22name%22%3A%22OA.7z%22%2C%22size%22%3A2340816%2C%22type%22%3A%22%22%2C%22ext%22%3A%227z%22%2C%22status%22%3A%22done%22%2C%22taskId%22%3A%22u4fd86f8d-99ce-42fb-bc73-c9fc3a5188e%22%2C%22taskType%22%3A%22upload%22%2C%22id%22%3A%22uc9562daf%22%2C%22card%22%3A%22file%22%7D)

​	

1.8 部门新增功能

```java
package oa.action;

import com.sun.corba.se.spi.orb.PropertyParser;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import oa.untils.DBUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Queue;

/**
 * @Author: 小雷学长
 * @Date: 2022/3/22 - 10:05
 * @Version: 1.8
 */
public class DeptListServlet extends HttpServlet {


    //处理Post请求
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        doGet(request, response);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //获取应用的根路径
        String contextPath = request.getContextPath();

        //设置响应的内容类型
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        out.print("        <!DOCTYPE html>");
        out.print("<html lang='en'>");
        out.print("<head>");
        out.print("	<meta charset='UTF-8'>");
        out.print("	<title>部门列表</title>");
        out.print("</head>");

        out.print("<body>");
        out.print("<script type='text/javascript'>");
        out.print("        function del(dno) {");
        out.print("    if(window.confirm('亲，删了不可恢复哦！')){");
        out.print("        document.location.href = '" + contextPath + "/dept/delete?deptno=' + dno");
        out.print("    }");
        out.print("}");
        out.print("</script>");
        out.print("<h1 align='center'>部门列表</h1>");
        out.print("<hr>");
        out.print("<table border='1px' align='center' width='50%'>");
        out.print("	<tr>");
        out.print("		<th>序号</th>");
        out.print("		<th>部门编号</th>");
        out.print("		<th>部门名称</th>");
        out.print("		<th>操作</th>");
        out.print("	</tr>");
        out.print("	<!--以上是固定的-->");
        /*
        上面一部分是死的
         */

        /*
         * 连接数据库
         */
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {

            //获取连接
            conn = DBUtil.getConnection();

            //获取预编译的数据库操作对象
            String sql = "select deptno,dname ,loc from dept";
            ps = conn.prepareStatement(sql);

            //执行SQL语句
            rs = ps.executeQuery();

            //处理结果集
            int i = 0;
            while (rs.next()) {
                String deptno = rs.getString("deptno");
                String dname = rs.getString("dname");
                String loc = rs.getString("loc");

                out.print("	<tr>");
                out.print("		<td>" + (++i) + "</td>");
                out.print("		<td>" + deptno + "</td>");
                out.print("		<td>" + dname + "</td>");
                out.print("		<td>");
                out.print("<a href='javascript:void(0)' onclick='del(" + deptno + ")'>删除</a>");
                out.print("			<a href='edit.html'>修改</a>");
                out.print("			<a href='" + contextPath + "/dept/detail?deptno=" + deptno + "'>详情</a>");
                out.print("		</td>");
                out.print("	</tr>");

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {

            //释放资源
            DBUtil.close(conn, ps, rs);
        }

        /*
        下面一部分是死的
         */
        out.print("	<!--一下是固定的-->");
        out.print("</table>");
        out.print("<hr>");
        out.print("<a href='" + contextPath + "/add.html'>新增部门</a>");
        out.print("</body>");
        out.print("</html>");
    }
}

```

```java
package oa.action;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import oa.untils.DBUtil;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @Author: 小雷学长
 * @Date: 2022/3/22 - 20:36
 * @Version: 1.8
 */
public class DeptSaveServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


        //获取部门信息
        String deptno = request.getParameter("deptno");
        String dname = request.getParameter("dname");
        String loc = request.getParameter("loc");

        //连接数据库执行insert语句
        Connection conn = null;
        PreparedStatement ps = null;
        int count = 0;

        try {
            conn = DBUtil.getConnection();

            String sql = "insert into dept(deptno,dname,loc) values(?,?,?)";
            ps = conn.prepareStatement(sql);
            ps.setString(1, deptno);
            ps.setString(2, dname);
            ps.setString(3, loc);

            count = ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            DBUtil.close(conn, ps, null);
        }

        //判断新增成功还是失败
        if (count == 1) {
            //新增成功
            //仍然跳回到部门列表页面
            //部门列表页面需要执行另一个Servlet，利用转发机制
            //转发一次请求
            request.getRequestDispatcher("/dept/list").forward(request, response);

        } else {
            //新增失败
            request.getRequestDispatcher("/error.html").forward(request, response);
        }
    }
}


```

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>新增部门</title>
  </head>
  <body>
    <h1>新增部门</h1>
    <hr>
    <form action="/oa/dept/save" method="post">
      部门编号<input type="text" name="deptno"><br>
      部门名称<input type="text" name="dname"><br>
      部门位置<input type="text" name="loc"><br>
      <input type="submit" value="保存"><br>
      
    </form>
    
  </body>
</html>
```

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
  
  <!--查看部门列表-->
  <servlet>
    <servlet-name>list</servlet-name>
    <servlet-class>oa.action.DeptListServlet</servlet-class>
  </servlet>
  <servlet-mapping>
    <servlet-name>list</servlet-name>
    <url-pattern>/dept/list</url-pattern>
  </servlet-mapping>
  
  <!--查看详情-->
  <servlet>
    <servlet-name>detail</servlet-name>
    <servlet-class>oa.action.DeptDetailServelt</servlet-class>
  </servlet>
  <servlet-mapping>
    <servlet-name>detail</servlet-name>
    <url-pattern>/dept/detail</url-pattern>
  </servlet-mapping>
  
  <!--删除-->
  <servlet>
    <servlet-name>delete</servlet-name>
    <servlet-class>oa.action.DeptDelServlet</servlet-class>
  </servlet>
  <servlet-mapping>
    <servlet-name>delete</servlet-name>
    <url-pattern>/dept/delete</url-pattern>
  </servlet-mapping>
  
  <!--保存-->
  <servlet>
    <servlet-name>save</servlet-name>
    <servlet-class>oa.action.DeptSaveServlet</servlet-class>
  </servlet>
  <servlet-mapping>
    <servlet-name>save</servlet-name>
    <url-pattern>/dept/save</url-pattern>
  </servlet-mapping>
  
</web-app>
```

​	

1.9 实现部门的修改

```java
package oa.action;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import oa.untils.DBUtil;

import java.awt.print.Printable;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
* @Author: 小雷学长
* @Date: 2022/3/23 - 11:19
* @Version: 1.8
*/
public class DeptUpdateServlet extends HttpServlet {
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        
        
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        
        String deptno = request.getParameter("deptno");
        String dname = request.getParameter("dname");
        String loc = request.getParameter("loc");
        
        //连接数据库执行insert语句
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int count = 0;
        
        try {
            conn = DBUtil.getConnection();
            
            String sql = "update dept set dname = ?,loc = ? where deptno = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, dname);
            ps.setString(2, loc);
            ps.setString(3, deptno);
            
            count = ps.executeUpdate();
            
        } catch (SQLException e) {
            e.printStackTrace();
            
        } finally {
            DBUtil.close(conn, ps, rs);
        }
        
        if (count == 1){
            //更新成功
            //跳转到部门列表页面，转发机制
            request.getRequestDispatcher("/dept/list").forward(request,response);
        }
        else {
            //更新失败
            request.getRequestDispatcher("/error.html").forward(request,response);
            
        }
    }
}

```

```java
package oa.action;

import com.sun.net.httpserver.HttpPrincipal;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import oa.untils.DBUtil;

import javax.xml.transform.Result;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @Author: 小雷学长
 * @Date: 2022/3/22 - 22:59
 * @Version: 1.8
 */
public class DeptEditServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        String contextPath = request.getContextPath();


        out.print("<html lang='en'>");
        out.print("<head>");
        out.print("	<meta charset='UTF-8'>");
        out.print("	<title>修改部门</title>");
        out.print("</head>");
        out.print("<body>");
        out.print("<h1>修改部门</h1>");
        out.print("<hr>");
        out.print("<form action='" + contextPath + "/dept/update' method='post'>");


        //获取部门编号
        String deptno = request.getParameter("deptno");

        //连接数据库执行insert语句
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = DBUtil.getConnection();

            String sql = "select dname,loc from dept where deptno=?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, deptno);

            rs = ps.executeQuery();

            if (rs.next()) {
                String dname = rs.getString("dname");
                String loc = rs.getString("loc");

                //输出动态网页
                out.print("                部门编号<input type='text' name='deptno' value='" + deptno + "' readonly><br><!--readonly只读-->");
                out.print("                部门名称<input type='text' name='dname' value='" + dname + "'><br>");
                out.print("                部门位置<input type='text' name='loc' value='" + loc + "'><br>");

            }

        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            DBUtil.close(conn, ps, rs);
        }

        //判断新增成功还是失败

        out.print("	<input type='submit' value='修改'><br>");
        out.print("");
        out.print("</form>");
        out.print("");
        out.print("</body>");
        out.print("</html>");

    }
}

```

```java
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">

    <!--查看部门列表-->
    <servlet>
        <servlet-name>list</servlet-name>
        <servlet-class>oa.action.DeptListServlet</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>list</servlet-name>
        <url-pattern>/dept/list</url-pattern>
    </servlet-mapping>

    <!--查看详情-->
    <servlet>
        <servlet-name>detail</servlet-name>
        <servlet-class>oa.action.DeptDetailServelt</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>detail</servlet-name>
        <url-pattern>/dept/detail</url-pattern>
    </servlet-mapping>

    <!--删除-->
    <servlet>
        <servlet-name>delete</servlet-name>
        <servlet-class>oa.action.DeptDelServlet</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>delete</servlet-name>
        <url-pattern>/dept/delete</url-pattern>
    </servlet-mapping>

    <!--保存-->
    <servlet>
        <servlet-name>save</servlet-name>
        <servlet-class>oa.action.DeptSaveServlet</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>save</servlet-name>
        <url-pattern>/dept/save</url-pattern>
    </servlet-mapping>

    <!--修改页面-->
    <servlet>
        <servlet-name>edit</servlet-name>
        <servlet-class>oa.action.DeptEditServlet</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>edit</servlet-name>
        <url-pattern>/dept/edit</url-pattern>
    </servlet-mapping>

    <!--修改部门-->
    <servlet>
        <servlet-name>update</servlet-name>
        <servlet-class>oa.action.DeptUpdateServlet</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>update</servlet-name>
        <url-pattern>/dept/update</url-pattern>
    </servlet-mapping>

   
</web-app>
```

### 21.2 实现预览

项目方法未完善，后续在下文改造[改造OA2](https://www.yuque.com/xleixz/aygur6/xr24gr#oTpEu)



![image-20220523002737596](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220523002737596.png)

![20220424173729](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424173729.png)

![20220424173737](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424173737.png)

![20220424173745](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424173745.png)

![20220424173751](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424173751.png)

![20220424173755](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424173755.png)

![20220424173800](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424173800.png)

---

​	

## 22 转发和重定向

- ✅一个web应用中通过两种方式，可以完成资源的转跳

- - 转发
  - 重定向

### 22.1 转发

- ✅转发是一次请求，不管转发多少次，都在一个Servlet中
- ✅请求路径不需要加项目名，因为他不是前端请求，是Tomcat内部跳转

```java
//获取请求转发器对象
RequestDispatcher dispatcher = request.getRequestDispatcher("/dept/list");

//调用请求转发器对象的forward方法完成转发
dispatcher.forward(request,response);

//合并一行代码
request.getRequestDispatcher("/dept/list").forward(request,response);
```

### 22.2 重定向

- ✅重定向时的路径当中需要以项目名开始，需要添加项目名
- ✅浏览器发送请求，请求路径上需要添加项目名
- ✅response对象将这个路径："/项目名/b"响应给了浏览器，浏览器又自发的向服务器发送了一个全新的请求：`http://localhost:8080/项目名/b`
- ✅所以浏览器一共发送了两次请求

- - 第一次请求：`http://localhost:8080/项目名/a`
  - 第二次请求：`http://localhost:8080/项目名/b`
  - 最终浏览器地址栏上显示地址当然是最后一次请求的地址，所以重定向会导致浏览器地址栏上的地址发生改变

```java
//重定向
reponse.sendRedirect(request.getContextPath() + "/项目/b");
```

### 22.3 转发和重定向的区别

- ✅转发：在浏览器地址栏上发送的请求是什么，请求结束后，浏览器的地址栏还是这个，没变
- ✅重定向：浏览器地址上的地址会根据第二次请求的路径而改变

------

- ✅转发：是由WEB服务器来控制的，A资源转跳到B资源，这个跳转的动作是Tomcat服务器内部完成的
- ✅重定向：是浏览器完成的，具体转跳的哪个资源，浏览器决定

------

- ✅用一个例子去描述转发和重定向

- - 借钱（转发：发送一次请求）

- - - 老王没钱了，找张三借钱，其实张三没有钱，但是张三够义气，张三自己找李四借了钱，然后张三把这个钱给了老王，老王不知道这个钱是李四的，老王只求了一个人

- - 借钱（重定向：发送两次请求）

- - - 老王没钱了，找张三借钱，张三没有钱，张三有个好哥们叫李四，张三把这个地址告诉了老王，然后老王按着这个地址去找到了李四借了钱，求了两个人，而且老王知道这个钱是李四借给他的

------

![20220424174454](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424174454.png)



![20220424174429](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424174429.png)

### 22.4 如何选择转发和重定向？

- ✅如果在上一个Servlet当中向request域中绑定了数据，希望从下一个Servlet当中把request域里的数据取出来，使用转发机制
- ✅剩下所有的请求均使用重定向 （重定向较多）

![20220424174509](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424174509.png)



- ✅转发会存在浏览器的刷新问题，可以用重定向解决问题

![20220424174516](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424174516.png)

---

​	

## 23 Servlet中的注解式开发

### 23.1 Servlet注解，简化配置

- [x] oa项目中的web.xml文件
  - 当开发项目过大时，web.xml中需要配置的文件会非常庞大，可能会有几十兆
  - 在web.xml文件中进行Servlet信息的配置，显然开发效率较低，每一个都需要配置、
  - 而且在web.xml文件中的配置是很少需要修改的
- [x] Servlet3.0版本之后，退出了各种Servlet基于注解式开发
  - 优点
    - 开发效率高，不需要编写大量的配置信息，直接写在Java类上使用注解进行标注
    - web.xml文件体积变小了
  - 并不是有了注解之后，web.xml文件就不需要了
    - 有的一些需要变化的信息，还是需要配置到web.xml文件中，一般都是`注解+配置文件`的开发模式
    - 一些不会经常变化修改的配置建议使用注解，一些可能会被修改的建议写到配置文件中

​	

### 23.2 注解的使用

- [x] 第一个注解

  ```java
  import jakarta.servlet.annotation.WebServlet;
  ```

  

![20220424174658](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424174658.png)

- [x] Servlet类上使用：`@WebServlet`
- [x] Servlet注解的属性
  - `name`属性L：用来指定Servlet的名字，等同于`<servlet-name>`
  - `urlPatterns`属性：用来指定Servlet的映射路径，可以指定多个字符串，等同于`url-pattern`
  - `loadOnStartUp`属性：用来指定服务器在启动阶段是否加载Servlet，等同于`<load-on-startup>`
  - `value`属性
    - ⚠️如果注解的属性名是value的话，属性名可以省略`@WebServlet(value = "/a")`可以写为`@WebServlet("/a")`
- [x] 注解对象的使用格式`initPatams`
  - `@注解名称(属性名=属性值,属性名=属性值,属性名=属性值)`

![20220424174739](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424174739.png)

- [x] 不需要将所有的属性都写上，只需要提供需要的（需要什么用什么）

⚠️当注解的属性是一个数组，并且数字中只有一个元素，大括号可以省略

​	

### 23.3 模板方法设计模式解决类爆炸

- [x] 如果一个复杂的业务系统，类太多，会导致类爆炸（类的数量急剧增多）
- [x] 可以使用模板设计模式解决类爆炸

❓如何解决类爆炸问题

   - 一个请求对应一个方法
   - 一个业务对应一个Servlet类
   - 处理部门的业务的对应一个DeptServlet
   - 处理用户相关的业务对应一个UserServlet
   - 处理银行卡片业务的对应一个CardServlet
- [x] 通配写法

![20220424174748](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424174748.png)

```java
package oa.action;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import oa.untils.DBUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @Author: 小雷学长
 * @Date: 2022/3/23 - 21:31
 * @Version: 1.8
 */

//模板类
@WebServlet({"/dept/list", "/dept/save", "/dept/edit", "/dept/detail", "/dept/delete", "/dept/update"})
//通配写法
//只要是路径已"/dept"开始的，都走Servlet
//@WebServlet("/dept/*")
public class DeptServlet extends HttpServlet {

    //模板方法
    //重写Servlet方法（并没有重写doGet或者doPost


    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //获取ServletPath
        String servletPath = request.getServletPath();

        if ("/dept/list".equals(servletPath)) {
            doList(request, response);
        } else if ("/dept/save".equals(servletPath)) {
            doSave(request, response);
        } else if ("/dept/edit".equals(servletPath)) {
            doEdit(request, response);
        } else if ("/dept/detail".equals(servletPath)) {
            doDeail(request, response);
        } else if ("/dept/delete".equals(servletPath)) {
            doDelete(request, response);
        } else if ("/dept/update".equals(servletPath)) {
            doUpdate(request, response);
        }
    }

    private void doList(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        {
            //获取应用的根路径
            String contextPath = request.getContextPath();

            //设置响应的内容类型
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();

            out.print("        <!DOCTYPE html>");
            out.print("<html lang='en'>");
            out.print("<head>");
            out.print("	<meta charset='UTF-8'>");
            out.print("	<title>部门列表</title>");
            out.print("</head>");

            out.print("<body>");
            out.print("<script type='text/javascript'>");
            out.print("        function del(dno) {");
            out.print("    if(window.confirm('亲，删了不可恢复哦！')){");
            out.print("        document.location.href = '" + contextPath + "/dept/delete?deptno=' + dno");
            out.print("    }");
            out.print("}");
            out.print("</script>");
            out.print("<h1 align='center'>部门列表</h1>");
            out.print("<hr>");
            out.print("<table border='1px' align='center' width='50%'>");
            out.print("	<tr>");
            out.print("		<th>序号</th>");
            out.print("		<th>部门编号</th>");
            out.print("		<th>部门名称</th>");
            out.print("		<th>操作</th>");
            out.print("	</tr>");
            out.print("	<!--以上是固定的-->");
        /*
        上面一部分是死的
         */

            /*
             * 连接数据库
             */
            Connection conn = null;
            PreparedStatement ps = null;
            ResultSet rs = null;

            try {

                //获取连接
                conn = DBUtil.getConnection();

                //获取预编译的数据库操作对象
                String sql = "select deptno,dname ,loc from dept";
                ps = conn.prepareStatement(sql);

                //执行SQL语句
                rs = ps.executeQuery();

                //处理结果集
                int i = 0;
                while (rs.next()) {
                    String deptno = rs.getString("deptno");
                    String dname = rs.getString("dname");
                    String loc = rs.getString("loc");

                    out.print("	<tr>");
                    out.print("		<td>" + (++i) + "</td>");
                    out.print("		<td>" + deptno + "</td>");
                    out.print("		<td>" + dname + "</td>");
                    out.print("		<td>");
                    out.print("<a href='javascript:void(0)' onclick='del(" + deptno + ")'>删除</a>");
                    out.print("			<a href='/oa/dept/edit?deptno=" + deptno + "'>修改</a>");
                    out.print("			<a href='" + contextPath + "/dept/detail?deptno=" + deptno + "'>详情</a>");
                    out.print("		</td>");
                    out.print("	</tr>");

                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {

                //释放资源
                DBUtil.close(conn, ps, rs);
            }

        /*
        下面一部分是死的
         */
            out.print("	<!--一下是固定的-->");
            out.print("</table>");
            out.print("<hr>");
            out.print("<a href='" + contextPath + "/add.html'>新增部门</a>");
            out.print("</body>");
            out.print("</html>");
        }
    }


    private void doSave(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        {


            //获取部门信息
            String deptno = request.getParameter("deptno");
            String dname = request.getParameter("dname");
            String loc = request.getParameter("loc");

            //连接数据库执行insert语句
            Connection conn = null;
            PreparedStatement ps = null;
            int count = 0;

            try {
                conn = DBUtil.getConnection();

                String sql = "insert into dept(deptno,dname,loc) values(?,?,?)";
                ps = conn.prepareStatement(sql);
                ps.setString(1, deptno);
                ps.setString(2, dname);
                ps.setString(3, loc);

                count = ps.executeUpdate();

            } catch (SQLException e) {
                e.printStackTrace();

            } finally {
                DBUtil.close(conn, ps, null);
            }

            //判断新增成功还是失败
            if (count == 1) {
                //新增成功
                //仍然跳回到部门列表页面
                //部门列表页面需要执行另一个Servlet，利用转发机制
                //转发一次请求
//            request.getRequestDispatcher("/dept/list").forward(request, response);

                //建议使用重定向,因为重定向是一次全新的请求，不会受dopost、doget方法影响
                response.sendRedirect(request.getContextPath() + "/dept/list");
            } else {
                //新增失败
//            request.getRequestDispatcher("/error.html").forward(request, response);

                //建议使用重定向,因为重定向是一次全新的请求，不会受dopost、doget方法影响
                response.sendRedirect(request.getContextPath() + "/error.hmtl");
            }


        }
    }

    private void doEdit(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        {

            response.setContentType("text/html");
            PrintWriter out = response.getWriter();

            String contextPath = request.getContextPath();


            out.print("<html lang='en'>");
            out.print("<head>");
            out.print("	<meta charset='UTF-8'>");
            out.print("	<title>修改部门</title>");
            out.print("</head>");
            out.print("<body>");
            out.print("<h1>修改部门</h1>");
            out.print("<hr>");
            out.print("<form action='" + contextPath + "/dept/update' method='post'>");


            //获取部门编号
            String deptno = request.getParameter("deptno");

            //连接数据库执行insert语句
            Connection conn = null;
            PreparedStatement ps = null;
            ResultSet rs = null;

            try {
                conn = DBUtil.getConnection();

                String sql = "select dname,loc from dept where deptno=?";
                ps = conn.prepareStatement(sql);
                ps.setString(1, deptno);

                rs = ps.executeQuery();

                if (rs.next()) {
                    String dname = rs.getString("dname");
                    String loc = rs.getString("loc");

                    //输出动态网页
                    out.print("                部门编号<input type='text' name='deptno' value='" + deptno + "' readonly><br><!--readonly只读-->");
                    out.print("                部门名称<input type='text' name='dname' value='" + dname + "'><br>");
                    out.print("                部门位置<input type='text' name='loc' value='" + loc + "'><br>");

                }

            } catch (SQLException e) {
                e.printStackTrace();

            } finally {
                DBUtil.close(conn, ps, rs);
            }

            //判断新增成功还是失败

            out.print("	<input type='submit' value='修改'><br>");
            out.print("");
            out.print("</form>");
            out.print("");
            out.print("</body>");
            out.print("</html>");

        }
    }

    private void doDeail(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        {

            response.setContentType("text/html");
            PrintWriter out = response.getWriter();


            out.print("<!DOCTYPE html>");
            out.print("<html lang='en'>");
            out.print("<head>");
            out.print("	<meta charset='UTF-8'>");
            out.print("	<title>部门详情</title>");
            out.print("</head>");
            out.print("<body>");
            out.print("<h1>部门详情</h1>");
            out.print("<hr>");


            //获取部门编号
            //虽然提交的是30，但服务器获取的是30这个字符串
            String deptno = request.getParameter("deptno");


            /*
             * 连接数据库
             */
            Connection conn = null;
            PreparedStatement ps = null;
            ResultSet rs = null;

            try {

                //获取连接
                conn = DBUtil.getConnection();

                //获取预编译的数据库操作对象
                String sql = "select deptno,dname,loc from dept where deptno=?";
                ps = conn.prepareStatement(sql);

                ps.setString(1, deptno);
                //这个结果集一定只有一个元素


                //执行SQL语句
                rs = ps.executeQuery();

                //处理结果集
                if (rs.next()) {
                    String dname = rs.getString("dname");
                    String loc = rs.getString("loc");

                    out.print("部门编号" + deptno + "<br>");
                    out.print("部门名称" + dname + "<br>");
                    out.print("部门位置" + loc + "<br>");


                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {

                //释放资源
                DBUtil.close(conn, ps, rs);
            }

            out.print("");
            out.print("<form action='list.html'>");
            out.print("	<input type='button' value='后退' onclick='window.history.back()'>");
            out.print("");
            out.print("</form>");
            out.print("");
            out.print("</body>");
            out.print("</html>");
        }
    }

    public void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        {

            //根据部门编号，删除部门
            //获取部门编号
            String deptno = request.getParameter("deptno");

            //连接数据库删除数据
            Connection conn = null;
            PreparedStatement ps = null;

            int count = 0;

            try {
                conn = DBUtil.getConnection();

                //开启事务（自动提交机制关闭）（非必要）
//            conn.setAutoCommit(false);

                String sql = "delete from dept where deptno=?";
                ps = conn.prepareStatement(sql);
                ps.setString(1, deptno);

                //返回值是：影响了数据库表当中多少条记录
                count = ps.executeUpdate();

                //事务提交（非必要）
//            conn.commit();
            } catch (SQLException e) {
                e.printStackTrace();

            } finally {
                DBUtil.close(conn, ps, null);
            }

            //判断删除成功还是失败
            if (count == 1) {
                //删除成功
                //仍然跳回到部门列表页面
                //部门列表页面需要执行另一个Servlet，利用转发机制
                //request.getRequestDispatcher("/dept/list").forward(request, response);

                //建议使用重定向,因为重定向是一次全新的请求，不会受dopost、doget方法影响
                response.sendRedirect(request.getContextPath() + "/dept/list");

            } else {
                //删除失败
                //建议使用重定向,因为重定向是一次全新的请求，不会受dopost、doget方法影响
                response.sendRedirect(request.getContextPath() + "/error.html");
            }

        }

    }

    private void doUpdate(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        {


            response.setContentType("text/html");
            PrintWriter out = response.getWriter();

            String deptno = request.getParameter("deptno");
            String dname = request.getParameter("dname");
            String loc = request.getParameter("loc");

            //连接数据库执行insert语句
            Connection conn = null;
            PreparedStatement ps = null;
            ResultSet rs = null;
            int count = 0;

            try {
                conn = DBUtil.getConnection();

                String sql = "update dept set dname = ?,loc = ? where deptno = ?";
                ps = conn.prepareStatement(sql);
                ps.setString(1, dname);
                ps.setString(2, loc);
                ps.setString(3, deptno);

                count = ps.executeUpdate();

            } catch (SQLException e) {
                e.printStackTrace();

            } finally {
                DBUtil.close(conn, ps, rs);
            }

            if (count == 1) {
                //更新成功
                //跳转到部门列表页面，转发机制
//    request.getRequestDispatcher("/dept/list").forward(request,response);

//建议使用重定向,因为重定向是一次全新的请求，不会受dopost、doget方法影响
                response.sendRedirect(request.getContextPath() + "/dept/list");
            } else {
                //更新失败
//    request.getRequestDispatcher("/error.html").forward(request,response);

                //建议使用重定向,因为重定向是一次全新的请求，不会受dopost、doget方法影响
                response.sendRedirect(request.getContextPath() + "/error.html");
            }
        }
    }
}


```

---

​	

## 24 总结获取路径

✅`request.getServletPath();`请求路径，注解内容

✅`request.getContextPath();`根目录（项目webapp名称）

![20220424174845](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424174845.png)

---

## JSP 语法

## 1 在Servlet中编写前端代码的问题

⚠️Java程序中编写前端代码，编写难度大，麻烦

⚠️Java程序中编写前端代码，显然程序的耦合度非常高

⚠️Java程序中编写前端代码，代码非常不美观

⚠️Java程序中编写前端代码，维护成本太高（不容易发现错误）

⚠️修改一个小小的前端代码，只要有改动，就需要重新编译Java代码，生成新的class文件，打一个新的war包，重新发布

​	

## 2 理解JSP

:mag:JSP就是Java程序（JSP的本质就是Servlet）

:mag:JSP是JavaServletPages的缩写（基于Java语言实现的服务器端的页面）

:mag:JSP也是JavaEE中13中规范之一

:mag:每一个web容器/web服务器都会内置一个JSP翻译引擎

:mag:jsp文件在WEB-INF目录之外，在webapp的根目录下

:mag:实际上访问`http://localhost:8080/jsp/index.jsp`这个index.jsp文件底层执行的是：index_jsp.class文件这个程序

:mag:这个index.jsp会被tomcat翻译生成index_jsp.java文件，然后tomcat服务器又将index.jsp编译生成index_jsp.class文件

:mag:访问index.jsp实际上执行的是index_jsp.class中的方法

:mag:JSP实际上是一个Servlet

+ index.jsp访问的时候，会自动翻译生成index_jsp.java，再自动百编译生成index_jsp.class，那么index_jsp这就是一个类

- index_jsp类继承HttpServlet
- jsp的生命周期和Servlet的生命周期完全相同

:mag:JSP文件第一次访问比较慢，因为大部分运维人员在演示项目的时候，会提前把所有的jsp文件都访问一遍

:mag:jsp文件中直接编写文字，被翻译到servlet类的service方法的out.writer("翻译到这里")，直接翻译到双引号里，打印输出到浏览器

​	

## 3 JSP的基础语法和输出语句

### 3.1 JSP的基础语句

`< %Java语法; %>`

- > 在这个符号中编写的视为Java程序，被翻译到Servlet类的service方法内部

- > `<%  %>`在这个里面写Java的时候，要时刻记住是在方法体中写代码

<img src="https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424214505.png" alt="20220424214505" style="zoom:50%;" />

​	

### 3.2 JSP专业注释

```jsp
<!-- HTML的注释，这个注释不专业，仍然会被翻译到Java源码中，在JSP中不要使用这种注释-->
<%--JSP专业，这个注释信息不会被翻译到Java源码中，建议使用这种注释信息--%>
```

:mag:在Service方法中不能使用private等访问权修饰变量，`<%  %> `这个相当于是Java里的方法，不能在方法体中编写静态代码块，不能再方法体中写方法，不能方法套方法；

:mag:在JSP中`<% %>`可以出现多个；

:mag:每一行都是Java语句，要符合Java规范；

:mag:`<%!  %>`这个符号编写的Java代码块会被翻译到service方法之外，这个很少用。因为：

![20220424214644](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424214644.png)

​	

### 3.3 JSP输出语句

```jsp
<%  String name = "jack"; out.write("name1 = " + name);%>
```

⚠️以上代码中的==out==是JSP的==九大内置对象之一==，可以直接拿来用，当然必须==只能在service方法内部使用==

<img src="https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424214826.png" alt="20220424214826" style="zoom:50%;" />

✅如果向浏览器上输出的内容没有Java代码，可以直接在JSP里编写，不需要写到`<% %>`里

![20220424214847](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424214847.png)

✅如果输出的内容含有java代码，这个时候可以使用一下格式的语法

- `<% = %>`注意：在`=`后面编写要输出的内容相当于`out.print();`

- ==当输出的是一个动态变量时使用，因为输出的内容可以直接在JSP里编写==

![20220424214900](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424214900.png)

- - 所以`<% = %>`最终被翻译成了java代码：`out.print();`

​	

### 3.4 JSP基础语法总结

- JSP中直接编写普通字符串

- - 翻译到service方法的out.write("这里");

- <% %>

- - 翻译到service方法体内部，里面一条一条的java语句

- <%! %>

- - 翻译到service方法之外

- <% = %>

- - 翻译为service方法的out.print();

- <%@ page contentType ="text/html;charset=UTF-8"%>

- - page指令，通过contentType属性用来设置响应内容类型
  - charst指令，采用的字符集是UTF-8

- <%-- --%>

- - JSP专业注释

​		

## 4 JSP和Servlet的本质区别（面试题）

> 职责不同

Servlet的职责是：收集数据

Servlet最强项的是逻辑处理，业务处理，然后连接数据库，获取/收集数据

JSP的职责是：展示数据

JSP最强项的是做数据的展示

​	

## 5 JSP的page指令，解决响应时的中文乱码问题

:mag:配置指：`<%@page contentType="text/html;charset=UTF-8" %>`表示响应内容时text/html，采用的字符集是UTF-8

![20220424182056](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424182056.png)

相当于：

![20220424182519](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220424182519.png)

---

​	

---

## session

## 一、理解session

- [x] session：会话
- [x] 一次会话：用户打开浏览器，在进行一些列操作之后，最终将浏览器关闭，这个过程叫做一次会话。会话在服务器端有一个java对象，这个对象叫做session
- [x] 一次请求：用户在浏览器上点击一下，然后在页面停了下来，可以粗略认为是一次请求，请求对应的服务器端的对象叫做request
- [x] 一次会话对应N次请求（一次会话包含多个请求）
- [x] 为什么要session对象来保存会话状态？
  - 因为Http协议是一种无状态协议
    - 无状态：请求的时候，B和S是连接的，但是请求结束之后，连接就断开了（为了减轻服务器的压力）
  - 只要B和S断开了，==关闭浏览器这个动作，服务器是不知道的==（关闭浏览器和退出账号是不一样的，一个安全，一个不安全）
- [x] session是以cookie形式保存在浏览器中

​	

## 二、语法规范

- [x] 在java的Servlet规范中，session对应的类名是：`HttpSession(jakarta.servlet.http.HttpSession)`
- [x] 获取session对象
  - `HttpSession session = request_._getSession();`
- [x] 向会话域中绑定数据
  - `session.setAttribute();`
- [x] 从会话域取出数据
  - `Object obj = session.getAttribute();`

​	

## 三、session实现原理

✅session列表是一个Map，map的key是sessionid，map的value是session对象

✅用户发送第一次请求，服务器生成session对象，同时生成id，将id发送给浏览器

✅用户发送第二次请求，自动将浏览器内存中的id发送给服务器，服务器根据id找到session对象

✅关闭浏览器，内存小时，cookie消失，sessionid消失，会话等同于结束

​	

⚠️cookie禁用了，session还能找到吗？

✅cookie禁用：服务器正常发送cookie给浏览器，但是浏览器不要了，拒收了，并不是服务器不发了

✅cookie禁用了，session机制仍然可以实现，需要使用URL重启机制

- URL重写会增高开发者的成本，开发者在编写任何请求路径的时候，都需要在后面添加一个sessionid

​	

## 四、session对象的获取

✅获取session对象

`HttpSession session = request*.*getSession();`

作用：从WEB服务器当中获取session对象，如果session对象没有，则新建

✅seesion不新建对象

`HttpSession session = request*.*getSession(false);`

作用：从WEB服务器中获取session对象，如果session对象没有，则返回一个null

​	

### 4.1 session什么时候被销毁？

⚠️浏览器关闭的时候，浏览器服务器是不知道的

✅一种销毁：是超时销毁（超时机制）

✅一种销毁：是手动销毁（点击退出按钮）`session*.*invalidate();`

​	

### 4.2 超时机制

✅在web.xml中设置session超时时长

⚠️如果不配，默认是30分钟，设置路径在：Apache-tomcat/conf/web.xml中第638行

```xml
 <!--session的超时时长是30分钟-->
    <!--如果30分钟过去了，session对象仍然没有被访问，session会被销毁-->
    <session-config>
        <session-timeout>30</session-timeout>
    </session-config>
    
```

![20220425003558](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425003558.png)

​	

###  4.3 注意

✅JSP会在程序启动时，创建session对象，使得session对象不为空，可以在inedx.jsp中设置，使得JSP不创建seesion，但是不影响seesion的功能

![20220425003613](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/20220425003613.png)

​	

### 4.4 域对象的总结

✅request（对应的类名：HttpServletRequest）

- 请求域（请求级别的）

✅session（对应的类名：HttpSession）

- 会话域（用户级别的）

✅application（对应的类名：ServletContext）

- 应用域（项目级别的，所有用户共享的）

------

▶️这三个域的大小关系

- request < session < application

------

▶️他们三个域对象都有以下三个公用的方法

- `setAttribute`（向域当中绑定数据）
- `getAttribute`（从域当中获取数据）
- `removeAttribute`（删除域当中的数据）

------

▶️使用原则：尽量使用小的域
