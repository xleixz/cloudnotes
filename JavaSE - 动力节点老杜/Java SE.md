# Java SE

> 收录、整理**重点**知识，SE较容易基础部分略过。

## 1、基础 - class简介

* 一个java源文件中可以定义多个class

- 一个java源文件中public的class不是必须的

- 一个class会定义生成一个xxx.class字节码文件

- 一个java源文件中定义公开的类，public的class只能有一个，并且该类必须和java源文件名称一致

- 每一个class中都可以编写main方法，都可以定义程序的入口

  ```java
  public class K {
      public static void main(String[] args){
          System.out.println("K");
      }
  }
  
  class a {
      public static void main(String[] args){
          System.out.println("a");
      }
  }
  class b {
      public static void main(String[] args){
          System.out.println("b");
      }
  }
  ```

## 2、基础 - 标识符

标识符定义：类名，方法名，接口名，变量名……都是标识符，<font color = "green">**一个合法的标识符只能由数字、字母、下划线、美元符号$组成，不能带数字开头，严格区分大小写，关键字不能做标识符。**</font>

标识符命名规范：

1. 最好见名知意
2. 遵守驼峰命名规范
   1. 类名、接口名，**首字母大写，后面每个单词首字母大写**
   2. 变量名、方法名，**首字母小写，后面每个单词首字母大写**

## 3、基础 - 变量

变量：内存中的一块空间，这块空间有“数据类型”、“名称”、“字面值”。变量是内存中存储数据的最基本的单元。

数据类型的作用：不同的数据类型会分配不同的大小空间。数据类型是指导程序在运行阶段应该分配多大的内存空间。

**声明/定义变量的语法格式：**数据类型 + 变量名；

**变量声明后赋值方法：**变量名 = 字面值；= 等号是一个运算符，叫做赋值运算符；

**声明和赋值可以一起完成**；

**变量赋值之后，可以重新赋值**；

> 变量的作用域

注意：变量出了大括号 } 就失效了，这就是变量的作用域。

在不同的作用域中，变量名可以重复；

在同一作用域中，变量名不可以重复；

> 变量的分类

成员变量：在方法体外声明的变量；

局部变量：在方法体内声明的变量；

## 4、基础 - 数据类型

> 数据类型的作用

指导JVM在运行程序是给数据分配多大的内存空间。

> 数据类型分类

基本数据类型和引用数据类型。

基本数据类型包括四大类大小种：

1. 整数型 byte，short，int，long
2. 浮点型  float，double
3. 布尔型  boolean
4. 字符型  char

**字符串不属于基本数据类型，属于引用数据类型。**

> 八中数据类型各自占用空间大小

基本数据类型                             占用空间大小【单位：字节】

--------------------------------------------------------------------------------------------

byte                                                   1

short                                                  2

int 																  4

long																8

float                                                   4

double													  	8

boolean                                             1

char                                                   2

**计算机在任何情况下都只能识别二进制，例如只认识010101010101……**

<font color = red>**注意：成员变量没有手动赋值，系统会默认赋值，但局部变量不会，局部变量必须声明并赋值。**</font>八种数据结构默认值一切是等同于0。

### 4.1、浮点型

float  单精度【4个字节】

double  双精度【8个字节，精度相对较高】

> double在财务软件方面相对来说精度太低，不适合做财务软件。财务软件涉及到钱的问题，要求精度较高，使用SUN公司在基础SE类库中提供一种更高的类型，这是一种引用数据类型，不属于基本数据类型。它是：java.math.BigDecimal

SE类库字节码：jdk\jre\lib\rt.jar

SE类库源码：jdk\src.zip

## 5、基础 - 控制语句 if

格式：

```java
for(初始化表达式；布尔表达式；更新表达式){
    // 重复执行的代码片段
}
```

初始化表达式、布尔表达式、更新表达式都不是必须的，但是分号是必须的。

```java
// 死循环
for(;;){
    // 重复执行的代码片段
}
```

## 6、基础 - 方法

### 6.1、方法

**规范建议，一个java源文件中，只定义一个class**

**方法结构：**

```java
/*
[修饰符列表] 返回值类型 方法名(形式参数){
    方法体
}
*/
```

**方法的调用方式：**

- 当方法的修饰符列表中有 `static` 关键字时，`类名.方法名(实参)` 【完整的调用方式】
- 在同一class类下，类名可以省略不写 `方法名(实参)`【省略方式】

**返回值类型：**可以为Java任意类型；但是当方法没有返回值时，必须用 `void`关键字；若返回类型不是void，则这个方法必须返回一个具体的数值，如果没有返回任何数据，会编译报错。

**返回方式：**`return 值`

**形式参数：**

- 简称形参，形参是局部变量；
- 多个形参之间用`,`隔开；
- 形参中起决定性作用的是形参的类型，形参的名字就是局部变量的名字；

**实际参数**：

- 方法在调用的时候，实际给这个方法传递的真实数据被称为实际参数，简称实参；
- 实参和形参必须满足：
  - 类型对应相同
  - 数量相同

注意：void返回方法，return可以加也可以不加，**return终止的是方法，而break终止的是循环语句。**

### 6.2、方法内存分析

方法只定义不调用，是不会执行的，并且方法在执行过程中，JVM不会给方法分配“运行所属”的内存空间。只有在调用方法时，才会动态的给方法分配内存空间。在JVM内存划分上有三块**主要**的内存空间：**方法区内存、堆内存、栈内存**。

常见的数据结构：数组、队列、栈、链表、二叉树、哈希表/散列表……

关于栈数据结构：

- 栈：stack，是一种数据结构；
- 数据结构反应的是数据的存储形态；

- 栈帧永远指向栈顶元素

- 栈顶元素处于活跃状态，其他元素静止

- 术语：

  - 压栈/入栈/push
  - 弹栈/出栈/pop

- 栈数据结构存储的特点：

  - 先进后出
  - 后进先出

  



