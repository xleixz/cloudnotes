# Git 常用命令和问题详解

## 1 常用命令

- 克隆远程仓库

  ```shell
  git clone 
  ```

- 比较文件的不同，即暂存区和工作区的差异

  ```shell
  git diff
  ```

- 查看推送提交记录

  ```shell
  git log
  ```

- 回滚到上一个/某个版本

  ```shell
  # 回滚到上一个版本
  git reset --hard
  
  # 回滚到某个版本
  git reset --hard commit_id(commit的id)
  ```

- 添加文件到暂存区

  ```shell
  git add .
  ```

- 查看仓库当前的状态，显示有变更的文件

  ```shell
  git status
  ```

- 提交暂存区到本地仓库

  ```shell
  git commit -m "" 
  ```

  `-m`是`git commit`中必需项HEAD部分的缩写

- 上传远程代码并合并

  ```shell
  git push
  ```

- 从远程获取代码库

  ```shell
  git fetch
  ```

- 下载远程代码并合并（<font color="red">存在pull成功但是本地代码并非最新的问题</font>，<font color="green">[点击查看解决方法](# 2.1 git pull 拉取不是最新问题  "点击查看2.1 git pull 拉取不是最新问题")</font>）

  ```shell
  git pull
  ```
  
- 查看本地分支

  ```shell
  git branch
  ```

- 查看所有分支（包括本地分支和远程分支）

  ```shell
  git branch --all
  # 或
  git branch -a
  ```

- 创建本地分支

  ```shell
  git branch <分支名>
  ```

- 切换本地分支

  ```shell
  git checkout <分支名>
  ```

- 删除本地分支

  ```shell
  git branch --delete <分支名> 
  # 或 
  git branch -d <分支名>
  ```

- 删除远程分支

  ```shell
  git push origin --delete <远程分支名>
  ```

- 克隆指定远程分支到本地

  ```shell
  git clone --branch <本地分支名> <远程仓库地址>
  ```

- 将本地 develop 分支修改为 dev 分支

  ```shell
  git branch --move develop dev    
  # 或
  git branch -m develop dev
  ```

- 将本地 dev 分支推送到远程

  ```shell
  git push origin dev
  ```

- 合并本地 bugfix 分支到 develop 分支

  ```shell
  git merge bugfix
  ```

- 添加分支描述信息

  ```shell
  git config branch.<分支名>.description "分支描述信息"
  ```

- 查看分支描述信息

  ```shell
  git config branch.<分支名>.description
  ```
  
- 修改远程地址（pull 和 push）

  ```shell
  git remote set-url origin [url]
  git remote set-url --push origin [url]
  ```

## 2 问题详解

### 2.1 git pull 拉取不是最新问题

在开发中使用Git的过程中，有时候会有一种需求，要从服务器拉取最新的状态，而本地进行了无关紧要的修改，这时候如果使用`git pull`命令，会提示本地有未缓存的修改。或者在开发中，使用`git pull`命令提示已经为最新，但是代码却不是最新的一次提交。这时候就需要强制覆盖本地的改变。

> <font color="red">**注意：**</font>本地未保存的代码将会丢失，任何未被推送的本地提交都将丢失。

1. 先从远程获取最新代码

   ```shell
   git fetch --all
   ```

2. 从分支上回滚最新的提交（hard后面可省略）

   ```shell
   git reset --hard  # (origin/master)/(origin/branch_name)
   ```

此时本地为远程仓库最新的版本。