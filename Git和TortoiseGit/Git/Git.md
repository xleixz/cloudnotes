# Git

![img](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/logo@2x.png)

## 1 版本控制

### 1.1 概念

> 什么是版本控制，版本迭代？

版本控制是一种在开发过程中用于管理对文件、目录、工程等内容的修改历史，方便查看更改历史记录，备份以便恢复以前版本的软件工程技术。

- 多人协同跨域开发
- 记载一个或多个文件的历史记录
- 组织和保护源码
- 减轻开发人员的负担，降低人为错误

这时就需要版本控制工具来解决这些问题。

> 常见的版本控制工具

主流的版本控制工具有：

- Git
- SVN
- CVS
- VSS
- TFS
- Visual Studio Online

### 1.2 版本控制分类

1、**本地版本控制**

记录文件每次的更新，可以对每个版本做一个对照，或是记录补丁文件，适合个人使用。

![image-20220820231814959](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220820231814959.png)

2、**集中版本控制**

所有的版本数据都保存在服务器上，协同开发者从服务器上同步更新或上传自己的修改。

![image-20220820231921947](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220820231921947.png)

但是存在缺点，如果不联网，用户就看不到历史版本，而且所有的数据都保存在单一的服务器上，有很大的风险服务器会宕机、损坏，这样就会丢失数据，<font color="red">**所以需要定期备份**</font>，代表产品：SVN，CVS，VSS。

3、**分布式版本控制**

所有的版本信息仓库全部同步到本地的每个用户，这样可以在本地查看所有版本的历史，即使离线状态，也可以本地提交，只需在联网时**push**到相应的服务器或者其他用户里即可。由于每个用户保存的都是所有的版本数据，只要有一个用户的设备没有问题，就可以恢复所有的数据，但这增加了本地存储空间的占用，<font color="red">**且每个人都拥有全部的代码，存在安全隐患。**</font><font color="green">**优点：不会因为服务器的损坏或者网络问题，造成不能工作的情况。**</font>代表产品：Git。

<img src="https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220820232639254.png" alt="image-20220820232639254" style="zoom:67%;" />

### 1.3 Git 和 SVN 的区别

- **SVN**是**集中式版本控制系统**，版本库是集中放在中央服务器的，而工作的时候，用的都是个人电脑，所以首先要从中央服务器得到最新版本，然后工作，完成工作后，需要把自己完成的代码推送到中央服务器。<font color="red">**集中式版本控制系统是必须联网才能工作的，对网络带宽要求较高。**</font>
- **Git**是**分布式版本控制系统**，没有中央服务器，每个人的电脑就是一个完整的版本库，工作的时候不需要联网，因为版本都在个人电脑上。协同的方法是：自己在电脑上改了文件A，其他人也在电脑上改了文件A，这时两个人之前只需把各自的修改推送给对方，就可以互相看到对方的修改记录 。<font color="green">**Git可以直接看到更新了哪些代码和文件！**</font>

<font color="blue">**Git是目前世界上最先进的分布式控制版本系统。**</font>

## 2 安装、卸载、配置 Git

> [Git官网 https://git-scm.com/](https://git-scm.com/ "点击查看Git官网")

### 2.1 安装

进行无脑安装即可。安装时已自动配置好，环境变量的目的就是为了可以全局使用，但是右键菜单栏中都会应用程序，所以无需再配置环境变量。

### 2.2 卸载

1. 清理电脑里跟Git相关的所有环境变量；
2. 卸载Git。

### 2.3 启动

在Windows应用中会有Git应用，同时右键的菜单栏中也可以看到对应的应用程序。

![image-20220823233308755](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220823233308755.png)

**Git Bash**：Unix与Linux风格的命令行，使用最多，推荐；

<img src="https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220823233541049.png" alt="image-20220823233541049" style="zoom: 67%;" />

**Git CMD**：Windows风格的命令行；

<img src="https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220823233605771.png" alt="image-20220823233605771" style="zoom: 50%;" />

**Git GUI**：图形界面的Git，不建议初学者使用，尽量熟悉常用命令。

![image-20220823233629197](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220823233629197.png)

### 2.4 配置 Git

Git相关的配置文件：

- 目录位置：Git\\etc\gitconfig      **Git安装目录下的gitconfig**                 `system系统级别`

  <img src="https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220824001611461.png" alt="image-20220824001611461" style="zoom: 67%;" />

- 目录位置：Users\Administrator\\.gitconfig      **只适用于当前登录用户的配置**     `global全局`      

  ![image-20220824001752562](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220824001752562.png)

> <font color="red">**设置用户名与邮箱（用户标识，必要）**</font>

当安装Git后，首先要做的事情就是设置用户名和e-mail地址。这是最重要的配置，因为每次Git提交都会使用该信息。这会被永远嵌入到每次的提交中。

```shell
git config --global user.name "xleixz"  #名称
git config --global user.email "xxxxxxx@qq.com"  #邮箱
```

> **查看配置**

```shell
# 查看配置文件
git config -l

# 查看当前用户(本地)配置
git config --global --list

# 查看系统配置
git config --system --list
```

![image-20220823235256052](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220823235256052.png)

## 3 Git 工作原理（核心）

### 3.1 工作区域

Git本地有三个工作区域，分别是：**工作目录**、**暂存区**、**资源库**。如果加上远程的Git仓库，就可以分为四个工作区域。文件在这四个区域之间转换关系如下：

<img src="https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220906224400280.png" alt="image-20220906224400280" style="zoom:40%;" />

- Workspace：工作区，就是平时存放代码的地方
- Index/Stage：暂存区，用于临时存放改动，事实上只是一个文件，保存即将提交到文件列表信息
- Repository：仓库区（或本地仓库），就是安全存放数据的位置，这里有提交打所有版本的数据。其中HEAD指向最新放入方库的版本
- Remote：远程仓库，托管代码的服务器，可以简单的认为是项目组中的一台电脑用于远程数据交换

### 3.2 工作流程

1. 在工作目录中添加、修改文件；
2. 将需要进行版本管理的文件放入暂存区域；
3. 将暂存区域的文件提交到git仓库。

因此，git管理文件有三种状态：已修改（modified），已暂存（staged），已提交（committed）

## 4 Git 操作

### 4.1 Git 项目搭建及克隆

1. 本地仓库搭建

   ```bash
   git init
   ```

   ![image-20220906230313806](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220906230313806.png)

2. 远程克隆仓库

   ```bash
   git clone [url]
   ```

   ![image-20220906230504507](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220906230504507.png)

### 4.2 Git 基本操作命令

> **查看文件状态**

```bash
# 查看指定文件状态
git status [filename]
```

```bash
# 查看所有文件状态
git status
```

> **添加所有命令到暂存区**

```bash
# 添加所有命令到暂存区
git add .
```

> **提交暂存中的内容到本地仓库**

```bash
# 提交暂存区内文件到本地仓库,-m 表示提交信息
git commit -m ""
```

> **推送至远程仓库**

```bash
# 推送
git push
```

> **拉取更新**

```bash
# 拉取更新
git pull
```

### 4.3 忽略文件

有时候不需要把某些文件提交到版本控制中，比如临时文件，设计文件等。

在主目录下建立“**.gitignore**”文件，此文件有如下规则：

1. 忽略文件中的空行或以井号（#）开始的行将会被忽略；
2. 可以使用Linux通配符。例如：星号（*）代表任意多个字符，问号（？）代表一个字符，方括号（[abc]）代表可选字符范围，大括号（{String1,String2,……}）代表可选的字符串等；
3. 如果名称的最前面是一个感叹号（！），表示例外规则，将不被忽略；
4. 如果名称的最前面是一个路径分隔符（/），表示要忽略的文件在此目录下，而子目录中的文件不忽略；
5. 如果名称的最后面是一个路径分隔符（/），表示要忽略的是此目录下该名称的子目录，而非文件（默认文件或目录都忽略）。

```bash
# 为注释
*.txt                 #忽略所有 .txt结尾的文件，上传时就不会被选中！
!lib.txt              #但lib.txt除外
/temp                 #仅忽略项目根目录下的TODO文件，不包括其他目录temp
build/                #忽略build/目录下的所有文件
doc/*.txt             #会忽略 doc/notes.txt 但不包括 doc/server/arch.txt
```

### 4.4 配置SSH公钥

1. 生成SSH公钥

   ```bash
   # 进入 C:\Users\Administrator\.ssh 目录
   # 生成公钥
   ssh-keygen
   
   # 可以使用官网推荐的加密算法
   ssh-keygen -t rsa
   ```

   <img src="https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220906234205545.png" alt="image-20220906234205545" style="zoom:60%;" />

   ![image-20220906234234630](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220906234234630.png)

2. 将公钥信息public key 添加到账户中，复制.ssh目录中的pub（公钥）文件

   ![image-20220906234541143](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220906234541143.png)

许可证：开源是否可以随意转载，开源是否可以商用，开源是否可以转载……

许可证推荐GPL-3.0即可。

## 5 IDEA集成Git

1. 绑定git
   - 将git文件下的所有内容剪切拷贝到项目目录即可！
2. 绑定成功后，IDEA中文件会出现颜色变化，并且出现了Git功能区
   - ![image-20220907000119168](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220907000119168.png)
   - ![image-20220907000130683](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20220907000130683.png)

步骤和命令行一样：先添加到暂存区，再提交到本地仓库，最后推送至远程仓库

<font color="red">建议：尽量使用命令行操作，比较快捷方便，图形化较为麻烦！</font>

## 6 Git 分支

> **列出所有本地分支**

```bash
# 列出所有本地分支
git branch
```

> **列出所有远程分支**

```bash
# 列出所有远程分支
git branch -r
```

> **新建一个分支，但是目前还在当前分支**

```bash
# 新建一个分支，但是目前还在当前分支
git branch [branch-name]
```

> **新建一个分支，并且切换到该分支**

```bash
# 新建一个分支，并且切换到该分支
git checkout -b [branch]
```

> **合并指定分支到当前分支**

```bash
# 合并指定分支到当前分支
git merge [branch]
```

> **删除本地分支**

```bash
# 删除本地分支
git branch -d [branch-name]
```

> **删除远程分支**

```bash
# 删除远程分支
git push origin --delete [branch-name]
```

## 7 Git命令大全

[https://gitee.com/all-about-git](https://gitee.com/all-about-git "点击查看Git命令大全")











