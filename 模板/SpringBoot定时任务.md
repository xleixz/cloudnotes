# SpringBoot 定时任务

## 一、数据库准备

创建一个定时任务配置表

```mysql
CREATE TABLE `scheduled` (
	`cron_id` VARCHAR ( 50 ) NOT NULL COMMENT '定时任务ID',
	`cron_name` VARCHAR ( 50 ) NOT NULL COMMENT '定时任务名称',
	`cron` VARCHAR ( 50 ) NOT NULL COMMENT '定时任务表达式',
  `cron_description` varchar(255) DEFAULT NULL COMMENT '定时任务描述',
  `is_status` tinyint NOT NULL COMMENT '0-禁用 1-启用',
PRIMARY KEY ( `cron_id` ) USING BTREE 
) COMMENT = '定时任务配置';
```

## 二、实体类

```java
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 定时任务实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true) // 忽略未知属性
@TableName(value = "scheduled") // 表名
public class ScheduledDO {

    @TableId(type = IdType.ASSIGN_UUID)
    @TableField(value = "cron_id")
    private String cronId; // 定时任务ID

    @TableField(value = "cron_name")
    private String cronName; // 定时任务名称

    @TableField(value = "cron")
    private String cron; // 定时任务表达式
  
    @TableField(value = "cron_description")
    private String cronDescription; // 定时任务描述

    @TableField(value = "is_status")
    private int isStatus; // 定时任务状态 0-禁用 1-启用
}
```

## 三、业务

开启注解`@EnableScheduling`

```java
/**
 * 定时任务
 */
@Component
@Slf4j
@EnableScheduling
public class ScheduledTask implements SchedulingConfigurer {

    @Autowired
    private ScheduledMapper scheduledMapper; // 调用定时任务服务

    @Autowired
    private ScheduledService scheduledService; // 调用定时任务服务

    @Autowired
    private WebSocketMessageService webSocketMessageService; // 调用WebSocket服务


    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        // 定时获取天气信息任务
        scheduledTaskRegistrar.addTriggerTask(() -> {
                    try {
                        webSocketMessageService.checkAndPushLivesWeatherUpdate(); // 获取实况天气信息
                        webSocketMessageService.checkAndPushForecastWeatherUpdate(); // 获取实况天气信息
                    } catch (JsonProcessingException e) {
                        throw new RuntimeException(e);
                    }
                },
                // 定时器触发器
                triggerContext -> {
                    // 查询任务时间
                    ScheduledDO scheduled = scheduledMapper.selectOne(new QueryWrapper<ScheduledDO>()
                            .lambda()
                            .eq(ScheduledDO::getCronName, "天气信息")
                            .eq(ScheduledDO::getIsStatus, 1));
                    if (scheduled != null) {
                        String cron = scheduled.getCron();
                        if (cron.isEmpty()) {
                            log.warn("未找到定时任务 -> {}", scheduled);
                        }
                        // 执行Controller方法
                        return new CronTrigger(cron).nextExecutionTime(triggerContext);
                    } else {
                        log.warn("未找到定时任务 -> {}", "天气信息");
                        return null;
                    }
                });

        // 获取新闻定时任务
        scheduledTaskRegistrar.addTriggerTask(() -> {
                    try {
                        webSocketMessageService.checkAndPushNewsUpdate(); // 获取新闻
                    } catch (JsonProcessingException e) {
                        throw new RuntimeException(e);
                    }
                },
                // 定时器触发器
                triggerContext -> {
                    // 查询任务时间
                    ScheduledDO scheduled = scheduledMapper.selectOne(new QueryWrapper<ScheduledDO>()
                            .lambda()
                            .eq(ScheduledDO::getCronName, "新闻信息")
                            .eq(ScheduledDO::getIsStatus, 1));
                    if (scheduled != null) {
                        String cron = scheduled.getCron();
                        if (cron.isEmpty()) {
                            log.warn("未找到定时任务 -> {}", scheduled);
                        }
                        // 执行Controller方法
                        return new CronTrigger(cron).nextExecutionTime(triggerContext);
                    } else {
                        log.warn("未找到定时任务 -> {}", "新闻信息");
                        return null;
                    }
                });
    }
}
```

主启动类也要开启`@EnableScheduling // 开启定时任务`

```java
@SpringBootApplication
@EnableScheduling // 开启定时任务
public class SihongVineyardApplication {

    public static void main(String[] args) {
        SpringApplication.run(SihongVineyardApplication.class, args);

        System.out.println("\033[32m" + " _________________\n" +
                "< Hi, 我是牛哥…… 启动成功！>\n" +
                " -----------------\n" +
                "        \\   ^__^\n" +
                "         \\  (oo)\\_______\n" +
                "            (__)\\       )\\/\\\n" +
                "                ||----w |\n" +
                "                ||     ||" + "\033[0m");
    }

}
```

## 四、注解的方式开启定时任务

只需要在方法中加入注解`@Scheduled(cron = "#表达式")`即可。不建议这种方式，因为需要改动配置时需要迭代

```java
   
   //秒   分   时     日   月   周几
   //0 * * * * MON-FRI
   //注意cron表达式的用法；
   @Scheduled(cron = "0 * * * * 0-7")
```

## 五、常用的表达式

```java
（1）0/2 * * * * ?   表示每2秒 执行任务
（1）0 0/2 * * * ?   表示每2分钟 执行任务
（1）0 0 2 1 * ?   表示在每月的1日的凌晨2点调整任务
（2）0 15 10 ? * MON-FRI   表示周一到周五每天上午10:15执行作业
（3）0 15 10 ? 6L 2002-2006   表示2002-2006年的每个月的最后一个星期五上午10:15执行作
（4）0 0 10,14,16 * * ?   每天上午10点，下午2点，4点
（5）0 0/30 9-17 * * ?   朝九晚五工作时间内每半小时
（6）0 0 12 ? * WED   表示每个星期三中午12点
（7）0 0 12 * * ?   每天中午12点触发
（8）0 15 10 ? * *   每天上午10:15触发
（9）0 15 10 * * ?     每天上午10:15触发
（10）0 15 10 * * ?   每天上午10:15触发
（11）0 15 10 * * ? 2005   2005年的每天上午10:15触发
（12）0 * 14 * * ?     在每天下午2点到下午2:59期间的每1分钟触发
（13）0 0/5 14 * * ?   在每天下午2点到下午2:55期间的每5分钟触发
（14）0 0/5 14,18 * * ?     在每天下午2点到2:55期间和下午6点到6:55期间的每5分钟触发
（15）0 0-5 14 * * ?   在每天下午2点到下午2:05期间的每1分钟触发
（16）0 10,44 14 ? 3 WED   每年三月的星期三的下午2:10和2:44触发
（17）0 15 10 ? * MON-FRI   周一至周五的上午10:15触发
（18）0 15 10 15 * ?   每月15日上午10:15触发
（19）0 15 10 L * ?   每月最后一日的上午10:15触发
（20）0 15 10 ? * 6L   每月的最后一个星期五上午10:15触发
（21）0 15 10 ? * 6L 2002-2005   2002年至2005年的每月的最后一个星期五上午10:15触发
（22）0 15 10 ? * 6#3   每月的第三个星期五上午10:15触发
```

​	