# Slf4j 日志

1. 添加依赖

   ```xml
   <dependency>
               <groupId>org.slf4j</groupId>
               <artifactId>slf4j-api</artifactId>
               <version>1.7.36</version>
           </dependency>
           
           <!-- 使用 logback 作为 slf4j 的实现 -->
           <dependency>
               <groupId>ch.qos.logback</groupId>
               <artifactId>logback-classic</artifactId>
               <version>1.2.11</version>
           </dependency>
   ```

   

2. 在 `application.yml` 配置文件中添加配置

   ```yacas
   # 日志配置
   logging:
     config: classpath:logback-spring.xml
     variables:
       app:
         package: com.hxlei    # 变量名格式
         name: account_book    # 添加应用名称变量，用于日志文件名
   ```

3. 新增 `logback-spring.xml` 配置文件

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <configuration>
       <!-- 定义通用变量 -->
       <property name="LOG_PATH" value="./log"/>
       <property name="APP_NAME" value="${app.name:-${artifactId:-application}}"/>
   
       <!-- 定义通用的日志格式 -->
       <property name="LOG_PATTERN" value="[%p][%d{yyyy-MM-dd HH:mm:ss.SSS}][%c]%m%n"/>
   
       <!-- 开发环境配置 -->
       <springProfile name="dev">
           <appender name="CONSOLE" class="ch.qos.logback.core.ConsoleAppender">
               <encoder>
                   <pattern>${LOG_PATTERN}</pattern>
               </encoder>
           </appender>
   
           <appender name="FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
               <file>${LOG_PATH}/dev/${APP_NAME}.log</file>
               <rollingPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy">
                   <fileNamePattern>${LOG_PATH}/dev/${APP_NAME}-%d{yyyy-MM-dd}.%i.log</fileNamePattern>
                   <maxFileSize>10MB</maxFileSize>
                   <maxHistory>7</maxHistory>
               </rollingPolicy>
               <encoder>
                   <pattern>${LOG_PATTERN}</pattern>
               </encoder>
           </appender>
   
           <!-- 应用包路径配置 -->
           <logger name="com" level="DEBUG"/>
           <logger name="${app.package:-com}" level="DEBUG"/>
   
           <!-- 框架日志配置 -->
           <logger name="org.mybatis" level="DEBUG"/>
           <logger name="java.sql" level="DEBUG"/>
           <logger name="org.springframework" level="INFO"/>
   
           <root level="DEBUG">
               <appender-ref ref="CONSOLE"/>
               <appender-ref ref="FILE"/>
           </root>
       </springProfile>
   
       <!-- 测试环境配置 -->
       <springProfile name="test">
           <appender name="CONSOLE" class="ch.qos.logback.core.ConsoleAppender">
               <encoder>
                   <pattern>${LOG_PATTERN}</pattern>
               </encoder>
           </appender>
   
           <appender name="FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
               <file>${LOG_PATH}/test/${APP_NAME}.log</file>
               <rollingPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy">
                   <fileNamePattern>${LOG_PATH}/test/${APP_NAME}-%d{yyyy-MM-dd}.%i.log</fileNamePattern>
                   <maxFileSize>20MB</maxFileSize>
                   <maxHistory>15</maxHistory>
               </rollingPolicy>
               <encoder>
                   <pattern>${LOG_PATTERN}</pattern>
               </encoder>
           </appender>
   
           <!-- 应用包路径配置 -->
           <logger name="com" level="INFO"/>
           <logger name="${app.package:-com}" level="INFO"/>
   
           <!-- 框架日志配置 -->
           <logger name="org.mybatis" level="INFO"/>
           <logger name="java.sql" level="INFO"/>
           <logger name="org.springframework" level="INFO"/>
   
           <root level="INFO">
               <appender-ref ref="CONSOLE"/>
               <appender-ref ref="FILE"/>
           </root>
       </springProfile>
   
       <!-- 生产环境配置 -->
       <springProfile name="prod">
           <appender name="FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
               <file>${LOG_PATH}/prod/${APP_NAME}.log</file>
               <rollingPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy">
                   <fileNamePattern>${LOG_PATH}/prod/${APP_NAME}-%d{yyyy-MM-dd}.%i.log</fileNamePattern>
                   <maxFileSize>50MB</maxFileSize>
                   <maxHistory>30</maxHistory>
                   <totalSizeCap>10GB</totalSizeCap>
               </rollingPolicy>
               <encoder>
                   <pattern>${LOG_PATTERN}</pattern>
               </encoder>
           </appender>
   
           <appender name="ERROR_FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
               <file>${LOG_PATH}/prod/error.log</file>
               <filter class="ch.qos.logback.classic.filter.ThresholdFilter">
                   <level>ERROR</level>
               </filter>
               <rollingPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy">
                   <fileNamePattern>${LOG_PATH}/prod/error-%d{yyyy-MM-dd}.%i.log</fileNamePattern>
                   <maxFileSize>20MB</maxFileSize>
                   <maxHistory>30</maxHistory>
                   <totalSizeCap>2GB</totalSizeCap>
               </rollingPolicy>
               <encoder>
                   <pattern>${LOG_PATTERN}</pattern>
               </encoder>
           </appender>
   
           <!-- 应用包路径配置 -->
           <logger name="com" level="INFO"/>
           <logger name="${app.package:-com}" level="INFO"/>
   
           <!-- 框架日志配置 -->
           <logger name="org.mybatis" level="WARN"/>
           <logger name="java.sql" level="WARN"/>
           <logger name="org.springframework" level="WARN"/>
   
           <root level="INFO">
               <appender-ref ref="FILE"/>
               <appender-ref ref="ERROR_FILE"/>
           </root>
       </springProfile>
   </configuration>
   ```

   