# SpringBoot的yml（properties）多环境配置

[TOC]

---

> 对于SpringBoot工程，在不同环境（例如`dev`，`test`，`prod`等）可能有不同的配置信息（配置在`application.yml`或`application.properties`中），例如swagger.enable这个变量，在dev和test环境值为true，在prod环境的值为false；或者dev开发环境的数据库url与prod生产环境的数据库url不一致的情况。

在springboot中，有两种方式可以实现多环境配置文件：

1. 一种是直接在一个配置文件中配置多个环境的配置信息（即多文档块，通过`---`分割），这种`仅支持application.yml文件`；
2. 一种是一个`主配置文件`（application.yml或application.properties）和`多个环境配置文件`（application-dev.yml，application-test.yml，application-prod.yml等）。<font color = 'red'>推荐！</font>

## 1、配置文件

1. 主配置文件：`application.yml`

   ```yaml
   spring:
     profiles:
       active: dev  # 激活指定使用哪个环境配置文件
   ```

2. 不同环境下的配置文件：`application-dev.yml`、`application-prod.yml`、`application-test.yml`

   ```yaml
   spring:
     config:
       activate:
         on-profile: dev # dev环境
     datasource:
       username: 
       password: 
       url: jdbc:mysql://xxxxxxxxxxxxxxxxxxxxx
       driver-class-name: com.mysql.cj.jdbc.Driver
       type: com.alibaba.druid.pool.DruidDataSource
   
   server:
     port: 521
     ssl:
       key-store-password: 
       key-store-type: JKS
       key-store: 
   ```

   ```yaml
   spring:
     config:
       activate:
         on-profile: prod # prod环境
     datasource:
       username: 
       password: 
       url: jdbc:mysql://xxxxxxxxxxxxxxxxxxxxx
       driver-class-name: com.mysql.cj.jdbc.Driver
       type: com.alibaba.druid.pool.DruidDataSource
   
   server:
     port: 520
     ssl:
       key-store-password: 
       key-store-type: JKS
       key-store: 
   ```

## 2、启动不同的配置

1. IDEA或本地编译器通过`Run`、`Debugger`启动时，仅需修改主配置文件`application.yml`中`spring.profiles.active`的指定配置即可启动相应的环境配置。

   ```bash
   spring:
     profiles:
       active: dev  # 激活指定使用哪个环境配置文件
   ```

2. 在部署、命令行启动时，仅需输入（dev：指定的配置）

   ```bash
   java -jar xxxxx.jar --spring.profiles.active=dev
   ```

   或永久运行并生成日志

   ```bash
   nohup java -jar xxxx.jar --spring.profiles.active=dev >xxxx.out &
   ```

   