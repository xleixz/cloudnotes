# jasypt 加密

[TOC]

---

## 1、依赖环境

```xml
        <dependency>
            <groupId>com.github.ulisesbocchio</groupId>
            <artifactId>jasypt-spring-boot-starter</artifactId>
            <version>2.1.1</version>
        </dependency>
```

## 2、设置加密字符串

在`application.yml` 配置文件中添加加密配置字符串，<font color='red'>注意删除明文，否则配置文件暴露后，jasypt很容易被破解！</font>

```yaml
# jasypt 加密配置
jasypt:
  encryptor:
    password: ^&%^abfiguHUG7878325d^YU
    algorithm: PBEWithMD5AndDES
    iv-generator-classname: org.jasypt.iv.NoIvGenerator
```

1. `jasypt`: 这是配置 Jasypt 的部分。
2. `encryptor`: 在这里配置了加密器的属性。
   - `password`: 这是用于解密属性值的密码。在你的配置中，密码是 `As12mda-02mdAadi123df`。这个密码被用于解密被加密的属性值。
   - `algorithm`: 这指定了用于加密和解密的算法。在你的配置中，算法是 `PBEWithMD5AndDES`。这个算法是基于密码的加密算法，使用 MD5 散列和 DES 加密。
   - `iv-generator-classname`: 这指定了用于生成初始化向量（Initialization Vector，IV）的类名。在你的配置中，使用的是 `org.jasypt.iv.NoIvGenerator`，这意味着没有使用 IV。

## 3、生成密钥

用工具类生成密钥，<font color='red'>注意生成密钥后，删除账户信息！</font>

```java
@Component
public class JasyptPasswordUtil {

    // 读取配置文件中的加密字符串
    @Value("${jasypt.encryptor.password}")
    private String jasypt;

    @PostConstruct
    public void encryptor() {
        BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
        // jasypt自身所需要设置的密钥
        textEncryptor.setPassword(jasypt);
        // 要设置的账号信息
        String dataSourceUsername = textEncryptor.encrypt("root");
        String dataSourcePassword = textEncryptor.encrypt("123456");
        String dataSourceRedis = textEncryptor.encrypt("123456");

        System.out.println("dataSourceUsername: " + dataSourceUsername);
        System.out.println("dataSourcePassword: " + dataSourcePassword);
        System.out.println("dataSourceRedis: " + dataSourceRedis);

    }
}
```

## 4、修改配置

修改所对应的配置密码，加上`ENC(#加密后的密钥)`

```xml
spring:
  config:
    activate:
      on-profile: dev
  datasource:
    username: ENC(xxxxxxxxxxxxxxxxxxxxxxxxxxxx)
    password: ENC(xxxxxxxxxxxxxxxxxxxxxxxxxxxx)
    url: jdbc:mysql://127.0.0.1:3306/account_book_test?useUnicode=true&characterEncoding=utf-8&serverTimezone=UTC
    driver-class-name: com.mysql.cj.jdbc.Driver
    type: com.alibaba.druid.pool.DruidDataSource
```

## 5、启动与部署

### 5.1、本地IDEA启动

- **系统属性：**

  ```shell
  java -jar xxxx.jar -Djasypt.encryptor.password=#你的密钥字符串
  ```

  环境变量，虚拟机选项中添加 `-Djasypt.encryptor.password=#你的密钥字符串`

![image-20240307103004119](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20240307103004119.png)

## 5.2、服务器运行

```shell
java -jar xxxx.jar --jasypt.encryptor.password=#你的密钥字符串
```

