# Java打印彩色输出

![image-20221110225805841](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20221110225805841.png)

```java
System.out.println("默认");
            System.out.println("\033[30m" + "黑色!" + "\033[0m");
			System.out.println("\033[30m这也是黑色!\033[0m");
            System.out.println("\033[31m" + "红色!" + "\033[0m");
            System.out.println("\033[32m" + "绿色!" + "\033[0m");
            System.out.println("\033[33m" + "黄色!" + "\033[0m");
            System.out.println("\033[34m" + "蓝色!" + "\033[0m");
            System.out.println("\033[35m" + "紫色!" + "\033[0m");
            System.out.println("\033[36m" + "青色!" + "\033[0m");
            System.out.println("\033[37m" + "灰色!" + "\033[0m");
            System.out.println("\033[30;4m" + "黑色下划线!" + "\033[0m");
            System.out.println("\033[31;4m" + "红色下划线!" + "\033[0m");
            System.out.println("\033[32;4m" + "绿色下划线!" + "\033[0m");
            System.out.println("\033[33;4m" + "黄色下划线" + "\033[0m");
            System.out.println("\033[34;4m" + "蓝色下划线" + "\033[0m");
            System.out.println("\033[35;4m" + "紫色下划线" + "\033[0m");
            System.out.println("\033[36;4m" + "青色下划线" + "\033[0m");
            System.out.println("\033[37;4m" + "灰色下划线" + "\033[0m");
            System.out.println("\033[40;31;4m" + "反色黑色下划线" + "\033[0m");
            System.out.println("\033[41;32;4m" + "反色红色下划线" + "\033[0m");
            System.out.println("\033[42;33;4m" + "反色绿色下划线" + "\033[0m");
            System.out.println("\033[43;34;4m" + "反色黄色下划线" + "\033[0m");
            System.out.println("\033[44;35;4m" + "反色蓝色下划线" + "\033[0m");
            System.out.println("\033[45;36;4m" + "反色紫色下划线" + "\033[0m");
            System.out.println("\033[46;37;4m" + "反色青色下划线" + "\033[0m");
            System.out.println("\033[47;4m" + "反色灰色下划线" + "\033[0m");
```

![image-20221110231841130](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20221110231841130.png)

```java
        System.out.println("\033[33m[MSG][" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "]邮件发送成功\033[0m");
```

> 格式

  `"\033[*;*;*m" //` 比如  `"\033[1;2;3m"`

 ` 前缀"\033["`，`后缀"m"`

  <颜色、背景颜色、样式都是用数字表示

  所以只需要把对应的字码用";"隔开就好了

> 范围

转义符之后的字符都会变成转义符所表示的样式

> 样式

  0 空样式

  1 粗体

  4 下划线

  7 反色

  颜色1：

  30 白色

  31 红色

  32 绿色

  33 黄色

  34 蓝色

  35 紫色

  36 浅蓝

  37 灰色

  背景颜色：

  40-47 和颜色顺序相同

  颜色2：

  90-97 比颜色1更鲜艳一些