# 工具类Utils

## 一、OkHttpUtils 【请求API】

```java
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * @description: OkHttpUtils工具类
 */
@Component
@Slf4j
public class OkHttpUtils {
    private static final OkHttpClient client = new OkHttpClient.Builder() // OkHttpClient 实例，设置了连接超时、调用超时、ping 间隔、读取超时和写入超时
            .connectTimeout(30, TimeUnit.SECONDS)
            .callTimeout(120, TimeUnit.SECONDS)
            .pingInterval(5, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .build();
    private static final ObjectMapper objectMapper = new ObjectMapper();


    // 请求类型枚举，包含 GET 和 POST
    public enum RequestType {
        GET,
        POST
    }


    // 参数类型枚举，包含 JSON 和 FORM
    public enum ParamType {
        JSON,
        FORM
    }


    /**
     * 发送 HTTP 请求的方法
     *
     * @param endpoint       请求的目标地址
     * @param requestType    请求类型，使用 RequestType 枚举表示（GET 或 POST）
     * @param paramType      参数类型，使用 ParamType 枚举表示（JSON 或 FORM，仅适用于 POST）
     * @param requestBodyObj 请求体对象，将根据 paramType 转换为相应的请求体
     * @param username       用户名，用于认证，可为空
     * @param password       密码，用于认证，可为空
     * @return 请求的响应结果字符串，如果请求失败则返回 null
     */
    public static String sendRequest(String endpoint, RequestType requestType, ParamType paramType, Map<String, Object> requestBodyObj, String username, String password) {
        try {
            Request request = buildRequest(endpoint, requestType, paramType, requestBodyObj, username, password);
            try (Response response = client.newCall(request).execute()) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        return response.body().string();
                    } else {
                        return null;
                    }
                } else {
                    log.error("请求失败，状态码: {}", response.code());
                    return null;
                }
            }
        } catch (IOException e) {
            log.error("请求失败: {}", e.getMessage());
            return null;
        }
    }


    /**
     * 构建 HTTP 请求的方法
     *
     * @param endpoint       请求的目标地址
     * @param requestType    请求类型，使用 RequestType 枚举表示（GET 或 POST）
     * @param paramType      参数类型，使用 ParamType 枚举表示（JSON 或 FORM，仅适用于 POST）
     * @param requestBodyObj 请求体对象，将根据 paramType 转换为相应的请求体
     * @param username       用户名，用于认证，可为空
     * @param password       密码，用于认证，可为空
     * @return 构建好的 Request 对象
     */
    private static Request buildRequest(String endpoint, RequestType requestType, ParamType paramType, Map<String, Object> requestBodyObj, String username, String password) {
        Request.Builder requestBuilder = new Request.Builder().url(endpoint);


        if (requestType == RequestType.POST) {
            RequestBody requestBody = buildRequestBody(requestBodyObj, paramType);
            if (requestBody != null) {
                requestBuilder.post(requestBody);
            }
        } else if (requestType == RequestType.GET) {
            HttpUrl.Builder urlBuilder = buildQueryParameters(endpoint, requestBodyObj);
            requestBuilder.url(urlBuilder.build()).get();
        }


        if (username != null && password != null) {
            requestBuilder.addHeader("Authorization", Credentials.basic(username, password));
        }


        return requestBuilder.build();
    }


    /**
     * 构建 POST 请求的请求体
     *
     * @param requestBodyObj 请求体对象
     * @param paramType      参数类型，使用 ParamType 枚举表示（JSON 或 FORM）
     * @return 构建好的 RequestBody 对象，如果构建失败则返回 null
     */
    private static RequestBody buildRequestBody(Map<String, Object> requestBodyObj, ParamType paramType) {
        try {
            if (paramType == ParamType.JSON) {
                String jsonBody = objectMapper.writeValueAsString(requestBodyObj);
                String contentType = "application/json; charset=utf-8";
                return RequestBody.create(MediaType.parse(contentType), jsonBody);
            } else if (paramType == ParamType.FORM) {
                FormBody.Builder formBodyBuilder = new FormBody.Builder();
                for (Map.Entry<String, Object> entry : requestBodyObj.entrySet()) {
                    formBodyBuilder.add(entry.getKey(), String.valueOf(entry.getValue()));
                }
                return formBodyBuilder.build();
            }
        } catch (JsonProcessingException e) {
            log.error("请求失败: {}", e.getMessage());
        }
        return null;
    }


    /**
     * 构建 GET 请求的查询参数
     *
     * @param endpoint       请求的目标地址
     * @param requestBodyObj 请求体对象，将转换为查询参数添加到 URL 中
     * @return 构建好的 HttpUrl.Builder 对象
     */
    private static HttpUrl.Builder buildQueryParameters(String endpoint, Map<String, Object> requestBodyObj) {
        HttpUrl.Builder urlBuilder = HttpUrl.get(endpoint).newBuilder();
        for (Map.Entry<String, Object> entry : requestBodyObj.entrySet()) {
            urlBuilder.addQueryParameter(entry.getKey(), String.valueOf(entry.getValue()));
        }
        return urlBuilder;
    }


    public static void main(String[] args) {


        // 测试 POST 请求，JSON 类型参数
        Map<String, Object> jsonRequestBody = new HashMap<>();
        jsonRequestBody.put("param1", "1234");
        jsonRequestBody.put("param2", "5678");
        jsonRequestBody.put("param3", "7890");
        String resultJson = sendRequest("http://127.0.0.1:520/test/wx/post", RequestType.POST, ParamType.JSON, jsonRequestBody, null, null);
        log.debug("resultJson: {}", resultJson);


        // 测试 POST 请求，FORM 类型参数
        Map<String, Object> formRequestBody = new HashMap<>();
        formRequestBody.put("module", "qb61113@qq.com");
        formRequestBody.put("action", "123456");
        String resultForm = sendRequest("http://127.0.0.1:520/test/syz/post/form", RequestType.POST, ParamType.FORM, formRequestBody, null, null);
        log.debug("resultForm: {}", resultForm);


        // 测试 GET 请求
        Map<String, Object> getRequestParam = new HashMap<>();
        getRequestParam.put("username", "qb61113@qq.com");
        getRequestParam.put("password", "123456");
        String resultGet = sendRequest("http://127.0.0.1:520/test/wx/get", RequestType.GET, null, getRequestParam, null, null);
        log.debug("resultGet: {}", resultGet);
    }

}
```



## 二、 AllowOriginFilter 【跨域访问配置】

```java
import com.sihong.vineyard.pojo.access_control.AccessControlDO;
import com.sihong.vineyard.pojo.access_control.AccessControlVO;
import com.sihong.vineyard.service.AccessControlService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @projectName: sihong-vineyard
 * @package: com.sihong.vineyard.config
 * @className: AllowOriginFilter
 * @author: sihong
 * @description: 允许跨域访问
 * @date: 2023/8/14 14:53
 * @version: 1.8
 */
@Slf4j
@Component
public class AllowOriginFilter implements Filter {

    @Autowired
    private AccessControlService accessControlService;

    @Override
    public void destroy() {
        // 清理资源
    }


    /**
     * @param:
     * @return: void
     * @author: QB61113
     * @description: 初始化配置
     * @date: 2025-01-12 0:15
     */
    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        try {
            // 打印请求方法用于调试
            log.info("请求方法: {}", request.getMethod());

            // 检查是否为 WebSocket 升级请求
            String upgradeHeader = request.getHeader("Upgrade");
            log.info("请求头信息: {}", upgradeHeader);
            boolean isWebSocketUpgrade = "websocket".equalsIgnoreCase(upgradeHeader);

            if (isWebSocketUpgrade) {
                // 如果是 WebSocket 升级请求，直接放行
                chain.doFilter(req, res);
                return;
            }


            // 允许的IP列表
            List<String> ALLOWED_IPS = new ArrayList<>();

            AccessControlDO accessControlDO = new AccessControlDO();
            accessControlDO.setIsDeleted(0); // 未废弃的配置
            List<AccessControlVO> allAccessControlList = accessControlService.getAllAccessControl(accessControlDO); // 获取所有未废弃的配置
            if (CollectionUtils.isNotEmpty(allAccessControlList)) {
                allAccessControlList.forEach(accessControlVO -> {
                    if (StringUtils.isNotEmpty(accessControlVO.getIpAddress())) { // 如果有配置的 IP 地址
                        ALLOWED_IPS.add(accessControlVO.getIpAddress()); // 添加到 ALLOWED_IPS
                    }
                });
            }

            String origin = request.getHeader("Origin"); // 获取 Origin 头信息
            String clientIp = getClientIp(request); // 获取客户端的 IP 地址
            String url = request.getRequestURL().toString(); // 获取请求的 URL
            log.info("来自源站的请求: {}, client IP: {}, URL: {}", origin, clientIp, url);

            boolean isAllowed = false;

            if (StringUtils.isNotEmpty(origin)) {
                // 检查 Origin 是否为允许的值
                isAllowed = "*".equals(origin) || ALLOWED_ORIGINS.contains(getOriginWithoutProtocol(origin));
            } else if (clientIp != null && !clientIp.isEmpty()) {
                // 如果没有 Origin，则检查 IP 地址
                isAllowed = ALLOWED_IPS.contains(clientIp);
            }

            if (isAllowed) {
                configureCorsHeaders(response, origin == null ? "*" : origin);
                chain.doFilter(req, res);
            } else {
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                log.warn("来自源站的禁止请求: {}, client IP: {}", origin, clientIp);
            }
        } catch (IOException | ServletException e) {
            log.error("筛选期间出错: {}", e.toString());
            throw e; // 重新抛出异常以便上层能够处理
        }
    }

    /**
     * @param:
     * @return: void
     * @author: QB61113
     * @description: 配置 CORS 头信息
     * @date: 2025-01-12 0:15
     */
    private void configureCorsHeaders(HttpServletResponse response, String allowedOrigin) {
        response.setHeader("Access-Control-Allow-Origin", allowedOrigin);
        response.setHeader("Access-Control-Allow-Headers", "origin, content-type, accept, authorization, x-requested-with");
        response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD, WebSocket");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Content-Type", "application/json;charset=UTF-8");
    }

    /**
     * @param:
     * @return: String
     * @author: QB61113
     * @description: 获取 Origin 的协议和域名
     * @date: 2025-01-12 0:14
     */
    private String getOriginWithoutProtocol(String origin) {
        return origin.replaceFirst("^https?://", "");
    }

    /**
     * @param:
     * @return: String
     * @author: QB61113
     * @description: 获取客户端的 IP 地址
     * @date: 2025-01-12 0:14
     */
    private String getClientIp(HttpServletRequest request) {
        String xfHeader = request.getHeader("X-Forwarded-For");
        if (xfHeader == null) {
            return request.getRemoteAddr();
        }
        return xfHeader.split(",")[0]; // 如果有多个代理，则取第一个IP
    }

    /**
     * @param:
     * @return: null
     * @author: QB61113
     * @description: 初始化配置
     * @date: 2025-01-12 0:14
     */
    // 允许的 Origin 列表（可以包含协议）
    private static final List<String> ALLOWED_ORIGINS = Arrays.asList(
            "http://example.com",
            "https://another-example.org"
    );


    /**
     * @param:
     * @return: void
     * @author: QB61113
     * @description: 初始化配置
     * @date: 2025-01-12 0:14
     */
    @Override
    public void init(FilterConfig arg0) throws ServletException {
        // 初始化配置
    }
}
```



## 三、 RedisUtil

### 3.1 RedisUtil.java

`RedisUtil.java`

```java
package com.hxlei.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @Author lei.hx
 * @Description //Redis工具类
 * @Date 2022/11/21 - 0:27
 * @Version 1.8
 */

@Component
@Slf4j
public class RedisUtil {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    // =============================common============================

    /**
     * 指定缓存失效时间
     *
     * @param key  键
     * @param time 时间(秒)
     * @return
     */
    public boolean expire(String key, long time) {
        try {
            if (time > 0) {
                redisTemplate.expire(key, time, TimeUnit.SECONDS);
            }
            return true;
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return false;
        }
    }

    /**
     * 根据key 获取过期时间
     *
     * @param key 键 不能为null
     * @return 时间(秒) 返回0代表为永久有效
     */
    public long getExpire(String key) {
        try {
            return redisTemplate.getExpire(key, TimeUnit.SECONDS);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return 0;
        }
    }

    /**
     * 判断key是否存在
     *
     * @param key 键
     * @return true 存在 false不存在
     */
    public boolean hasKey(String key) {
        try {
            return redisTemplate.hasKey(key);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return false;
        }
    }

    /**
     * 删除缓存
     *
     * @param key 可以传一个值 或多个
     */
    @SuppressWarnings("unchecked")
    public void del(String... key) {
        try {
            if (key != null && key.length > 0) {
                if (key.length == 1) {
                    redisTemplate.delete(key[0]);
                } else {
                    redisTemplate.delete(String.valueOf(CollectionUtils.arrayToList(key)));
                }
            }
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
        }
    }

    // ============================String=============================

    /**
     * 普通缓存获取
     *
     * @param key 键
     * @return 值
     */
    public Object get(String key) {
        try {
            return key == null ? null : redisTemplate.opsForValue().get(key);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return null;
        }
    }

    /**
     * 普通缓存放入
     *
     * @param key   键
     * @param value 值
     * @return true成功 false失败
     */
    public boolean set(String key, Object value) {
        try {
            redisTemplate.opsForValue().set(key, value);
            return true;
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return false;
        }

    }

    /**
     * 普通缓存放入并设置时间
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒) time要大于0 如果time小于等于0 将设置无限期
     * @return true成功 false 失败
     */
    public boolean set(String key, Object value, long time) {
        try {
            if (time > 0) {
                redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
            } else {
                set(key, value);
            }
            return true;
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return false;
        }
    }

    /**
     * 递增
     *
     * @param key   键
     * @param delta 要增加几(大于0)
     * @return
     */
    public long incr(String key, long delta) {
        try {
            if (delta < 0) {
                throw new RuntimeException("递增因子必须大于0");
            }
            return redisTemplate.opsForValue().increment(key, delta);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return 0;
        }
    }

    /**
     * 递减
     *
     * @param key   键
     * @param delta 要减少几(小于0)
     * @return
     */
    public long decr(String key, long delta) {
        try {
            if (delta < 0) {
                throw new RuntimeException("递减因子必须大于0");
            }
            return redisTemplate.opsForValue().increment(key, -delta);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return 0;
        }
    }


    // ================================Map=================================

    /**
     * HashGet
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return 值
     */
    public Object hget(String key, String item) {
        try {
            return redisTemplate.opsForHash().get(key, item);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return null;
        }
    }

    /**
     * 获取hashKey对应的所有键值
     *
     * @param key 键
     * @return 对应的多个键值
     */
    public Map<Object, Object> hmget(String key) {
        try {
            return redisTemplate.opsForHash().entries(key);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return null;
        }
    }

    /**
     * HashSet
     *
     * @param key 键
     * @param map 对应多个键值
     * @return true 成功 false 失败
     */
    public boolean hmset(String key, Map<String, Object> map) {
        try {
            redisTemplate.opsForHash().putAll(key, map);
            return true;
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return false;
        }
    }

    /**
     * HashSet 并设置时间
     *
     * @param key  键
     * @param map  对应多个键值
     * @param time 时间(秒)
     * @return true成功 false失败
     */
    public boolean hmset(String key, Map<String, Object> map, long time) {
        try {
            redisTemplate.opsForHash().putAll(key, map);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return false;
        }
    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @return true 成功 false失败
     */
    public boolean hset(String key, String item, Object value) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            return true;
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return false;
        }
    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @param time  时间(秒) 注意:如果已存在的hash表有时间,这里将会替换原有的时间
     * @return true 成功 false失败
     */
    public boolean hset(String key, String item, Object value, long time) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return false;
        }
    }

    /**
     * 删除hash表中的值
     *
     * @param key  键 不能为null
     * @param item 项 可以使多个 不能为null
     */
    public void hdel(String key, Object... item) {
        try {
            redisTemplate.opsForHash().delete(key, item);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
        }
    }

    /**
     * 判断hash表中是否有该项的值
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return true 存在 false不存在
     */
    public boolean hHasKey(String key, String item) {
        try {
            return redisTemplate.opsForHash().hasKey(key, item);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return false;
        }
    }

    /**
     * hash递增 如果不存在,就会创建一个 并把新增后的值返回
     *
     * @param key  键
     * @param item 项
     * @param by   要增加几(大于0)
     * @return
     */
    public double hincr(String key, String item, double by) {
        try {
            return redisTemplate.opsForHash().increment(key, item, by);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return 0;
        }
    }

    /**
     * hash递减
     *
     * @param key  键
     * @param item 项
     * @param by   要减少记(小于0)
     * @return
     */
    public double hdecr(String key, String item, double by) {
        try {
            return redisTemplate.opsForHash().increment(key, item, -by);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return 0;
        }
    }

    // ============================set=============================

    /**
     * 根据key获取Set中的所有值
     *
     * @param key 键
     * @return
     */
    public Set<Object> sGet(String key) {
        try {
            return redisTemplate.opsForSet().members(key);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return null;
        }
    }

    /**
     * 根据value从一个set中查询,是否存在
     *
     * @param key   键
     * @param value 值
     * @return true 存在 false不存在
     */
    public boolean sHasKey(String key, Object value) {
        try {
            return redisTemplate.opsForSet().isMember(key, value);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return false;
        }
    }

    /**
     * 将数据放入set缓存
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 成功个数
     */
    public long sSet(String key, Object... values) {
        try {
            return redisTemplate.opsForSet().add(key, values);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return 0;
        }
    }

    /**
     * 将set数据放入缓存
     *
     * @param key    键
     * @param time   时间(秒)
     * @param values 值 可以是多个
     * @return 成功个数
     */
    public long sSetAndTime(String key, long time, Object... values) {
        try {
            Long count = redisTemplate.opsForSet().add(key, values);
            if (time > 0) {
                expire(key, time);
            }
            return count;
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return 0;
        }
    }

    /**
     * 获取set缓存的长度
     *
     * @param key 键
     * @return
     */
    public long sGetSetSize(String key) {
        try {
            return redisTemplate.opsForSet().size(key);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return 0;
        }
    }

    /**
     * 移除值为value的
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 移除的个数
     */
    public long setRemove(String key, Object... values) {
        try {
            Long count = redisTemplate.opsForSet().remove(key, values);
            return count;
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return 0;
        }
    }
    // ===============================list=================================

    /**
     * 获取list缓存的内容
     *
     * @param key   键
     * @param start 开始
     * @param end   结束 0 到 -1代表所有值
     * @return
     */
    public List<Object> lGet(String key, long start, long end) {
        try {
            return redisTemplate.opsForList().range(key, start, end);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return null;
        }
    }

    /**
     * 获取list缓存的长度
     *
     * @param key 键
     * @return
     */
    public long lGetListSize(String key) {
        try {
            return redisTemplate.opsForList().size(key);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return 0;
        }
    }

    /**
     * 通过索引 获取list中的值
     *
     * @param key   键
     * @param index 索引 index>=0时， 0 表头，1 第二个元素，依次类推；index<0时，-1，表尾，-2倒数第二个元素，依次类推
     * @return
     */
    public Object lGetIndex(String key, long index) {
        try {
            return redisTemplate.opsForList().index(key, index);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return null;
        }
    }

    /**
     * 将list放入缓存 从左边(上面)开始放
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public boolean lSet(String key, Object value) {
        try {
            redisTemplate.opsForList().leftPush(key, value);
            return true;
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return false;
        }
    }

    /**
     * @param:
     * @return: boolean
     * @author: hxlei
     * @description: 将list放入缓存 从右边(下面)开始放
     * @date: 2023/8/25 13:35
     */
    public boolean lRSet(String key, Object value) {
        try {
            redisTemplate.opsForList().rightPush(key, value);
            return true;
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return false;
        }
    }

    /**
     * @param:
     * @return: boolean
     * @author: hxlei
     * @description: 将list放入缓存 从右边(下面)开始放
     * @date: 2023/8/25 14:39
     */
    public boolean lRSet(String key, Object value, long time) {
        try {
            redisTemplate.opsForList().rightPush(key, value);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return false;
        }
    }


    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒)
     * @return
     */
    public boolean lSet(String key, Object value, long time) {
        try {
            redisTemplate.opsForList().leftPush(key, value);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public boolean lSet(String key, List<Object> value) {
        try {
            redisTemplate.opsForList().leftPushAll(key, value);
            return true;
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒)
     * @return
     */
    public boolean lSet(String key, List<Object> value, long time) {
        try {
            redisTemplate.opsForList().leftPushAll(key, value);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return false;
        }
    }

    /**
     * 根据索引修改list中的某条数据
     *
     * @param key   键
     * @param index 索引
     * @param value 值
     * @return
     */
    public boolean lUpdateIndex(String key, long index, Object value) {
        try {
            redisTemplate.opsForList().set(key, index, value);
            return true;
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return false;
        }
    }

    /**
     * 移除N个值为value
     *
     * @param key   键
     * @param count 移除多少个
     * @param value 值
     * @return 移除的个数
     */
    public long lRemove(String key, long count, Object value) {
        try {
            Long remove = redisTemplate.opsForList().remove(key, count, value);
            return remove;
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return 0;
        }
    }

// ===============================Zset=================================

    /**
     * @param: key，member，score
     * @return: null
     * @author: QB61113
     * @description: 设置zset值  long类型
     * @date: 2024-01-09 15:15
     */
    public boolean zSetAdd(String key, Object member, long score) {
        try {
            return redisTemplate.opsForZSet().add(key, member, score);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return false;
        }
    }

    /**
     * @param: key，member，score
     * @return: null
     * @author: QB61113
     * @description: 设置zset值  double类型
     * @date: 2024-01-09 15:15
     */
    public boolean zSetAdd(String key, Object member, double score) {
        try {
            return redisTemplate.opsForZSet().add(key, member, score);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return false;
        }
    }

    /**
     * @param:
     * @return: long
     * @author: QB61113
     * @description: 批量设置zset值 重复的覆盖
     * @date: 2024-01-09 15:16
     */
    public long zSetAdd(String key, Set<ZSetOperations.TypedTuple<Object>> tuples) {
        try {
            return redisTemplate.opsForZSet().add(key, tuples);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return 0;
        }

    }

    /**
     * @param:
     * @return: long
     * @author: QB61113
     * @description: 批量设置zset值 不存在的才设置
     * @date: 2024-01-09 15:16
     */
    public long zSetAddIfAbsent(String key, Set<ZSetOperations.TypedTuple<Object>> tuples) {
        try {
            return redisTemplate.opsForZSet().addIfAbsent(key, tuples);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return 0;
        }

    }

    /**
     * @param:
     * @return: null
     * @author: QB61113
     * @description: 移除某一个或者多个成员
     * @date: 2024-01-09 16:49
     */
    public long zSetRemove(String key, Object... members) {
        try {
            return redisTemplate.opsForZSet().remove(key, members);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return 0;
        }
    }

    /**
     * @param:
     * @return: void
     * @author: QB61113
     * @description: 自增zset值
     * @date: 2024-01-09 15:16
     */
    public void zSetIncValue(String key, Object member, long delta) {
        try {
            redisTemplate.opsForZSet().incrementScore(key, member, delta);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
        }
    }

    /**
     * @param:
     * @return: long
     * @author: QB61113
     * @description: 获取zset 成员数量
     * @date: 2024-01-09 15:17
     */
    public long zSetGetScore(String key, Object member) {
        try {
            Double score = redisTemplate.opsForZSet().score(key, member);
            if (score == null) {
                return 0;
            } else {
                return score.longValue();
            }
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return 0;
        }
    }

    /** ================ zSet 按索引范围 ================== */
    /**
     * @param:
     * @return: Set<Object>
     * @author: QB61113
     * @description: 从大到小 按索引范围start-end获取zset值
     * @date: 2024-01-09 15:18
     */
    public Set<Object> zSetReverseRange(String key, long start, long end) {
        try {
            return redisTemplate.opsForZSet().reverseRange(key, start, end);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return null;
        }
    }

    /**
     * @param:
     * @return: Set<Object>
     * @author: QB61113
     * @description: 从小到大 按索引范围start-end获取zset值
     * @date: 2024-01-09 15:18
     */
    public Set<Object> zSetRange(String key, long start, long end) {
        try {
            return redisTemplate.opsForZSet().range(key, start, end);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return null;
        }
    }

    /**
     * @param:
     * @return: Set<ZSetOperations.TypedTuple < Object>>
     * @author: QB61113
     * @description: 从大到小 按索引范围start-end获取zset成员信息
     * @date: 2024-01-09 15:18
     */
    public Set<ZSetOperations.TypedTuple<Object>> zSetReverseRangeWithScores(String key, long start, long end) {
        try {
            return redisTemplate.opsForZSet().reverseRangeWithScores(key, start, end);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return null;
        }
    }

    /**
     * @param:
     * @return: Set<ZSetOperations.TypedTuple < Object>>
     * @author: QB61113
     * @description: 从小到大 按索引范围start-end获取zset成员信息
     * @date: 2024-01-09 15:18
     */
    public Set<ZSetOperations.TypedTuple<Object>> zSetRangeWithScores(String key, long start, long end) {
        try {
            return redisTemplate.opsForZSet().rangeWithScores(key, start, end);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return null;
        }
    }

    /** ================ zSet 按值范围 ================== */
    /**
     * @param:
     * @return: Set<Object>
     * @author: QB61113
     * @description: 从大到小 按值范围max-min获取zset成员信息
     * @date: 2024-01-09 15:18
     */
    public Set<Object> zSetReverseRangeByScore(String key, long min, long max) {
        try {
            return redisTemplate.opsForZSet().reverseRangeByScore(key, min, max);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return null;
        }
    }

    /**
     * @param:
     * @return: Set<Object>
     * @author: QB61113
     * @description: 从小到大 按值范围max-min获取zset成员信息
     * @date: 2024-01-09 15:18
     */
    public Set<Object> zSetRangeByScore(String key, long min, long max) {
        try {
            return redisTemplate.opsForZSet().rangeByScore(key, min, max);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return null;
        }
    }


    /**
     * @param:
     * @return: Set<ZSetOperations.TypedTuple < Object>>
     * @author: QB61113
     * @description: 从大到小 按值范围max-min获取zset成员信息
     * @date: 2024-01-09 15:18
     */
    public Set<ZSetOperations.TypedTuple<Object>> zSetReverseRangeByScoreWithScores(String key, long min, long max) {
        try {
            return redisTemplate.opsForZSet().reverseRangeByScoreWithScores(key, min, max);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return null;
        }
    }


    /**
     * @param:
     * @return: Set<ZSetOperations.TypedTuple < Object>>
     * @author: QB61113
     * @description: 从小到大 按值范围max-min获取zset成员信息
     * @date: 2024-01-09 15:18
     */
    public Set<ZSetOperations.TypedTuple<Object>> zSetRangeByScoreWithScores(String key, long min, long max) {
        try {
            return redisTemplate.opsForZSet().rangeByScoreWithScores(key, min, max);
        } catch (Exception e) {
            log.error("异常: {}", e.toString());
            return null;
        }
    }


    /**
     * @param:
     * @return: null
     * @author: QB61113
     * @description: 获取ZSet集合的size
     * @date: 2024-04-02 13:01
     */
    public long getZSetSize(String key) {
        Long size = redisTemplate.opsForZSet().size(key);
        return size != null ? size : 0;
    }

}
```



### 3.2 RedisConfig.java

`RedisConfig.java`

```java
package com.hxlei.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @Author lei.hx
 * @Description //Redis配置模板
 * @Date 2022/11/20 - 18:11
 * @Version 1.8
 */
@Configuration
public class RedisConfig {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        // 为了开发方便，一般直接使用<String, Object>
        RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
        template.setConnectionFactory(redisConnectionFactory);

        // Json序列化配置
        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);

        // String的序列化
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();

        // key采用String的序列化方式
        template.setKeySerializer(stringRedisSerializer);
        // hash的key也采用String的序列化方式
        template.setHashKeySerializer(stringRedisSerializer);
        // value序列化方式采用jackson
        template.setValueSerializer(jackson2JsonRedisSerializer);
        // hash的value序列化方式采用jackson
        template.setHashValueSerializer(jackson2JsonRedisSerializer);

        template.afterPropertiesSet();

        return template;

    }

}
```

