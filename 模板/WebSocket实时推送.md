# WebSocket实时推送

## 一、依赖准备

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-websocket</artifactId>
</dependency>
```

## 二、配置

### 2.1 单组Socket的简单配置

准备一个WebSocket配置类

```java
/**
 * WebSocket配置类
 */
@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Autowired
    private MyWebSocketHandler myWebSocketHandler;

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(myWebSocketHandler, "/weather-updates")
                .setAllowedOrigins("*"); // 根据需求配置允许的域名
    }
}
```

准备一个WebSocket 处理器

```java
/**
 * WebSocket 处理器
 */
@Slf4j
@Component
public class MyWebSocketHandler extends TextWebSocketHandler {

    // 存储组与会话的映射
    private final ConcurrentHashMap<String, Set<WebSocketSession>> groupSessions = new ConcurrentHashMap<>();

    // 添加到指定的组
    public void addToGroup(String group, WebSocketSession session) {
        groupSessions.computeIfAbsent(group, k -> {
            log.info("创建新组 -> {}", group);
            return new HashSet<>();
        }).add(session);
        log.info("组 [{}] 添加会话 [{}], 当前会话数: {}", group, session.getId(), groupSessions.get(group).size());
    }

    // 从指定的组移除
    public void removeFromGroup(String group, WebSocketSession session) {
        Set<WebSocketSession> groupSet = groupSessions.get(group);
        if (groupSet != null) {
            groupSet.remove(session);
            log.info("组 [{}] 移除会话 [{}], 当前会话数: {}", group, session.getId(), groupSet.size());
            if (groupSet.isEmpty()) {
                groupSessions.remove(group);
                log.info("组 [{}] 已被移除", group);
            }
        }
    }


    // 按组推送消息
    public void broadcastToGroup(String group, String message) {
        Set<WebSocketSession> groupSet = groupSessions.get(group);
        if (groupSet != null) {
            for (WebSocketSession session : groupSet) {
                try {
                    if (session.isOpen()) {
                        session.sendMessage(new TextMessage(message));
                    }
                } catch (IOException e) {
                    log.error("推送消息失败 -> {}", e.getMessage());
                }
            }
        }
    }

    // 获取指定组的所有会话
    public Set<WebSocketSession> getSessionsForGroup(String group) {
        return groupSessions.get(group);
    }


    // 处理连接事件
    @Override
    public void afterConnectionEstablished(WebSocketSession session) {
        log.info("新客户端连接 -> {}", session.getId());
        this.addToGroup("defaultGroup", session);
    }

    // 处理消息事件
    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) {
        // 处理消息
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        log.info("客户端断开连接 -> {}", session.getId());
        this.removeFromGroup("defaultGroup", session);
    }
}
```

封装一个推送的方案

```java
@Component
@Slf4j
public class WebSocket {

    @Autowired
    private MyWebSocketHandler myWebSocketHandler; // WebSocket服务


    /**
     * 发送天气数据
     *
     * @throws JsonProcessingException
     */
    public void checkAndPushWeatherUpdate() throws JsonProcessingException {
        try {
            // 推送更新到 WebSocket
            myWebSocketHandler.broadcastToGroup("deafultroup", "天气笑嘻嘻"); 

        } catch (Exception e) {
            log.error("推送天气数据失败 -> {}", e.toString());
        }
    }
}
```

客户端建立WebSocket连接

注意：http请求用 ws 例如： `new WebSocket('wss://127.0.0.1:8083/weather-updates');`

https请求用 wss  例如：`new WebSocket('wss://127.0.0.1:8083/weather-updates');`

```js
<script>
    const socket = new WebSocket('wss://127.0.0.1:8083/weather-updates');
    const weatherDiv = $('#weather-now').val();
    console.log('weatherDiv ->', weatherDiv);

    socket.onmessage = (event) => {
      	// 处理数据
        $('#weather-now').text(event.data);
    };

    socket.onopen = () => {
        console.log('WebSocket 连接已建立');
    };

    socket.onclose = () => {
        console.log('WebSocket 连接已关闭');
    };

    socket.onerror = (error) => {
        console.error('WebSocket 错误:', error);
    };
</script>
```

### 2.2 常用多组Socket的动态配置

在`application.yml`中添加路径和组

```yaml
# websocket 组
websocket:
  groups:
    - path: /weather
      group: weatherGroup
    - path: /news
      group: newsGroup
    # 可以继续添加其他路径和组
```

准备一个映射类

```java
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import java.util.List;

/**
 * websocket 动态组映射配置
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ConfigurationProperties(prefix = "websocket")
public class WebSocketGroupConfig {

    private List<GroupConfig> groups;  // 用来存储多个组的配置

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class GroupConfig {
        private String path;   // 路径
        private String group;  // 组名
    }

}
```

在WebSocket配置类中动态注册组和路径

```java
/**
 * WebSocket配置类
 */
@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Autowired
    private MyWebSocketHandler myWebSocketHandler;

    @Autowired
    private WebSocketGroupConfig webSocketGroupConfig; // 注入 WebSocketGroupConfig

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        // 动态注册路径
        for (WebSocketGroupConfig.GroupConfig groupConfig : webSocketGroupConfig.getGroups()) {
            registry.addHandler(myWebSocketHandler, groupConfig.getPath())
                    .setAllowedOrigins("*"); // 根据配置文件动态注册路径
        }
    }
}
```

 在WebSocket 处理器中动态处理组和路径

```java
/**
 * WebSocket 处理器
 */
@Slf4j
@Component
public class MyWebSocketHandler extends TextWebSocketHandler {

    private final ConcurrentHashMap<String, Set<WebSocketSession>> groupSessions = new ConcurrentHashMap<>();
    private final Map<String, String> pathToGroupMap = new HashMap<>();  // 路径到组的映射

    @Autowired
    private WebSocketGroupConfig websocketGroups;  // 注入 WebSocketGroupConfig 配置类


    @PostConstruct
    public void init() {
        // 初始化时，加载路径与组的映射关系
        for (WebSocketGroupConfig.GroupConfig groupConfig : websocketGroups.getGroups()) {
            pathToGroupMap.put(groupConfig.getPath(), groupConfig.getGroup());
        }
    }

    // 添加到指定的组
    public void addToGroup(String group, WebSocketSession session) {
        groupSessions.computeIfAbsent(group, k -> new HashSet<>()).add(session);
        log.info("组 [{}] 添加会话 [{}], 当前会话数: {}", group, session.getId(), groupSessions.get(group).size());
    }

    // 从指定的组移除
    public void removeFromGroup(String group, WebSocketSession session) {
        Set<WebSocketSession> groupSet = groupSessions.get(group);
        if (groupSet != null) {
            groupSet.remove(session);
            log.info("组 [{}] 移除会话 [{}], 当前会话数: {}", group, session.getId(), groupSet.size());
            if (groupSet.isEmpty()) {
                groupSessions.remove(group);
                log.info("组 [{}] 已被移除", group);
            }
        }
    }

    // 按组推送消息
    public void broadcastToGroup(String group, String message) {
        Set<WebSocketSession> groupSet = groupSessions.get(group);
        if (groupSet != null) {
            for (WebSocketSession session : groupSet) {
                try {
                    if (session.isOpen()) {
                        session.sendMessage(new TextMessage(message));
                        log.info("时间 -> {}, 推送组 -> {}, 推送消息 -> {}", System.currentTimeMillis(), group, message);
                    }
                } catch (IOException e) {
                    log.error("推送消息失败 -> {}", e.getMessage());
                }
            }
        }
    }

    // 处理连接事件
    @Override
    public void afterConnectionEstablished(WebSocketSession session) {
        String uri = Objects.requireNonNull(session.getUri()).toString();
        log.info("新客户端连接 -> {}", session.getId());

        String group = pathToGroupMap.entrySet().stream()
                .filter(entry -> uri.contains(entry.getKey()))
                .map(Map.Entry::getValue)
                .findFirst()
                .orElse("defaultGroup");

        this.addToGroup(group, session);
    }

    // 处理断开连接事件
    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        log.info("客户端断开连接 -> {}", session.getId());
        String uri = Objects.requireNonNull(session.getUri()).toString();

        String group = pathToGroupMap.entrySet().stream()
                .filter(entry -> uri.contains(entry.getKey()))
                .map(Map.Entry::getValue)
                .findFirst()
                .orElse("defaultGroup");

        this.removeFromGroup(group, session);
    }
}
```

推送方案需要指定到组

```java
@Component
@Slf4j
public class WebSocket {

    @Autowired
    private MyWebSocketHandler myWebSocketHandler; // WebSocket服务


    /**
     * 发送天气数据
     *
     * @throws JsonProcessingException
     */
    public void checkAndPushWeatherUpdate() throws JsonProcessingException {
        try {
            // 推送更新到 WebSocket
            myWebSocketHandler.broadcastToGroup("weatherGroup", "天气笑嘻嘻"); // weatherGroup这里指定组

        } catch (Exception e) {
            log.error("推送天气数据失败 -> {}", e.toString());
        }
    }
}
```

最后在Application启动程序中添加读取配置文件的注解`@EnableConfigurationProperties(WebSocketGroupConfig.class)`

```java
@SpringBootApplication
@EnableConfigurationProperties(WebSocketGroupConfig.class) // 读取配置文件
public class SihongVineyardApplication {

    public static void main(String[] args) {
        SpringApplication.run(SihongVineyardApplication.class, args);

        System.out.println("\033[32m" + " _________________\n" +
                "< Hi, 我是牛哥…… 启动成功！>\n" +
                " -----------------\n" +
                "        \\   ^__^\n" +
                "         \\  (oo)\\_______\n" +
                "            (__)\\       )\\/\\\n" +
                "                ||----w |\n" +
                "                ||     ||" + "\033[0m");
    }

}
```

客户端建立多组WebSocket连接

```javascript
<script>
const host = 'https://127.0.0.1:8083'
    const socketHost = 'wss://' + host.replace('http://', '').replace('https://', ''); // socket 地址
    const hosts = {
        hostLivesWeather: socketHost + '/livesWeather', // 实况天气socket
        hostForecastWeather: socketHost + '/forecastWeather', // 预报天气socket
        hostNews: socketHost + '/news' // 新闻socket
    }

    // 连接实况天气 WebSocket
    connectWebSocket(hosts.hostLivesWeather, (event) => {
        setLivesWeather(JSON.parse(event.data))
    });

    // 连接预报天气 WebSocket
    connectWebSocket(hosts.hostForecastWeather, (event) => {
        setForecastWeather(JSON.parse(event.data))
    });

    // 连接新闻 WebSocket
    connectWebSocket(hosts.hostNews, (event) => {
        setNews(JSON.parse(event.data))
    });


    // 处理 WebSocket 连接的通用方法
    function connectWebSocket(url, onMessageCallback) {
        let socket;
        let reconnectInterval = 1000;  // 初始重连间隔，1秒
        let reconnectTimeout;

        /* 重连函数 */
        function reconnect() {
            clearTimeout(reconnectTimeout);  // 清除之前的重连定时器
            reconnectTimeout = setTimeout(() => {
                console.log(new Date().toLocaleString() + ' 尝试重新连接 WebSocket...');
                socket = new WebSocket(url);  // 尝试重新连接
                handleSocket(socket);  // 重新设置 socket 事件监听
            }, reconnectInterval);

            // 每次重连时，逐渐增加间隔（指数退避），最大30秒
            reconnectInterval = Math.min(reconnectInterval * 2, 30000);  // 最大间隔30秒
        }


        /* 设置 socket 事件监听 */
        function handleSocket(socket) {
            socket.onopen = () => {
                console.log(new Date().toLocaleString() + ' ' + url + ' WebSocket 连接已建立');
                reconnectInterval = 1000;  // 重置间隔为初始值
            };

            socket.onmessage = (event) => {
                onMessageCallback(event);
            };

            socket.onclose = () => {
                console.log(new Date().toLocaleString() + ' ' + url + ' WebSocket 连接已关闭');
                reconnect();  // 连接关闭时尝试重连
            };

            socket.onerror = (error) => {
                const errorMessage = new Date().toLocaleString() + ' ' + url + ' WebSocket 连接发生错误：';
                console.error(errorMessage, error);

                // 如果 WebSocket 出现错误，尝试重连
                reconnect();  // 可以在这里调用重连机制

                // 进一步处理错误
                if (error && error instanceof ErrorEvent) {
                    // 错误类型检查
                    if (error.message.includes("ECONNREFUSED")) {
                        console.error('连接被拒绝，请检查服务器是否启动或网络问题');
                    } else if (error.message.includes("INVALID_STATE_ERR")) {
                        console.error('WebSocket 状态无效，请检查 WebSocket 配置');
                    } else {
                        console.error('发生未知错误:', error.message);
                    }
                } else {
                    console.error('未知的 WebSocket 错误:', error);
                }

                // 显示给用户的友好提示（如果有 UI）
                // alert("WebSocket 连接遇到问题，正在尝试重新连接...");
            };

        }

        // 初次连接
        socket = new WebSocket(url);
        handleSocket(socket);

        return socket;
    }
</script>
```

监听，防止服务端宕机，定时重连。

## 三、监听和使用推送

### 3.1 结合定时任务

方式一：可以搭配定时任务去定时推送



### 3.2 事件监听器

方式二：可以使用事件监听器去监听事件操作推送

比如每次重启服务端时，向客户端发送一条重启标识，以便让浏览器实现自动刷新。

1、实现 `ApplicationListener` 接口监听应用启动事件

```java
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class WebSocketStartupMessageSender implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private MyWebSocketHandler myWebSocketHandler;

    /**
     * 此方法用于监听Spring应用上下文刷新事件（ContextRefreshedEvent），并在后端重启完成且应用上下文初始化完毕后，
     * 向前端客户端发送一条表示后端已重启的消息。通过获取注入的MyWebSocketHandler实例，调用其broadcastToGroup方法
     * 来实现消息的广播推送。
     *
     * @param event 传入的Spring应用上下文刷新事件（ContextRefreshedEvent）对象，该对象包含了应用上下文刷新相关的信息，
     *              通过此对象可以判断是否是根应用上下文刷新等情况，以确保消息发送逻辑只在合适的时机执行一次，避免重复发送。
     * @return 此方法无返回值，它的主要作用是响应应用上下文刷新事件，执行发送消息的操作，以此完成后端重启后通知前端的功能。
     */
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (event.getApplicationContext().getParent() == null) {
            // 确保只在根应用上下文刷新时执行（避免重复执行，比如在子上下文中也触发）
            String message = "restartFlag";
            // 发送消息
            myWebSocketHandler.broadcastToGroup("defaultGroup", message);
            log.info("已向客户端发送重启命令");
        }
    }
}
```

2、在客户端判断是否是重启标识

```javascript
socket.onmessage = (event) => {
                // 判断消息中是否包含重启信息，如果消息是restartFlag，则说明是重启
                if (event && event.data === 'restartFlag') {
                    console.log('检测到服务端重启，即将刷新浏览器...');
                    window.location.reload();
                } else {
                    // 如果不是重启相关消息，正常执行业务逻辑对应的回调函数
                    onMessageCallback(event);
                }
            };
```



### 3.3 消息队列



……等等等