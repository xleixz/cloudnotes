# OKHttp详解

[TOC]

---

## 准备Maven依赖

```xml
<!-- https://mvnrepository.com/artifact/com.squareup.okhttp3/okhttp -->
<dependency>
    <groupId>com.squareup.okhttp3</groupId>
    <artifactId>okhttp</artifactId>
    <version>4.10.0</version>
</dependency>
```

## 1、发送 GET 请求

```java
@Test
    public void OkHttpTest() {
        OkHttpClient client = new OkHttpClient.Builder()
            //连接超时(单位:秒)
            .connectTimeout(30, TimeUnit.SECONDS)
            //整个流程耗费的超时时间(单位:秒)--很少人使用
            .callTimeout(120, TimeUnit.SECONDS)
            //websocket轮训间隔(单位:秒)
            .pingInterval(5, TimeUnit.SECONDS)
            //读取超时(单位:秒)
            .readTimeout(60, TimeUnit.SECONDS)
            //写入超时(单位:秒)
            .writeTimeout(60, TimeUnit.SECONDS)
            .build();

        try {
          	String endpoint = "https://baishitongpro.cn:520/test/wx/get";
          	// 构建请求的 URL，并添加查询参数
            HttpUrl url = Objects.requireNonNull(HttpUrl.parse(endpoint))
                    .newBuilder()
                    .addQueryParameter("city", "xxxx")
                    .addQueryParameter("unit", "metric")
                    .build();
          
            Request build = new Request.Builder()
                    .url(url)
                    .get()
                    .build();
            Response response = client.newCall(build).execute();
            if (response.body() != null) {
                JSONObject jsonObject = JSON.parseObject(response.body().string());
                System.out.println("----------------------------------------------------");
                System.out.println(jsonObject);
            } else {
                System.out.println("----------------------------------------------------");
                System.out.println("请求失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
```

## 2、发送 POST 请求 

### 2.1、 x-www-form-urlencoded

```java
OkHttpClient client = new OkHttpClient.Builder()
            //连接超时(单位:秒)
            .connectTimeout(30, TimeUnit.SECONDS)
            //整个流程耗费的超时时间(单位:秒)--很少人使用
            .callTimeout(120, TimeUnit.SECONDS)
            //websocket轮训间隔(单位:秒)
            .pingInterval(5, TimeUnit.SECONDS)
            //读取超时(单位:秒)
            .readTimeout(60, TimeUnit.SECONDS)
            //写入超时(单位:秒)
            .writeTimeout(60, TimeUnit.SECONDS)
            .build();

try {
            String endpoint = "https://baishitongpro.cn:520/test/syz/post/form";
            RequestBody formBody = new FormBody.Builder()
                    .add("username", "qb61113@qq.com")
                    .add("password", "123456")
                    .build();

            Request build = new Request.Builder()
                    .url(endpoint)
                    .addHeader("Authorization", Credentials.basic("username", "password"))
                    .post(formBody)
                    .build();
            Response response = client.newCall(build).execute();
            if (response.body() != null) {
                JSONObject jsonObject = JSON.parseObject(response.body().string());
                System.out.println("----------------------------------------------------");
                System.out.println(jsonObject);
            } else {
                System.out.println("----------------------------------------------------");
                System.out.println("请求失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
```

### 2.2、json

```java
OkHttpClient client = new OkHttpClient.Builder()
            //连接超时(单位:秒)
            .connectTimeout(30, TimeUnit.SECONDS)
            //整个流程耗费的超时时间(单位:秒)--很少人使用
            .callTimeout(120, TimeUnit.SECONDS)
            //websocket轮训间隔(单位:秒)
            .pingInterval(5, TimeUnit.SECONDS)
            //读取超时(单位:秒)
            .readTimeout(60, TimeUnit.SECONDS)
            //写入超时(单位:秒)
            .writeTimeout(60, TimeUnit.SECONDS)
            .build();

try {
            String contentType = "application/json; charset=utf-8";
            String endpoint = "https://baishitongpro.cn:520/test/wx/post";
            JSONObject body = new JSONObject();
            body.put("param1", "1234");
            body.put("param2", "5678");
            Request build = new Request.Builder()
                    .url(endpoint)
                    .post(RequestBody.create(MediaType.parse(contentType), String.valueOf(body)))
                    .build();
            Response response = client.newCall(build).execute();
            if (response.body() != null) {
                JSONObject jsonObject = JSON.parseObject(response.body().string());
                System.out.println("----------------------------------------------------");
                System.out.println(jsonObject);
            } else {
                System.out.println("----------------------------------------------------");
                System.out.println("请求失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
```

## 3、SSL证书验证问题

引入依赖

```java
import javax.net.ssl.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
```

增加接受不信任证书配置

```java
OkHttpClient client = new OkHttpClient.Builder()
    .sslSocketFactory(getSSLSocketFactory(), (X509TrustManager) getTrustManager())
    ……
    ……
```



```java
private static SSLSocketFactory getSSLSocketFactory() {
        try {
            TrustManager[] trustManagers = new TrustManager[]{
                    getTrustManager()
            };

            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustManagers, new java.security.SecureRandom());
            return sslContext.getSocketFactory();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static TrustManager getTrustManager() {
        return new X509TrustManager() {
            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[]{};
            }
        };
    }
```

## 4、封装通用工具类

```java
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * @description: OkHttpUtils工具类
 */
@Component
@Slf4j
public class OkHttpUtils {
    private static final OkHttpClient client = new OkHttpClient.Builder() // OkHttpClient 实例，设置了连接超时、调用超时、ping 间隔、读取超时和写入超时
            .connectTimeout(30, TimeUnit.SECONDS)
            .callTimeout(120, TimeUnit.SECONDS)
            .pingInterval(5, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .build();
    private static final ObjectMapper objectMapper = new ObjectMapper();


    // 请求类型枚举，包含 GET 和 POST
    public enum RequestType {
        GET,
        POST
    }


    // 参数类型枚举，包含 JSON 和 FORM
    public enum ParamType {
        JSON,
        FORM
    }


    /**
     * 发送 HTTP 请求的方法
     *
     * @param endpoint       请求的目标地址
     * @param requestType    请求类型，使用 RequestType 枚举表示（GET 或 POST）
     * @param paramType      参数类型，使用 ParamType 枚举表示（JSON 或 FORM，仅适用于 POST）
     * @param requestBodyObj 请求体对象，将根据 paramType 转换为相应的请求体
     * @param username       用户名，用于认证，可为空
     * @param password       密码，用于认证，可为空
     * @return 请求的响应结果字符串，如果请求失败则返回 null
     */
    public static String sendRequest(String endpoint, RequestType requestType, ParamType paramType, Map<String, Object> requestBodyObj, String username, String password) {
        try {
            Request request = buildRequest(endpoint, requestType, paramType, requestBodyObj, username, password);
            try (Response response = client.newCall(request).execute()) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        return response.body().string();
                    } else {
                        return null;
                    }
                } else {
                    log.error("请求失败，状态码: {}", response.code());
                    return null;
                }
            }
        } catch (IOException e) {
            log.error("请求失败: {}", e.getMessage());
            return null;
        }
    }


    /**
     * 构建 HTTP 请求的方法
     *
     * @param endpoint       请求的目标地址
     * @param requestType    请求类型，使用 RequestType 枚举表示（GET 或 POST）
     * @param paramType      参数类型，使用 ParamType 枚举表示（JSON 或 FORM，仅适用于 POST）
     * @param requestBodyObj 请求体对象，将根据 paramType 转换为相应的请求体
     * @param username       用户名，用于认证，可为空
     * @param password       密码，用于认证，可为空
     * @return 构建好的 Request 对象
     */
    private static Request buildRequest(String endpoint, RequestType requestType, ParamType paramType, Map<String, Object> requestBodyObj, String username, String password) {
        Request.Builder requestBuilder = new Request.Builder().url(endpoint);


        if (requestType == RequestType.POST) {
            RequestBody requestBody = buildRequestBody(requestBodyObj, paramType);
            if (requestBody != null) {
                requestBuilder.post(requestBody);
            }
        } else if (requestType == RequestType.GET) {
            HttpUrl.Builder urlBuilder = buildQueryParameters(endpoint, requestBodyObj);
            requestBuilder.url(urlBuilder.build()).get();
        }


        if (username != null && password != null) {
            requestBuilder.addHeader("Authorization", Credentials.basic(username, password));
        }


        return requestBuilder.build();
    }


    /**
     * 构建 POST 请求的请求体
     *
     * @param requestBodyObj 请求体对象
     * @param paramType      参数类型，使用 ParamType 枚举表示（JSON 或 FORM）
     * @return 构建好的 RequestBody 对象，如果构建失败则返回 null
     */
    private static RequestBody buildRequestBody(Map<String, Object> requestBodyObj, ParamType paramType) {
        try {
            if (paramType == ParamType.JSON) {
                String jsonBody = objectMapper.writeValueAsString(requestBodyObj);
                String contentType = "application/json; charset=utf-8";
                return RequestBody.create(MediaType.parse(contentType), jsonBody);
            } else if (paramType == ParamType.FORM) {
                FormBody.Builder formBodyBuilder = new FormBody.Builder();
                for (Map.Entry<String, Object> entry : requestBodyObj.entrySet()) {
                    formBodyBuilder.add(entry.getKey(), String.valueOf(entry.getValue()));
                }
                return formBodyBuilder.build();
            }
        } catch (JsonProcessingException e) {
            log.error("请求失败: {}", e.getMessage());
        }
        return null;
    }


    /**
     * 构建 GET 请求的查询参数
     *
     * @param endpoint       请求的目标地址
     * @param requestBodyObj 请求体对象，将转换为查询参数添加到 URL 中
     * @return 构建好的 HttpUrl.Builder 对象
     */
    private static HttpUrl.Builder buildQueryParameters(String endpoint, Map<String, Object> requestBodyObj) {
        HttpUrl.Builder urlBuilder = HttpUrl.get(endpoint).newBuilder();
        for (Map.Entry<String, Object> entry : requestBodyObj.entrySet()) {
            urlBuilder.addQueryParameter(entry.getKey(), String.valueOf(entry.getValue()));
        }
        return urlBuilder;
    }


    public static void main(String[] args) {


        // 测试 POST 请求，JSON 类型参数
        Map<String, Object> jsonRequestBody = new HashMap<>();
        jsonRequestBody.put("param1", "1234");
        jsonRequestBody.put("param2", "5678");
        jsonRequestBody.put("param3", "7890");
        String resultJson = sendRequest("http://127.0.0.1:520/test/wx/post", RequestType.POST, ParamType.JSON, jsonRequestBody, null, null);
        log.debug("resultJson: {}", resultJson);


        // 测试 POST 请求，FORM 类型参数
        Map<String, Object> formRequestBody = new HashMap<>();
        formRequestBody.put("module", "qb61113@qq.com");
        formRequestBody.put("action", "123456");
        String resultForm = sendRequest("http://127.0.0.1:520/test/syz/post/form", RequestType.POST, ParamType.FORM, formRequestBody, null, null);
        log.debug("resultForm: {}", resultForm);


        // 测试 GET 请求
        Map<String, Object> getRequestParam = new HashMap<>();
        getRequestParam.put("username", "qb61113@qq.com");
        getRequestParam.put("password", "123456");
        String resultGet = sendRequest("http://127.0.0.1:520/test/wx/get", RequestType.GET, null, getRequestParam, null, null);
        log.debug("resultGet: {}", resultGet);
    }

}
```

