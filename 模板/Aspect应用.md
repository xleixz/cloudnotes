# Aspect 应用

[toc]

---

## 1、导入依赖

```xml
        <!-- https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-aop -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-aop</artifactId>
            <version>3.2.0</version>
        </dependency>
```

## 2、定义一个注解类

```java
import java.lang.annotation.*;

// METHOD声明在方法上使用
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Aop {

}
```

## 3、切面类

```java
import com.ddj.Interceptor.AuthenticationInterceptor;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * @projectName: ding-dong-record
 * @package: com.ddj.common
 * @className: MyAspect
 * @author: QB61113
 * @description:
 * @date: 2024-03-21 11:03
 * @version: 1.8
 */
@Aspect
@Component
public class MyAspect {

    /**
     * @param:
     * @return: void
     * @author: QB61113
     * @description: 声明切入点，@Doc自定义注解出现在哪个方法上就对哪个方法生效
     * @date: 2024-03-21 16:38
     */
    @Pointcut("@annotation(com.ddj.annotation.Aop)")
    private void pointcut() {

    }

    /**
     * @param:
     * @return: void
     * @author: QB61113
     * @description: 后置切面 拦截所有@Aop注解的方法
     * @date: 2024-03-21 11:08
     */
    @After("pointcut()")
    public void beforeCallingAPI(JoinPoint joinPoint) {
        try {
            // 从连接点中获取方法参数
            Object[] args = joinPoint.getArgs();
            if (args != null && args.length > 0) {
                // 遍历args中参数的类型是否为 HttpServletRequest
                for (Object arg : args) {
                    if (arg instanceof HttpServletRequest) {
                    }
             }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        
    /**
     * @param:
     * @return: void
     * @author: QB61113
     * @description: 前置切面 拦截所有@Aop注解的方法
     * @date: 2024-03-21 11:08
     */
    @Before("pointcut()")
```

## 4、在启动类中添加开启注解

```java
@EnableAspectJAutoProxy(proxyTargetClass = true)
```

![image-20240325173937373](https://xleixz.oss-cn-nanjing.aliyuncs.com/typora-img/image-20240325173937373.png)