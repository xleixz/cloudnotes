# Js定位滚动条

## 一、定位页面滚动条

```javascript
<script>
	$(function () {
		// 将被销毁之前（beforeunload 事件）将滚动位置信息存储在 sessionStorage 中
		$(window).on('beforeunload', function() {
    		sessionStorage.setItem('scrollPosition', $(window).scrollTop());
		});

   		 // 获取滚动条位置
		var scrollPosition = sessionStorage.getItem('scrollPosition');
		$(window).scrollTop(scrollPosition || 0);
	})
</script>
```



## 二、定位执行id元素滚动条

```javascript
<div id = "overflow-div">  
<script>
	$(function () {
		// 将被销毁之前（beforeunload 事件）将滚动位置信息存储在 sessionStorage 中
        $(window).on('beforeunload', function() {
            const scrollPosition = $('#overflow-div').scrollTop();
            sessionStorage.setItem('scrollPosition', scrollPosition);
        });

        // 获取滚动条位置
        const scrollPosition = sessionStorage.getItem('scrollPosition');
        if (scrollPosition) {
            $('#overflow-div').scrollTop(scrollPosition || 0);
        }
	})
</script>
```

