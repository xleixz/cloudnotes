# js获取地址栏参数

[TOC]

---

## 1、获取地址栏

```js
var loc = location.href;
```

## 2、获取地址栏的长度

```js
var n1 = loc.length;
```

## 3、获取地址栏中第一个等号的位置

```js
var n2 = loc.indexOf('=')；
```

## 4、以等号位置，截取后面的内容

```js
var str = loc.slice(n2+1,n1);
```

